/*****************************************************************************
 Driver_DistanceSensor.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _DRIVER_DISTANCESENSOR_H_
#define _DRIVER_DISTANCESENSOR_H_


#include "User_Generic.h"


extern uint16_t bin_distance;
extern unsigned char bin_capacity;


/*############################################################################*/
/*#                               API
/*############################################################################*/


uint8_t Run_DistSensorCtrl(void);
void Power_On_DistSensorCtrl(void);
void Power_Off_DistSensorCtrl(void);



#endif
