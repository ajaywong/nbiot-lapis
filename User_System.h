/*****************************************************************************
 User_System.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _USER_SYSTEM_H_
#define _USER_SYSTEM_H_

#include <stdio.h>
#include <string.h>

/* MCU */
#include "mcu.h"
#include "rdwr_reg.h"

#include "User_Generic.h"


/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/
/*  */
#define LOW_BAT_LED_INDICATION
#ifdef LOW_BAT_LED_INDICATION
#define LOW_BATTER_LEVEL 10
#endif

/*  */
/*############################################################################*/
#define CONFIG_MAIN_CLK_4MHZ
//#define CONFIG_MAIN_CLK_8MHZ
//#define CONFIG_MAIN_CLK_16MHZ


/*############################################################################*/
//#define GAS_COUNTER
//#define GAS_COUNTER_TEST

#define WATER_LEAKAGE_SENSOR
//#define WATER_LEAKAGE_SENSOR_TEST

//#define SMART_BIN_SENSOR
//#define SMART_BIN_SENSOR_TEST

//#define PACKING_SENSOR
//#define PACKING_SENSOR_TEST


#define USE_DEBUG_CONSOLE
/*############################################################################*/
#ifdef USE_DEBUG_CONSOLE
#define DebugPrint  printf
#else
#define DebugPrint  //
#endif

/*############################################################################*/
#define DISABLE_FCC_RULE

/*############################################################################*/
#define P31_SW_ENABLE

/*############################################################################*/
#if defined (GAS_COUNTER) || defined (WATER_LEAKAGE_SENSOR)
#define USE_ADC_VOLTAGE_DETECT
#else
#define USE_I2C_VOLTAGE_DETECT
#endif


/*############################################################################*/
#if defined(CONFIG_MAIN_CLK_16MHZ)
#define US_DELAY_COUNT  1060   //~1Ms @16MHz
#define NOP_COUNT       {__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");}
#define CLK_SYSC        CLK_SYSC_OSCLK
#elif defined(CONFIG_MAIN_CLK_8MHZ)
#define US_DELAY_COUNT  1100   //~1Ms @8MHz
#define NOP_COUNT       {__asm("nop");}
#define CLK_SYSC        CLK_SYSC_OSCLK_DIV2
#elif defined(CONFIG_MAIN_CLK_4MHZ)
#define US_DELAY_COUNT  550   //~1Ms @4MHz
#define NOP_COUNT       {__asm("nop");} //5us @4MHZ
#define CLK_SYSC        CLK_SYSC_OSCLK_DIV4

#endif //


/*############################################################################*/
/* Low-Speed Time Base Counter */
#define TBC_T128HZ  0x00
#define TBC_T64HZ   0x01
#define TBC_T32HZ   0x02
#define TBC_T16HZ   0x03
#define TBC_T8HZ    0x04
#define TBC_T4HZ    0x05
#define TBC_T2HZ    0x06
#define TBC_T1HZ    0x07


/*############################################################################*/
#define TBC_0_INTERVAL  TBC_T128HZ
#define TBC_1_INTERVAL  TBC_T2HZ
#define TBC_2_INTERVAL  TBC_T1HZ


/*############################################################################*/
#define	SIGFOX_ERROR            0
#define	SIGFOX_OK               1
#define	SIGFOX_FRAME_RECEIVED   2
#define	SIGFOX_FRAME_WAIT       3


/*############################################################################*/
#define	STATE_KEY_RELEASED  0
#define	STATE_KEY_DEBOUNCE  1
#define	STATE_KEY_PRESSED   2
#define	STATE_KEY_CARRIER   3
#define STATE_KEY_DOWNLINK  4

#define KEY_EVENT_NONE      0
#define KEY_EVENT_PRESSED   1
#define KEY_EVENT_RELEASED  2
#define KEY_EVENT_CARRIER   3
#define	KEY_EVENT_DOWNLINK  4

/*############################################################################*/
#define WATER_NO_LEAK   0x55
#define WATER_LEAK      0xAA

#define DEMO

/*############################################################################*/
#define blinkLED(led)   write_bit( led, ~(get_bit( led )) )
#define onLED(led)      write_bit( led, 1)
#define offLED(led)     write_bit( led, 0)

//#define DEBUG_BOARD

/*############################################################################*/
#define GREEN_LED       P42D // Power
#define RED_LED         P46D // Low Battery

/*############################################################################*/
#define SIGFOX_NO_REPLY  0 //
#define SIGFOX_REPLY     1 //

#define SIGFOX_DEFAULT_LINK     SIGFOX_NO_REPLY // Default

#define SIGFOX_RETRY_COUNT  3

/*############################################################################*/
// Chip select to battery IC: Active-LOW = CHIP ENABLE
#define BATIC_CE_ON 		P23D = 0
#define BATIC_CE_OFF 		P23D = 1

#define BATIC_EN1_ON 		P02D = 1
#define BATIC_EN1_OFF 		P02D = 0

#define BATIC_EN2_ON 		P03D = 1
#define BATIC_EN2_OFF 		P03D = 0

#define POWER_ON			P42D = 1
#define POWER_OFF			P42D = 0

#define USER_DATA_SIZE 4
extern uint16_t userData[USER_DATA_SIZE];


#define BATTERY_CHG_PIN     P32D
#define BATTERY_GOOD_PIN    P33D
#define BATTERY_CE_PIN      P23D

/*############################################################################*/
#define I2C_DATA_SIZE_MAX (32)
#define SPI_DATA_SIZE_MAX (32)


/*############################################################################*/

#define MIN_SCHEDUCLE_TIME      60      //Second
#define SCHEDUCLE_TIME          900    //Second , release version should be 3600


#define RET_ERROR   0
#define RET_OK      1

#define SIGFOX_RETRY_ON   1 //Flag to On/FF retry
#define SIGFOX_RETRY_OFF  0 //Flag to On/FF retry

/*############################################################################*/
/*#                   Global  Variable
/*############################################################################*/
extern unsigned long SysTickets;
extern unsigned char SysTicketFlag ;

extern unsigned char keyEvent;

extern unsigned char MAG3110_Int;

extern unsigned long schedule_Time;
extern unsigned long schedule_Stamp;

extern unsigned char batteryCapacity;
extern uint16_t analogVoltage;



/* I2C-0 */
extern unsigned char i2c_rxData[];
extern unsigned char i2c_txData[];

/* SPI */

extern unsigned char Spi_rxData[];    /**< The data of read           */
extern unsigned char Spi_txData[];    /**< The data of write          */

#ifdef WATER_LEAKAGE_SENSOR
extern unsigned char waterState;
extern unsigned char waterOldState;
#endif // WATER_LEAKAGE_SENSOR

#ifdef GAS_COUNTER
extern unsigned int gasCounter;
extern unsigned int gasOldCounter;
#endif // GAS_COUNTER



/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
void s_initPeri( void );


/*  */
unsigned char get_WaterState(void);


/*  */
void delay_Us(unsigned short value);
void delay_Ms(unsigned short value);

/*  */
void smpl_procWdtHandle( void );
void smpl_LTBC0_Handle( void );
void smpl_LTBC1_Handle( void );
void smpl_LTBC2_Handle( void );
void smpl_procExi3Int(void);
void smpl_procExi5Int(void);
void smpl_procExi4Int(void);
void smpl_procExi6Int(void);


#endif /*_USER_SYSTEM_H_*/



