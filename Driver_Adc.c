
#include <string.h>
#include <stdio.h>

#include "mcu.h"
#include "wdt.h"
#include "gpio_common.h"
#include "irq.h"
#include "clock.h"
#include "lp_manage.h"
#include "saAdc.h"



#include "User_Board.h"
#include "User_System.h"
#include "User_Generic.h"

/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/
/* Parameters for saAdc control */
#define ADC_PARAM_CON0     ( SAADC_ONESHOT | SAADC_CK_LOW | SAADC_TRG_NOT )  /**< one shot, LSCLK, non-touch sense, clock dev=1/1 */
#define ADC_PARAM_TOUCH    ( SAADC_TOUCH_MODE_OFF )     /**< Touch sensor mode */
#define ADC_PARAM_SADEN    (SADEN_SACH0|SADEN_SACH4)//( SADEN_SACH0   | SADEN_SACH1  | SADEN_SACH2 | SADEN_SACH3 | SADEN_SACH4 | SADEN_SACH5 | SADEN_SACH6 | SADEN_SACH7 | SADEN_SACH8 | SADEN_SACH9 | SADEN_SACHA | SADEN_SACHB )
#define ADC_PARAM_SADTCH   ( SAADC_MODE_NOT_TOUCH )     /**< all channels are connected to non-touch sensor */
#define ADC_PARAM_SADTRG   ( SAADC_TRG_NOT )            /**< SA-ADC is started to convert by software */
#define ADC_PARAM_SADCVT   ( SAADC_CVT_SACK_0_SATCM_0 ) /**< LSCLK, non-touch sensor */


/* ADC Sampling times */
#define ADC_SAMPLING_NUMBER ( 10 )
/* ADC Chunnel number */
//#define ADC_CHUNNEL         ( 12 )

/* ADC V Ref */
#define ADC_VREF        3300000  // uV, scale for the resolution
#define BATTERY_LOW     1800000 // uV

#define VALID_BATTERY_RANGE 15000     
#define ADC_RESOLUTION  4096

#define BATTERY_MAX     3200

float Vref  =  ADC_VREF/ADC_RESOLUTION;   // 805


/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/
//static unsigned long    s_adcResultBuffer[ADC_SAMPLING_NUMBER][ADC_CHUNNEL]; /**< SA-ADC result buffer    */
static unsigned char   s_Adc_flag;  /**< The status of SA-ADC           */

uint16_t analogVoltage = 0;


void SA_Adc_Initialize(void)
{
    /*** SA_ADC Initialize */
    /* P21 - VBAT_M_EN */
    clear_bit( P21D );
    clear_bit( P21DIR );			/* Output */
    set_bit( P21C0 );				/* CMOS */
    set_bit( P21C1 );
    clear_bit( P21MD0 );			/* GPIO mode */
    clear_bit( P21MD1 );
#ifdef DEBUG_BOARD
    /* AIN 14 */
    GPIO_INPUT_OUTPUT_MODE(SENSOR_ANALOG_PIN);
    GPIO_INPUT_HI_Z(SENSOR_ANALOG_PIN);
#endif
    GPIO_INPUT_OUTPUT_MODE(BATT_ADC_PIN);
    GPIO_INPUT_HI_Z(BATT_ADC_PIN);

    /* P22 - BAT_NTC ADC */
    write_bit( P22D, 0 );
    clear_bit( P22DIR );			/* Output */
    clear_bit( P22C0 );			/* Hi-Z */
    clear_bit( P22C1 );
    clear_bit( P22MD0 );		/* GPIO mode */
    clear_bit( P22MD1 );

    /* Enable ADC Module */
    clear_bit(DSAD);

	/* set SA-ADC mode */
	saAdc_init( (unsigned char)ADC_PARAM_CON0 );
	saAdc_setEnableChannel( (unsigned short)ADC_PARAM_SADEN );
	saAdc_setTouchChannel ( (unsigned short)ADC_PARAM_SADTCH );
	saAdc_setConversionMode( (unsigned char)ADC_PARAM_TOUCH);
	saAdc_setTrigger( (unsigned short)ADC_PARAM_SADTRG );
	saAdc_setAccuracy ( (unsigned short)ADC_PARAM_SADCVT );
}



void get_Adc_Value(void)
{
    unsigned long  s_adcResult;
    unsigned char i = 0;

	/* start to count */
	set_bit( P21D );
    delay_Ms(15);

	s_adcResult = 0;
	for(i=0; i < ADC_SAMPLING_NUMBER; i++)
  {
		saAdc_execute( SAADC_RUN );

		s_Adc_flag = FLAG_CLR;
		while( s_Adc_flag == FLAG_CLR )
		{
		}
		s_adcResult += saAdc_getResult4();
		delay_Ms(1);
	}

    clear_bit( P21D );

    s_adcResult = s_adcResult / ADC_SAMPLING_NUMBER;
    s_adcResult = s_adcResult * Vref;

    DebugPrint("V:%d\r\n", s_adcResult);

    if(s_adcResult < BATTERY_LOW)
    {
        s_adcResult = 0;
    }
    else
    {
        s_adcResult = s_adcResult - BATTERY_LOW;
    }

    batteryCapacity = (s_adcResult / VALID_BATTERY_RANGE);
    if(batteryCapacity > 95)
    {
        batteryCapacity = 100;
    }
#ifdef DEMO
    batteryCapacity = 100;
#endif
    DebugPrint("Batttery Cap:%d\r\n", batteryCapacity);

}

void get_sensor_Adc_Value(void)
{
    unsigned long  s_adcResult;
    unsigned char i = 0;
    unsigned long  tmp_Result;

	s_adcResult = 0;
	for(i=0; i < ADC_SAMPLING_NUMBER; i++)
  {
		saAdc_execute( SAADC_RUN );

		s_Adc_flag = FLAG_CLR;
		while( s_Adc_flag == FLAG_CLR )
		{
		}
    s_adcResult += saAdc_getResult0(); 
		delay_Ms(1);
	}

    s_adcResult = s_adcResult / ADC_SAMPLING_NUMBER;

    s_adcResult = s_adcResult * 805;
    
    analogVoltage = s_adcResult/1000;

    DebugPrint("Analog Pin Voltage %d mV\r\n", analogVoltage);
}



/**
 * SA-Adc interrupt routine
 *
 * @param           -
 * @return          None
 */
void smpl_procSaAdcInt( void )
{
	s_Adc_flag = FLAG_SET;
}









