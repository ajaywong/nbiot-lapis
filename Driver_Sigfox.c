
/*****************************************************************************
 Driver_Sigfox.c

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/

#include <string.h>
#include <stdio.h>

/* MCU Include */
#include "mcu.h"
#include "uartf0.h"


/* User Include */
#include "User_System.h"
#include "User_Generic.h"


/* Driver Include */
#include "Driver_Sigfox.h"
#include "Driver_Adc.h"
#include "Driver_DataFlash.h"

#include "Driver_TempSensor.h"
#include "Driver_MotionSensor.h"
#include "Driver_DistanceSensor.h"
#include "Driver_Battery.h"
#include "Driver_MAG3110.h"
#include "Driver_Sigfox.h"

#define DebugPrintTest  printf



/*############################################################################*/
#define INFO_SOFT_NAME_VER      0
#define INFO_SILICON_VER_L      2
#define INFO_SILICON_VER_H      3
#define INFO_FIRMWARE_VER_MAJOR 4
#define INFO_FIRMWARE_VER_MINOR 5
#define INFO_FIRMWARE_REV       6
#define INFO_FIRMWARE_VARIANT   7
#define INFO_FIRMWARE_VCS       8
#define INFO_SIGFOX_LIB_VER     9


#define SIGFOX_PIN_LOW          0
#define SIGFOX_PIN_HIGH         1
#define SIGFOX_PIN_INPUT_HZ     Z
#define SIGFOX_PIN_INPUT_PU     U
#define SIGFOX_PIN_ADC          A
#define SIGFOX_PIN_DAC          T

#define OPENCLOSED_TIMEOUT 5000
//#define TCP_PROTOCOL

/*############################################################################*/
#define SIGFOX_INFO_LEN         20

/*############################################################################*/
#define SIGFOX_FRAME_TIMEOUT        10  // 60
#define SIGFOX_FRAME_REPLY_TIMEOUT  55

/*############################################################################*/
#ifdef AM53     // Water Leakage
#define SIGFOX_START_ID   0x7C
#else           // Water Rope
#define SIGFOX_START_ID   0x7B
#endif

static unsigned char s_tx_flag;
static unsigned char s_rx_flag;
unsigned char dumpBuffer[UARTF_RW_MAX];
unsigned char s_rxBuffer[UARTF_RW_MAX];
unsigned char s_txBuffer[UARTF_RW_MAX];

unsigned char Sigfox_StrBuff[SIGFOX_INFO_LEN];
unsigned char Sigfox_Tx_Freq[SIGFOX_INFO_LEN];

unsigned long SigfoxTimeStamp = 0;
unsigned int  SigfoxTimeout = SIGFOX_FRAME_TIMEOUT;

//static unsigned char Sigfox_SendState = SIGFOX_FRAME_NONE;
static unsigned char sigfox_state = 0;
static unsigned char Sigfox_PowerState = SIGFOX_STANDBY;

unsigned char replyOption = SIGFOX_NO_REPLY;

/*############################################################################*/
/*#                              Subroutine                                  #*/
/*############################################################################*/
/**
 *
 */
void Sigfox_HW_Reset(void)
{
    SIGFOX_RESET_ON;
    delay_Ms(50);

    SIGFOX_RESET_OFF;  // not reset to Sigfox module
    delay_Ms(200);

    Sigfox_PowerState = SIGFOX_STANDBY;
    DebugPrint("Sigfox Reset\r\n");
}

/**
 *
 */
void Sigfox_Wakeup(void)
{
    SIGFOX_WAKEUP_ON;
    delay_Ms(50);

    SIGFOX_WAKEUP_OFF;  // not wakeup to Sigfox module
}

/**
 *
 */
void Sigfox_PowerOn(void)
{
    SIGFOX_PWR_ON;
}

/**
 *
 */
void Sigfox_PowerOff(void)
{
    SIGFOX_PWR_OFF;
}


/**
 *
 */
unsigned char HexStrToLong(char *src, unsigned long *dest)
{
    unsigned long tmp = 0;

    while (*src != 0)
    {
        if (*src >= '0' && *src <='9')
            tmp = tmp*16 + *src - '0';
        else if (*src >= 'A' && *src <='F')
            tmp = tmp*16 + *src - 'A' + 10;
        else if (*src >= 'a' && *src <='f')
            tmp = tmp*16 + *src - 'a' + 10;
        else if (*src != ' ' && *src != 'x' && *src != 'X')
            return 1;
        src++;
    }
    *dest = tmp;

    return 0;
}

/**
 *
 */
unsigned char StrToLong(char *src, unsigned long *dest)
{
    unsigned long tmp = 0;

    while (*src != 0)
    {
        if (*src >= '0' && *src <='9')
            tmp = tmp*10 + *src - '0';
        else if (*src != ' ' && *src != 'x' && *src != 'X')
            return 1;
        src++;
    }
    *dest = tmp;

    return 0;
}


/**
 *
 */
unsigned char Make_SigfoxCtrl_CRC(unsigned char *ptr, unsigned char len)
{
    unsigned char checksum = 0;

    while(len > 0)
    {
        checksum += *ptr;
        checksum %= 256;
        *ptr++;
        len--;
    }

    return checksum;
}


#ifdef WATER_LEAKAGE_SENSOR
/**
 * Sigfox_WaterLakage_Packet
 */
void Sigfox_Frame_Packet(void)
{
    unsigned char checksum = 0;

    /*  */
    get_Adc_Value();
    
    get_sensor_Adc_Value();

    memset(s_txBuffer, 0x00, UARTF_RW_MAX);
    
    dumpBuffer[0] = ((analogVoltage >> 8) & 0xff);
    dumpBuffer[1] = (analogVoltage & 0xff);
    if (waterOldState == WATER_LEAK) {
          dumpBuffer[3] = 0;
    } else {
          dumpBuffer[3] = 1;
    }

    checksum = Make_SigfoxCtrl_CRC(dumpBuffer, 5);
    memset(s_txBuffer, 0x00, UARTF_RW_MAX);
    

    if(replyOption == SIGFOX_NO_REPLY)
    {
        sprintf(s_txBuffer, "AT+QISENDEX=4,\"00000000%02x%02x%02d\"\r\n",  dumpBuffer[0], dumpBuffer[1], dumpBuffer[3]);
    }
    else
    {
        sprintf(s_txBuffer, "AT+QISENDEX=4,\"00000000%02x%02x%02d\"\r\n",  dumpBuffer[3]);
    }
}
#endif

/**
 *
 */
unsigned char Sigfox_Parser_Packet(void)
{
    unsigned long time = 0;

    /*
         Reply Packet Format:OK\r\nRX=00 00 40 11 22 33 44 55\r\n
         00 00 40: Schedule Time
    */

    /*   */
    if(memcmp(s_rxBuffer, "OK", 2) == 1)
    {
        return SIGFOX_ERROR;
    }

    /*   */
    if(memcmp(s_rxBuffer+4, "RX=", 3) == 1)
    {
        return SIGFOX_ERROR;
    }

    /*  */
    memset(dumpBuffer, 0x00, UARTF_RW_MAX);
    memcpy(dumpBuffer, s_rxBuffer + 7, 2);
    memcpy(dumpBuffer+2, s_rxBuffer + 10, 2);
    memcpy(dumpBuffer+4, s_rxBuffer + 13, 2);

    StrToLong(dumpBuffer, &time);

    if((time != schedule_Time) && (time >= MIN_SCHEDUCLE_TIME))
    {
        uint16_t tmp_data[4];
        schedule_Time = time; 
        DebugPrint("Schedule Time updated:%lu\r\n", schedule_Time);

#ifdef GAS_COUNTER        
        userData[0] = ((gasCounter >> 16) & 0xffff);
        userData[1] = (gasCounter & 0x0000ffff);                
#endif        
        userData[2] = ((schedule_Time >> 16) & 0xffff);        
        userData[3] = (schedule_Time & 0x0000ffff);        
        UserDataFlash_Write(userData, USER_DATA_SIZE);
        
        
        UserDataFlash_Read(tmp_data, USER_DATA_SIZE);        
        DebugPrint("tmp data updated:0x%04x 0x%04x 0x%04x 0x%04x\r\n", tmp_data[0], tmp_data[1], tmp_data[2], tmp_data[3]);
        
    }

    return SIGFOX_OK;
}



/**
 *
 */
unsigned char Check_Reply_OK(void)
{
#if 0    
    if(memcmp(s_rxBuffer, "OK", 2) == 0)
    {
        return SIGFOX_OK;
    }
#endif    

    return SIGFOX_OK;
}

/**
 *
 */
static void s_procUartWrite( unsigned int size, unsigned char errStat )
{
    size    = size;
    errStat = errStat;
}

/**
 *
 */
static void s_procUartRead( unsigned int size, unsigned char errStat )
{
    errStat = errStat;
    /* get the size of valid data */
    size = size;

    s_rxBuffer[size] = '\0';
    s_rx_flag = FLAG_SET;                                                                                                   
}

/**
 *
 */
unsigned char Sigfox_Cmd(void)
{
    unsigned char retry = 1;
    unsigned long timeout = 1000;

    /*  */
    while(retry--)
    {
        timeout = 0x50000;
        /*  */
        memset(s_rxBuffer, 0x00, UARTF_RW_MAX);

        s_tx_flag = FLAG_CLR;
        uartf0_write( s_txBuffer, strlen(s_txBuffer), (void *)0 );
//        DebugPrint("->%s", s_txBuffer);
        while(s_tx_flag == FLAG_CLR);

        /*   */
        s_rx_flag = FLAG_CLR;
        reply_count = 1;
        uartf0_read( s_rxBuffer, UARTF_RW_MAX, s_procUartRead);
        
        while (timeout-- != 0)
        {
            if (s_rx_flag == FLAG_SET)
              break;
        }
        uartf0_clearReadFifo();

        if(s_rx_flag == FLAG_SET)
        {
//            DebugPrint("<-%s", s_rxBuffer);
            return SIGFOX_OK;
        }
    }

    return SIGFOX_ERROR;
}

unsigned char Sigfox_ATQUERY(void)
{
    
    sprintf(s_txBuffer,"AT+CGATT?\r\n");
    DebugPrint(">>>>> Going to issue command %s \r\n", s_txBuffer);
    if(Sigfox_Cmd())
    {        
        return Check_Reply_OK();
    }

    return SIGFOX_ERROR;
}


unsigned char Sigfox_ATConnect(void)
{

    sprintf(s_txBuffer,"AT+QIACT=1\r\n");
    if(Sigfox_Cmd())
    {        
        return Check_Reply_OK();
    }

    return SIGFOX_ERROR;
}

extern unsigned char currentSensor;
unsigned char Sigfox_ATOpenSock(void)
{

    
#ifdef TCP_PROTOCOL
    if (currentSensor == 1) {   // smoker
        DebugPrint("Request to open socket with TCP For Smoke Sensor\r\n");
    } else {
        DebugPrint("Request to open socket with TCP For NH3\r\n");
    }
    
    if (currentSensor == 1) {   // smoker    
      sprintf(s_txBuffer,"AT+QIOPEN=1,4,\"TCP\",\"113.28.21.226\",4561,0,0\r\n");
    } else {
      sprintf(s_txBuffer,"AT+QIOPEN=1,4,\"TCP\",\"113.28.21.226\",4559,0,0\r\n");
    }
        
#else
    DebugPrint("Request to open socket with UDP\r\n");
    if (currentSensor == 1) {   // smoker            
      sprintf(s_txBuffer,"AT+QIOPEN=1,4,\"UDP\",\"113.28.21.226\",4560,0,0\r\n");
    } else { 
      sprintf(s_txBuffer,"AT+QIOPEN=1,4,\"UDP\",\"113.28.21.226\",4558,0,0\r\n");
    }
          
#endif    
    if(Sigfox_Cmd())
    {
        delay_Ms(OPENCLOSED_TIMEOUT);        
        return Check_Reply_OK();
    }

    return SIGFOX_ERROR;
}

unsigned char Sigfox_ATCloseSock(void)
{

    DebugPrint("Request to close socket\r\n");
    sprintf(s_txBuffer, "AT+QICLOSE=4\r\n");
    if(Sigfox_Cmd())
    {
        delay_Ms(OPENCLOSED_TIMEOUT);        
        return Check_Reply_OK();
    }

    return SIGFOX_ERROR;
}


unsigned char Sigfox_AT(void)
{

    strcpy(s_txBuffer,"AT\r\n");
    if(Sigfox_Cmd())
    {
        DebugPrint("AT Command return :%s", s_rxBuffer);
        return SIGFOX_OK;
    }

    return SIGFOX_ERROR;
}


/*
*
*/
unsigned char Sigfox_SetPowerMode(unsigned char uint)
{
    unsigned char ret;
    
    return SIGFOX_OK;

}


/*
*
*/
unsigned char Sigfox_ChangePowerMode(void)
{
    unsigned char ret = SIGFOX_OK;
    return ret;
}


/*
*
*/
unsigned char Sigfox_CarrierMode(void)
{
    Sigfox_ChangePowerMode();

    if(strncmp(Sigfox_Tx_Freq, "90", 2) == 0)
    {
        strcpy(s_txBuffer, "AT$CW=902200000,1\r");
        DebugPrint("Set CW:902200000\r\n");
    }
    else if(strncmp(Sigfox_Tx_Freq, "92", 2) == 0)
    {
        strcpy(s_txBuffer, "AT$CW=920800000,1\r");
        DebugPrint("Set CW:920800000\r\n");
    }
    else if(strncmp(Sigfox_Tx_Freq, "86", 2) == 0)
    {
        strcpy(s_txBuffer, "AT$CW=868130000,1,14\r");
        DebugPrint("Set CW:868130000\r\n");
    }

    if(Sigfox_Cmd())
    {
        return Check_Reply_OK();
    }

    return SIGFOX_ERROR;
}


/*
*
*/
unsigned char Sigfox_SetCarrier(void)
{
    unsigned char retVal = SIGFOX_ERROR;

    if(strncmp(Sigfox_Tx_Freq, "90", 2) == 0)
    {
        strcpy(s_txBuffer, "AT$CW=902200000,0\r");
        DebugPrint("CW:902200000\r\n");
    }
    else if(strncmp(Sigfox_Tx_Freq, "92", 2) == 0)
    {
        strcpy(s_txBuffer, "AT$CW=920800000,0\r");
        DebugPrint("CW:920800000\r\n");
    }
    else if(strncmp(Sigfox_Tx_Freq, "86", 2) == 0)
    {
        strcpy(s_txBuffer, "AT$CW=868130000,0,14\r");
        DebugPrint("CW:868130000\r\n");
    }

    if(Sigfox_Cmd())
    {
        retVal = Check_Reply_OK();
    }

    // Test
//    Sigfox_GetTxFreq();
    //Sigfox_GetRxFreq();

    return retVal;
}


/*
*
*/
unsigned char Sigfox_Save(void)
{
    sprintf(s_txBuffer," AT$WR\r");
    if(Sigfox_Cmd())
    {
        return Check_Reply_OK();
    }

    return SIGFOX_ERROR;
}

/*
*
*/
unsigned char Sigfox_config(void)
{
    unsigned char ret;
    /*   */
    ret = Sigfox_AT();
    if(ret == SIGFOX_ERROR)
    {
        return ret;
    }
    
    DebugPrint("connect to Server\r\n");    
    ret = Sigfox_ATConnect();               

    return ret;
}




/*
*
*/
unsigned char Sigfox_SendFrameInit(void)
{
    /*  */
    Sigfox_ATOpenSock();

    /*  */
    Sigfox_Frame_Packet();
    
    if (batteryCapacity >= LOW_BATTER_LEVEL)  {  /* Only Send Sigfox Battery level is enough*/
      s_tx_flag = FLAG_CLR;
      uartf0_write( s_txBuffer, strlen(s_txBuffer), (void *)0 );
      while(s_tx_flag == FLAG_CLR);
  
      DebugPrint("Send Frame:%s", s_txBuffer);
  
      /*  */
      SigfoxTimeStamp = SysTickets;
      s_rx_flag = FLAG_CLR;
      uartf0_read( s_rxBuffer, UARTF_RW_MAX, s_procUartRead);
      return SIGFOX_OK;
    } else {
      return SIGFOX_ERROR;
    }

}


unsigned char Sigfox_SendFrameCheck(void)
{
    if(SigfoxTimeout >= (SysTickets - SigfoxTimeStamp))  // check timeout
    {
        if(s_rx_flag == FLAG_SET)
        {
            if(replyOption == SIGFOX_REPLY)
            {
/*            
                DebugPrint("Reply:%s", s_rxBuffer);
                //DebugPrint("Time Used:%d\r\n", (SysTickets - SigfoxTimeStamp));

                uartf0_clearReadFifo();
                if(memcmp(s_rxBuffer, "ERR", 3) == 0)
                {
                    Sigfox_SetPowerMode(FIRMWARE_MODULE_SLEEP_MODE);
                    return SIGFOX_ERROR;
                }
                else
                {
                    Sigfox_Parser_Packet();
                    Sigfox_SetPowerMode(FIRMWARE_MODULE_SLEEP_MODE);
                    return SIGFOX_OK;
                }
*/                
            }
            else
            {
                if(Check_Reply_OK())
                {
                    Sigfox_ATCloseSock();
                    return SIGFOX_OK;
                }
            }
        }

        return SIGFOX_FRAME_WAIT;
    }
    uartf0_clearReadFifo();
#ifdef DEMO
    if(Check_Reply_OK())
    {
        return SIGFOX_OK;
    }
#endif    
    DebugPrint("Frame Timeout!\r\n");
    return SIGFOX_ERROR;
}




/**
 * UARTF interrupt routine
 * @note
 * Writing "1" to this bit(UF0IRQ) while there is any unprocessed interrupt source and <br>
 * processing all the interrupt sources before exiting the interrupt vector will       <br>
 * cause re-entry to the interrupt vector with no interrupt source after exiting the interrupt vector. <br>
 * Ensure to write "1" immediately before exiting the interrupt vector.
 *
 * @param           -
 * @return          None
 */
void smpl_procUartfInt( void )
{
    unsigned short intStatus = 0;
    int rdStatus  = UARTF_R_OK;

    intStatus = uartf0_getIntCause() & UARTF_MASK_INT;
    /* Int(Request of reading data / Character time out / Received data error) */
    if( (intStatus  & UARTF_MASK_INT_RECEIVE) != 0 )
    {
        rdStatus = uartf0_continueRead();
    }
    /* Int(Request of writing data) */
    else if ( intStatus == UARTF_IRID_WRITE_REQ )
    {
        rdStatus = uartf0_continueWrite();
        if( rdStatus == UARTF_R_TRANS_FIN )
        {
            s_tx_flag = FLAG_SET;
        }
    }

    uartf0_trigIRQ();
}

unsigned char Sigfox_OOB(void)
{
    unsigned char ret;

    strcpy(s_txBuffer," AT$SO\r");

    ret = Sigfox_Cmd();
    if(ret)
    {
        ret = Check_Reply_OK();
        
    }
    DebugPrint("SigFox OOB Send with status %d\r\n", ret);
    return ret;
    
}
