/*****************************************************************************

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */

#include <stdio.h>
#include <String.h>
#include <stdlib.h>

/*  API  */
#include "mcu.h"
#include "wdt.h"
#include "irq.h"
#include "clock.h"


#include "User_System.h"
#include "User_Generic.h"

/*  */
#include "Driver_Battery.h"
#include "Driver_Soft_I2c.h"


/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/
//volatile short gBattery_Current = 0;

/*
short FG_STATUS = 0;
short FG_ID = 0;
short FG_FW_Version = 0;
short FG_HW_Version = 0;
short FG_Int_Temp  = 0;
short FG_Bat_Temp  = 0;
short FG_Bat_Current  = 0;
short FG_Bat_Volt  = 0;
short FG_Bat_Cyc_Count  = 0;
*/

short FG_Bat_SOC  = 0;

/*
short FG_Bat_NAC  = 0;  // Nom Available Capacity
short FG_Bat_FAC  = 0;  // Full Available Capacity
short FG_Bat_RMC  = 0;  // Remaining Capacity
short FG_Bat_FCC  = 0;  // Full Charge Capacity
*/


//#ifdef USE_DEBUG
//volatile short  FG_IntTemp  = 0;
//volatile short  FG_BatTemp  = 0;
//volatile short  FG_BatVolt  = 0;
//#endif // USE_DEBUG

/*############################################################################*/
/*#                                Function Prototype                                  #*/
/*############################################################################*/


/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/

unsigned char bq27541_SET_HIBERNATE(void)
{
    unsigned char ret = I2C_ERROR_ACK;
    unsigned char cmd[2] = { 0x11, 0x00 };

    cmd[0] = 0x10;
    cmd[1] = 0x00;
    ret = swI2cMst0_Write(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);

    cmd[0] = 0x11;
    cmd[1] = 0x00;
    ret = swI2cMst0_Write(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);

    return ret;
}



unsigned char bq27541_get_status(void)
{
    unsigned char ret = I2C_ERROR_ACK;
    unsigned char cmd[2] = { 0x00, 0x00 };
    unsigned short FG_STATUS;

    ret = swI2cMst0_Write(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
    if(ret == I2C_OK)
    {
        ret = swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
        if( ret == I2C_OK )
        {
        	FG_STATUS = cmd[0];
        	FG_STATUS |= cmd[1]<<8;

        	DebugPrint("FG Status = 0x%x\r\n", FG_STATUS);
        }
    }

	return ret;
}




/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_BatterySOC(short *fg_bat_soc)
{
    unsigned char dump[2];
    unsigned short val = 0;

    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_SOC, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        *fg_bat_soc = val;

		DebugPrint("FG Bat SOC = 0x%x (%d percent)\r\n", *fg_bat_soc, *fg_bat_soc);

        return  I2C_OK;
    }

    return I2C_ERROR_ACK;
}



#if 0
unsigned char bq27541_get_ID(short *fg_id)
{
    unsigned char ret = I2C_ERROR_ACK;
    unsigned char cmd[2] = { 0x01, 0x00 };

    ret = swI2cMst0_Write(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
    if(ret == I2C_OK)
    {
        ret = swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
        if( ret == I2C_OK )
        {
        	*fg_id = cmd[0];
        	*fg_id |= cmd[1]<<8;

        	DebugPrint("FG ID = 0x%x\r\n", *fg_id);
        }
    }

	return ret;
}



unsigned char bq27541_get_FW_version(void)
{
	unsigned char ret = I2C_ERROR_ACK;
	unsigned char cmd[2] = { 0x02, 0x00 };

	ret = swI2cMst0_Write(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
	if(ret == I2C_OK)
	{
		ret = swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
		if( ret == I2C_OK )
		{
			FG_FW_Version = cmd[0];
        	FG_FW_Version |= cmd[1]<<8;

        	DebugPrint("FG FWV = 0x%x\r\n", FG_FW_Version);

		}
	}

	return ret;
}


unsigned char bq27541_get_HW_version(void)
{
	unsigned char ret = I2C_ERROR_ACK;
	unsigned char cmd[2] = { 0x03, 0x00 };

	ret = swI2cMst0_Write(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
	if(ret == I2C_OK)
	{
		ret = swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_CONTROL, cmd, 2);
		if( ret == I2C_OK )
		{
			FG_HW_Version = cmd[0];
        	FG_HW_Version |= cmd[1]<<8;

        	DebugPrint("FG HWV = 0x%x\r\n", FG_HW_Version);

		}
	}

	return ret;
}



/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_NAC(void)
{
    unsigned char dump[2];
    unsigned short val = 0;

    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_NAC, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        FG_Bat_NAC = val;

		DebugPrint("FG NAC = 0x%x (%d mAh)\r\n", FG_Bat_NAC, FG_Bat_NAC);

        return  I2C_OK;
    }
    return I2C_ERROR_ACK;
}


/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_FAC(void)
{
    unsigned char dump[2];
    unsigned short val = 0;

    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_FAC, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        FG_Bat_FAC = val;

        DebugPrint("FG FAC = 0x%x (%d mAh)\r\n", FG_Bat_FAC, FG_Bat_FAC);

        return  I2C_OK;
    }
    return I2C_ERROR_ACK;
}


/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_RMC(void)
{
    unsigned char dump[2];
    unsigned short val = 0;

    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_RMC, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        FG_Bat_RMC = val;

		DebugPrint("FG RMC = 0x%x (%d mAh)\r\n", FG_Bat_RMC, FG_Bat_RMC);

        return  I2C_OK;
    }
    return I2C_ERROR_ACK;
}



/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_FCC(void)
{
    unsigned char dump[2];
    unsigned short val = 0;

    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_FCC, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        FG_Bat_FCC = val;

		DebugPrint("FG FCC = 0x%x (%d mAh)\r\n", FG_Bat_FCC, FG_Bat_FCC);

        return  I2C_OK;
    }
    return I2C_ERROR_ACK;
}




/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_BatteryCurrent(void)
{
    unsigned char dump[2];
    unsigned short val = 0;

    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_CURRENT, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        FG_Bat_Current = val;

		DebugPrint("FG Bat Current = 0x%x (%d mA)\r\n", FG_Bat_Current, FG_Bat_Current);

        return  I2C_OK;
    }

    return I2C_ERROR_ACK;
}


/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_BatteryCycleCount(void)
{
    unsigned char dump[2];
    unsigned short val = 0;

    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_CC, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        FG_Bat_Cyc_Count = val;

        DebugPrint("FG Bat Cyc Cnt = 0x%x (%d cnt)\r\n", FG_Bat_Cyc_Count, FG_Bat_Cyc_Count);

        return  I2C_OK;
    }

    return I2C_ERROR_ACK;

}

/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_BatterInfo(void)
{

/*
	if( bq27541_get_BatterySOC() == I2C_OK)
    {
        if( bq27541_get_BatteryCurrent() == I2C_OK)
        {
            if( bq27541_get_BatteryCycleCount() == I2C_OK)
            {
               // dataFlash_Write2Byte(ble_Reg[BAT_CYCLE_COUNT]);

                //return BQ2589X_FAULT_NONE;
            }
        }
    }

*/
	return 0;
}


//#ifdef USE_DEBUG
/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_BatteryVolt(void)
{
    unsigned char dump[2];
    unsigned short val = 0;


    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_VOLTAGE, dump, 2) == I2C_OK )
    {
        val = dump[1];
        val = (val << 8) | dump[0];
        FG_Bat_Volt = val;

		DebugPrint("FG Bat Volt = 0x%x (%d mV)\r\n", FG_Bat_Volt, FG_Bat_Volt);

        return  I2C_OK;
    }

    return I2C_ERROR_ACK;

}


/******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*******************************************************************************************/
unsigned char bq27541_get_BatteryTemp(void)
{
    unsigned char dump[2];
    unsigned short val = 0;
	float temp=0;


    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_TEMP, dump, 2) == I2C_ERROR_ACK )
    {
        return I2C_ERROR_ACK;
    }
    val = dump[1];
    val = (val << 8) | dump[0];
    FG_Bat_Temp = val;

    temp = (float)(FG_Bat_Temp/10) - 272.15f;
    DebugPrint("FG Bat Temp = 0x%x (%d per 0.1K, %.2f C)\r\n", FG_Bat_Temp, FG_Bat_Temp, temp);


    if( swI2cMst0_Read(BQ27541_ADDRESS, BQ27541_REG_INTTEMP, dump, 2) == I2C_ERROR_ACK )
    {
        return I2C_ERROR_ACK;
    }
    val = dump[1];
    val = (val << 8) | dump[0];
    FG_Int_Temp  = val;


    temp = (float)(FG_Int_Temp/10) - 272.15f;
    DebugPrint("FG Int Temp = 0x%x (%d per 0.1K, %.2f C)\r\n", FG_Int_Temp, FG_Int_Temp, temp);

    return I2C_OK;

}
#endif // 0

