#include <stdio.h>
#include <String.h>
#include <stdlib.h>

/*  */
#include "rdwr_reg.h"
#include "mcu.h"

#include "clock.h"
#include "wdt.h"
#include "irq.h"

/*   */
#include "User_System.h"
#include "User_Generic.h"

#include "Driver_LIS3DH.h"
#include "Driver_LIS3DSH.h"

#include "Driver_MotionSensor.h"


//#define USE_LIS3DSH  1
#define USE_LIS3DH   1

#define READ_MOTION_SENSOR_COUNT 16




unsigned char Bin_CoverClosed = 0;

// Output enable to translator signals: Active-HIGH = Output enable
#define SPI_M_OE_ON			P53D = 1
#define SPI_M_OE_OFF		P53D = 0


void Power_On_MotionSensorCtrl(void)
{
	SPI_M_OE_ON; // enable 2.5V diver IC to motion sensor
}

void Power_Off_MotionSensorCtrl(void)
{
	SPI_M_OE_OFF; // disable 2.5V diver IC to motion sensor
}



#ifdef USE_LIS3DSH

void Init_MotionSensorCtrl(void)
{
	uint8_t status = 0;

	Power_On_MotionSensorCtrl();

	status = LIS3DSH_init();

	if(status)
	{
		DebugPrint("Error - init motion sensor");
	}
	else
	{
		DebugPrint("OK - init motion sensor");
	}
}


void Run_MotionSensorCtrl(void)
{
	uint8_t status = 0;
	uint8_t i = 0;
	uint16_t retry = 0;
	float x_g = 0;
	float y_g = 0;
	float z_g = 0;

	/*
	// read one times
	uint8_t status = 0;

	status = LIS3DSH_get_measure_XYZ(&LIS3DSH_X_G, &LIS3DSH_Y_G, &LIS3DSH_Z_G);

	if(status)
	{
		DebugPrint("Error - new XYZ data not available");
	}
	*/

	Init_MotionSensorCtrl();

	while(i<READ_MOTION_SENSOR_COUNT)
	{
		if(LIS3DSH_get_measure_XYZ(&LIS3DSH_X_G, &LIS3DSH_Y_G, &LIS3DSH_Z_G)==0)
		{
			x_g += LIS3DSH_X_G;
			y_g += LIS3DSH_Y_G;
			z_g += LIS3DSH_Z_G;

			i++;
			retry = 0;
		}
		else
		{
			retry++;
			if(retry > 5000)
				break;
		}
	}

	if(i==READ_MOTION_SENSOR_COUNT)
	{
		x_g = x_g/READ_MOTION_SENSOR_COUNT;
		y_g = y_g/READ_MOTION_SENSOR_COUNT;
		z_g = z_g/READ_MOTION_SENSOR_COUNT;

		LIS3DSH_X_G = x_g;
		LIS3DSH_Y_G = y_g;
		LIS3DSH_Z_G = z_g;

		if(LIS3DSH_Z_G > -1.1 && LIS3DSH_Z_G < -0.8)
			Bin_CoverClosed = 1;
		else
			Bin_CoverClosed = 0;


		DebugPrint("#AX=%.2f, AY=%.2f, AZ=%.2f, cover_closed=%d\r\n", LIS3DSH_X_G, LIS3DSH_Y_G, LIS3DSH_Z_G, Bin_CoverClosed);

	}
	else
	{
		DebugPrint("Error - read MOTION sensor\r\n");
	}

	Power_Off_MotionSensorCtrl();
}

#else

uint8_t Init_MotionSensorCtrl(void)
{
	uint8_t status = 0;

    Power_On_MotionSensorCtrl();

	status = LIS3DH_init();

	if(status)
	{
		return RET_ERROR;
	}

	return RET_OK;
}


uint8_t Run_MotionSensorCtrl(void)
{
	uint8_t status = 0;
	uint8_t i = 0;
	uint16_t retry = 0;
	float x_g = 0;
	float y_g = 0;
	float z_g = 0;

	// read one times
	/*
	uint8_t status = 0;

	status = LIS3DH_get_measure_XYZ(&LIS3DH_X_G, &LIS3DH_Y_G, &LIS3DH_Z_G);
	if(status)
	{
		DebugPrint("Error - new XYZ data not available");
	}
	*/

    if(Init_MotionSensorCtrl() == RET_ERROR)
    {
        LIS3DH_writeReg8(LIS3DH_REG_CTRL1, 0x00);
        Power_Off_MotionSensorCtrl();
        return RET_ERROR;
    }

	while(i<READ_MOTION_SENSOR_COUNT)
	{
		if(LIS3DH_get_measure_XYZ(&LIS3DH_X_G, &LIS3DH_Y_G, &LIS3DH_Z_G)==0)
		{
			x_g += LIS3DH_X_G;
			y_g += LIS3DH_Y_G;
			z_g += LIS3DH_Z_G;

			i++;
			retry = 0;
		}
		else
		{
			retry++;
			if(retry > 5000)
				break;
		}
	}

	if(i==READ_MOTION_SENSOR_COUNT)
	{
		x_g = x_g/READ_MOTION_SENSOR_COUNT;
		y_g = y_g/READ_MOTION_SENSOR_COUNT;
		z_g = z_g/READ_MOTION_SENSOR_COUNT;

		LIS3DH_X_G = x_g;
		LIS3DH_Y_G = y_g;
		LIS3DH_Z_G = z_g;

		//DebugPrint("X:%.2f, Y:%.2f, Z:%.2f ", LIS3DH_X_G, LIS3DH_Y_G, LIS3DH_Z_G);

		if(LIS3DH_Z_G > -1.1 && LIS3DH_Z_G < -0.8)
        {
            Bin_CoverClosed = 1;
            DebugPrint("Cover Closed\r\n");
        }
		else
        {
            Bin_CoverClosed = 0;
            DebugPrint("Cover Opened\r\n");
        }

        LIS3DH_writeReg8(LIS3DH_REG_CTRL1, 0x00);
        Power_Off_MotionSensorCtrl();

        return RET_OK;
	}

    LIS3DH_writeReg8(LIS3DH_REG_CTRL1, 0x00);
    Power_Off_MotionSensorCtrl();

    return RET_ERROR;
}

#endif




