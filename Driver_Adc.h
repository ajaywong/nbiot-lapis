/*****************************************************************************
 Driver_Adc.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _DRIVER_ADC_H_
#define _DRIVER_ADC_H_


extern unsigned long  s_adcResult;

void SA_Adc_Initialize(void);
void get_Adc_Value(void);
void get_sensor_Adc_Value(void);

void smpl_procSaAdcInt( void );


#endif
