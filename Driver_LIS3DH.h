/*****************************************************************************
 Driver_LIS3DH.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/


#ifndef _DRIVER_LIS3DH_H_
#define _DRIVER_LIS3DH_H_

/* User Include */
#include "User_System.h"
#include "User_Generic.h"


/*############################################################################*/
/*#                               I2C Related                                #*/
/*############################################################################*/

// Use SPI mode to read/write LIS3DH


#define LIS3DH_REG_STATUS1       0x07
#define LIS3DH_REG_OUTADC1_L     0x08
#define LIS3DH_REG_OUTADC1_H     0x09
#define LIS3DH_REG_OUTADC2_L     0x0A
#define LIS3DH_REG_OUTADC2_H     0x0B
#define LIS3DH_REG_OUTADC3_L     0x0C
#define LIS3DH_REG_OUTADC3_H     0x0D
#define LIS3DH_REG_INTCOUNT      0x0E
#define LIS3DH_REG_WHOAMI        0x0F
#define LIS3DH_REG_TEMPCFG       0x1F
#define LIS3DH_REG_CTRL1         0x20
#define LIS3DH_REG_CTRL2         0x21
#define LIS3DH_REG_CTRL3         0x22
#define LIS3DH_REG_CTRL4         0x23
#define LIS3DH_REG_CTRL5         0x24
#define LIS3DH_REG_CTRL6         0x25
#define LIS3DH_REG_REFERENCE     0x26
#define LIS3DH_REG_STATUS2       0x27
#define LIS3DH_REG_OUT_X_L       0x28
#define LIS3DH_REG_OUT_X_H       0x29
#define LIS3DH_REG_OUT_Y_L       0x2A
#define LIS3DH_REG_OUT_Y_H       0x2B
#define LIS3DH_REG_OUT_Z_L       0x2C
#define LIS3DH_REG_OUT_Z_H       0x2D
#define LIS3DH_REG_FIFOCTRL      0x2E
#define LIS3DH_REG_FIFOSRC       0x2F
#define LIS3DH_REG_INT1CFG       0x30
#define LIS3DH_REG_INT1SRC       0x31
#define LIS3DH_REG_INT1THS       0x32
#define LIS3DH_REG_INT1DUR       0x33
#define LIS3DH_REG_CLICKCFG      0x38
#define LIS3DH_REG_CLICKSRC      0x39
#define LIS3DH_REG_CLICKTHS      0x3A
#define LIS3DH_REG_TIMELIMIT     0x3B
#define LIS3DH_REG_TIMELATENCY   0x3C
#define LIS3DH_REG_TIMEWINDOW    0x3D
#define LIS3DH_REG_ACTTHS        0x3E
#define LIS3DH_REG_ACTDUR        0x3F


typedef enum
{
  LIS3DH_RANGE_16_G          = 0x3,   // +/- 16g
  LIS3DH_RANGE_8_G           = 0x2,   // +/- 8g
  LIS3DH_RANGE_4_G           = 0x1,   // +/- 4g
  LIS3DH_RANGE_2_G           = 0x0    // +/- 2g (default value)
} lis3dh_range_t;

typedef enum
{
  LIS3DH_AXIS_X         = 0x0,
  LIS3DH_AXIS_Y         = 0x1,
  LIS3DH_AXIS_Z         = 0x2
} lis3dh_axis_t;

typedef enum
{
  LIS3DH_FIFO_BYPASS     = 0x0,   // BYPASS mode (default value)
  LIS3DH_FIFO_ON         = 0x1,
  LIS3DH_FIFO_STREAM     = 0x2,
  LIS3DH_FIFO_TRI        = 0x3
} lis3dh_fifo_mode_t;


/* Used with register 0x2A (LIS3DH_REG_CTRL_REG1) to set bandwidth */
typedef enum
{
  LIS3DH_DATARATE_400_HZ     = 0x7, //  400Hz
  LIS3DH_DATARATE_200_HZ     = 0x6, //  200Hz
  LIS3DH_DATARATE_100_HZ     = 0x5, //  100Hz
  LIS3DH_DATARATE_50_HZ      = 0x4, //   50Hz
  LIS3DH_DATARATE_25_HZ      = 0x3, //   25Hz
  LIS3DH_DATARATE_10_HZ      = 0x2, // 10 Hz
  LIS3DH_DATARATE_1_HZ       = 0x1, // 1 Hz
  LIS3DH_DATARATE_POWERDOWN  = 0x0,
  LIS3DH_DATARATE_LOWPOWER_1K6HZ  = 0x8,
  LIS3DH_DATARATE_LOWPOWER_5KHZ  =  0x9

} lis3dh_dataRate_t;


extern int16_t LIS3DH_Raw_Data_X;
extern int16_t LIS3DH_Raw_Data_Y;
extern int16_t LIS3DH_Raw_Data_Z;

extern float LIS3DH_X_G;
extern float LIS3DH_Y_G;
extern float LIS3DH_Z_G;


uint8_t LIS3DH_write_multi(uint8_t address, uint8_t *pdata, uint8_t count);
uint8_t LIS3DH_read_multi(uint8_t address, uint8_t *pdata, uint8_t count);
uint8_t LIS3DH_writeReg8(uint8_t reg, uint8_t value);
uint8_t LIS3DH_readReg8(uint8_t reg);

uint8_t LIS3DH_init(void);
void LIS3DH_setDataRate(lis3dh_dataRate_t dataRate);


lis3dh_dataRate_t LIS3DH_getDataRate(void);
void LIS3DH_setRange(lis3dh_range_t range);
lis3dh_range_t LIS3DH_getRange(void);
void LIS3DH_setFifoMode(lis3dh_fifo_mode_t mode);
lis3dh_fifo_mode_t LIS3DH_getFifoMode(void);
uint8_t LIS3DH_get_measure_XYZ(float *X_G, float *Y_G, float *Z_G);


#endif
