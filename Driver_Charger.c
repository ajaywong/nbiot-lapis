/*  */
#include <string.h>
#include <stdio.h>

/* MCU */
#include "rdwr_reg.h"
#include "mcu.h"

/*   */
#include "User_System.h"
#include "User_Generic.h"


void ChargerIc_Initialize(void)
{
    /*############################################################################*/
    // P02 - BATIC_EN1
    write_bit( P02D, 0 );
    clear_bit( P02DIR );		/* Output */
    set_bit( P02C0 );			/* CMOS */
    set_bit( P02C1 );
    clear_bit( P02MD0 );		/* GPIO mode */
    clear_bit( P02MD1 );

    // P03 - BATIC_EN2
    write_bit( P03D, 1);
    clear_bit( P03DIR );		/* Output */
    set_bit( P03C0 );			/* CMOS */
    set_bit( P03C1 );
    clear_bit( P03MD0 );		/* GPIO mode */
    clear_bit( P03MD1 );

    // P32 - BATIC_NCHG
    write_bit( P32D, 0 );
    set_bit( P32DIR );			/* Input */
    set_bit( P32C0 );			/* Hi-Z */
    set_bit( P32C1 );
    //clear_bit( P32C0 );			/* Internal pull-high - avoid high current at floating pin */
    //set_bit( P32C1 );
    clear_bit( P32MD0 );		/* GPIO mode */
    clear_bit( P32MD1 );

    // P33 - BATIC_NPGOOD
    write_bit( P33D, 0 );
    set_bit( P33DIR );			/* Input */
    set_bit( P33C0 );			/* Hi-Z */
    set_bit( P33C1 );
    //clear_bit( P33C0 );			/* Internal pull-high - avoid high current at floating pin  */
    //set_bit( P33C1 );
    clear_bit( P33MD0 );		/* GPIO mode */
    clear_bit( P33MD1 );

    // P23 - BATIC_CE - (LOW = enable battery charging)
    write_bit( P23D, 1 );
    clear_bit( P23DIR );			/* Output */
    set_bit( P23C0 );				/* CMOS */
    set_bit( P23C1 );
    clear_bit( P23MD0 );			/* GPIO mode */
    clear_bit( P23MD1 );


	//EN2=0, EN1=0 => 100mA charging current
	//EN2=0, EN1=1 => 500mA charging current
	//EN2=1; EN1=0 => charging current depends on external resistor 890ohm = 1A charging current
	//EN2=1, EN1=1 => standby mode
	BATIC_EN1_OFF;
	BATIC_EN2_ON;
	BATIC_CE_ON;
}





