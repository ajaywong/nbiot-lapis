

#include <stdio.h>
#include <String.h>
#include <stdlib.h>

#include "rdwr_reg.h"
#include "mcu.h"
#include "clock.h"
#include "wdt.h"
#include "irq.h"

#include "ssio_common.h"
#include "ssio0.h"


/* User Include */
#include "User_System.h"
#include "User_Generic.h"

#include "Driver_LIS3DSH.h"

// Chip select to G-sensor (LI3DH): Active-LOW = CHIP SELECT ENABLE
#define SPI_CS_OFF			P47D = 1
#define SPI_CS_ON			P47D = 0


int16_t LIS3DSH_Raw_Data_X = 0;
int16_t LIS3DSH_Raw_Data_Y = 0;
int16_t LIS3DSH_Raw_Data_Z = 0;

float LIS3DSH_X_G = 0;
float LIS3DSH_Y_G = 0;
float LIS3DSH_Z_G = 0;



uint8_t LIS3DSH_write_multi(uint8_t address, uint8_t *pdata, uint8_t count)
{

    uint8_t status, i;
    uint16_t retry;
	uint8_t addr;
	uint8_t spi_data[SPI_DATA_SIZE_MAX];


	SPI_CS_ON;

	Ssio0_flag = 0;

	if(count < 1 || count >= 32)
		return 1;

	addr = ~0x80 & address; // bit7 = 0: Write to 3G sensor

// This is no need to set
/*
	if(count == 1)
		addr = ~0x40 & addr;  // bit6 = 0: not auto-increment address
	else
		addr = 0x40 | addr; // bit6 = 1: auto-increment address
*/

	spi_data[0] = addr;
	memcpy(&spi_data[1], pdata, (SPI_DATA_SIZE_MAX-1));


	status = ssio0_start( SSIO_MODE2, (void *)0, (unsigned char *)spi_data, (count+1), (void *)0 );

    retry = 0;
    while ((Ssio0_flag != 1) || (status != SSIO_R_OK))
    {
        wdt_clear();
        //Avoid dead loop here
        retry++;
        if (retry > 2000)
        {
            status = 1;

            DebugPrint("!!SPI WriteLoopError!!\r\n");
            retry = 0;
            return status;
        }
    }

	SPI_CS_OFF;

    return status;
}



uint8_t LIS3DSH_read_multi(uint8_t address, uint8_t *pdata, uint8_t count)
{

    uint8_t status, i;
    uint16_t retry;
	uint8_t addr;
	uint8_t spi_data[SPI_DATA_SIZE_MAX];

	SPI_CS_ON;

	Ssio0_flag = 0;

	if(count < 1 || count >= 32)
		return 1;

	addr = 0x80 | address; // bit7 = 1: Write to 3G sensor

// This is no need to set
/*	if(count == 1)
		addr = ~0x40 & addr;  // bit6 = 0: not auto-increment address
	else
		addr = 0x40 | addr; // bit6 = 1: auto-increment address
*/

	memset(spi_data, 0, SPI_DATA_SIZE_MAX);
	spi_data[0] = addr;

	status = ssio0_start( SSIO_MODE3, (unsigned char *)pdata, (unsigned char *)spi_data, (count+1), (void *)0 );

    retry = 0;
    while ((Ssio0_flag != 1) || (status != SSIO_R_OK))
    {
        wdt_clear();
        //Avoid dead loop here
        retry++;
        if (retry > 2000)
        {
            status = 1;

            DebugPrint("!!SPI ReadLoopError!!\r\n");
            retry = 0;
            return status;
        }
    }

	SPI_CS_OFF;

    return status;
}



uint8_t LIS3DSH_writeReg8(uint8_t reg, uint8_t value)
{
    uint8_t status;
    Spi_txData[0] = value;

    status = LIS3DSH_write_multi(reg, Spi_txData, 1); // actually send 2 byte (address and data) in SPI
    if(status == 1)
    {
		DebugPrint("!!SPI Write Reg8!!\r\n");
    }

    return status;
}

uint8_t LIS3DSH_readReg8(uint8_t reg)
{
    uint8_t status;

    status = LIS3DSH_read_multi(reg, Spi_rxData, 1);  // actually send 1 byte (address) and read 1 byte (data) in SPI
    if(status == 1)
    {
		DebugPrint("!!SPI Read Reg8!!\r\n");
    }

    return Spi_rxData[1]; // actual data store in array [1]
}



uint8_t LIS3DSH_init(void)
{

	/* Check connection */
	uint8_t status = 0;

	status = LIS3DSH_readReg8(LIS3DSH_REG_WHOAMI);

	if (status != 0x3F)
	{
		/* No LIS3DSH detected ... return false */
		return 1;
	}

	// Normal power mode, all axes enabled, 50 Hz ODR
	LIS3DSH_writeReg8(LIS3DSH_REG_CTRL4, 0x5F);
	//status = LIS3DSH_readReg8(LIS3DSH_REG_CTRL4);
	//sprintf(text_buf, "CTRL4=0x%x\r\n", status);
	//DebugPrint(text_buf);

	// 400 Hz antialias filter, +/- 2g FS range, self-test disabled, 4-wire SPI mode
	LIS3DSH_writeReg8(LIS3DSH_REG_CTRL5, 0x80);
	//status = LIS3DSH_readReg8(LIS3DSH_REG_CTRL5);
	//sprintf(text_buf, "CTRL5=0x%x\r\n", status);
	//DebugPrint(text_buf);

	// configure FIFO for bypass mode
	LIS3DSH_writeReg8(LIS3DSH_REG_FIFOCTRL, 0);
	//status = LIS3DSH_readReg8(LIS3DSH_REG_FIFOCTRL);
	//sprintf(text_buf, "FIFOCTRL=0x%x\r\n", status);
	//DebugPrint(text_buf);


	// disable FIFO, enable register address auto-increment
	LIS3DSH_writeReg8(LIS3DSH_REG_CTRL6, 0x10);
	//status = LIS3DSH_readReg8(LIS3DSH_REG_CTRL6);
	//sprintf(text_buf, "CTRL6=0x%x\r\n", status);
	//DebugPrint(text_buf);

	//LIS3DSH_read_multi(0x0D, Spi_rxData, 6);

	// 3.125Hz rate
	//LIS3DSH_setDataRate(LIS3DSH_DATARATE_3_125_HZ);
	// 25Hz rate
	LIS3DSH_setDataRate(LIS3DSH_DATARATE_25_HZ);

	// No FIFO used
	LIS3DSH_setFifoMode(LIS3DSH_FIFO_BYPASS);

	// No signal on INT1 - use polling register
	LIS3DSH_writeReg8(LIS3DSH_REG_CTRL3, 0x00);


	return 0;
}



void LIS3DSH_setDataRate(lis3dsh_dataRate_t dataRate)
{
  uint8_t ctl4 = LIS3DSH_readReg8(LIS3DSH_REG_CTRL4);

  ctl4 &= ~(0xF0); // mask off bits
  ctl4 |= (dataRate << 4);
  LIS3DSH_writeReg8(LIS3DSH_REG_CTRL4, ctl4);
}

lis3dsh_dataRate_t LIS3DSH_getDataRate(void)
{
  return (lis3dsh_dataRate_t)((LIS3DSH_readReg8(LIS3DSH_REG_CTRL4) >> 4)& 0x0F);
}


void LIS3DSH_setRange(lis3dsh_range_t range)
{
  uint8_t r = LIS3DSH_readReg8(LIS3DSH_REG_CTRL5);
  r &= ~(0x38);
  r |= range << 3;
  LIS3DSH_writeReg8(LIS3DSH_REG_CTRL4, r);
}


lis3dsh_range_t LIS3DSH_getRange(void)
{
  /* Read the data format register to preserve bits */
  return (lis3dsh_range_t)((LIS3DSH_readReg8(LIS3DSH_REG_CTRL5) >> 3) & 0x07);
}

void LIS3DSH_setFifoMode(lis3dsh_fifo_mode_t mode)
{
  uint8_t m = LIS3DSH_readReg8(LIS3DSH_REG_FIFOCTRL);
  m &= ~(0xE0);
  m |= mode << 5;
  LIS3DSH_writeReg8(LIS3DSH_REG_FIFOCTRL, m);
}


lis3dsh_fifo_mode_t LIS3DSH_getFifoMode(void)
{
   return (lis3dsh_fifo_mode_t)((LIS3DSH_readReg8(LIS3DSH_REG_FIFOCTRL) >> 5) & 0x07);
}



uint8_t LIS3DSH_get_measure_XYZ(float *X_G, float *Y_G, float *Z_G)
{
	uint8_t status=0;
	uint8_t range;
	uint16_t divider;

	status = LIS3DSH_readReg8(LIS3DSH_REG_STATUS) & 0x08; // check if XYZ data available

	if(status)
	{
		LIS3DSH_read_multi(LIS3DSH_REG_OUT_X_L, Spi_rxData, 6);

		LIS3DSH_Raw_Data_X = Spi_rxData[1];
		LIS3DSH_Raw_Data_X |= ((uint16_t)Spi_rxData[2]) << 8;
		LIS3DSH_Raw_Data_Y = Spi_rxData[3];
		LIS3DSH_Raw_Data_Y |= ((uint16_t)Spi_rxData[4]) << 8;
		LIS3DSH_Raw_Data_Z = Spi_rxData[5];
		LIS3DSH_Raw_Data_Z |= ((uint16_t)Spi_rxData[6]) << 8;

		range = LIS3DSH_getRange();
		divider = 1;

		// convert sensitivity level from mg to g
		// e.g. +/-2G = total 4G, at 16-bit = 65535 level, 4G/65535 = each LSB equals 0.00006g change


  		if (range == LIS3DSH_RANGE_16_G) divider = 1370;      // 1/(0.73/1000)
  		else if (range == LIS3DSH_RANGE_8_G) divider = 4167;  // 1/(0.24/1000)
  		else if (range == LIS3DSH_RANGE_6_G) divider = 5556;  // 1/(0.18/1000)
  		else if (range == LIS3DSH_RANGE_4_G) divider = 8333;  // 1/(0.12/1000)
  		else if (range == LIS3DSH_RANGE_2_G) divider = 16667; // 1/(0.06/1000)

		// convert from raw data to g-value
		//LIS3DSH_X_G = (float)LIS3DSH_Raw_Data_X / divider;
  		//LIS3DSH_Y_G = (float)LIS3DSH_Raw_Data_Y / divider;
		//LIS3DSH_Z_G = (float)LIS3DSH_Raw_Data_Z / divider;


		*X_G = (float)LIS3DSH_Raw_Data_X / divider;
  		*Y_G = (float)LIS3DSH_Raw_Data_Y / divider;
		*Z_G = (float)LIS3DSH_Raw_Data_Z / divider;

		//sprintf(text_buf, "X=0x%x, %d\r\n", LIS3DSH_Raw_Data_X, LIS3DSH_Raw_Data_X);
		//DebugPrint(text_buf);
		//sprintf(text_buf, "Y=0x%x, %d\r\n", LIS3DSH_Raw_Data_Y, LIS3DSH_Raw_Data_Y);
		//DebugPrint(text_buf);
		//sprintf(text_buf, "Z=0x%x, %d\r\n", LIS3DSH_Raw_Data_Z, LIS3DSH_Raw_Data_Z);
		//DebugPrint(text_buf);

		DebugPrint("X=%.2f, Y=%.2f, Z=%.2f\r\n", *X_G, *Y_G, *Z_G);

		if((*X_G < -2 || *X_G >2) ||
			(*Y_G < -2 || *Y_G >2) ||
			(*Z_G < -2 || *Z_G >2))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 1;
	}
}




