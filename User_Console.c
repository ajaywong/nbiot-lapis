

#include <stdio.h>
#include <string.h>


/* MCU */
#include "rdwr_reg.h"
#include "mcu.h"
#include "wdt.h"
#include "irq.h"

#include "uart0.h"
#include "uartf0.h"

#include "User_Console.h"
#include "User_Generic.h"
#include "User_System.h"
#include "Driver_Sigfox.h"

/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/

unsigned char uart0_Rx_flag;
unsigned char uart0_Tx_flag;

unsigned char uartf0_Rx_flag;
unsigned char uartf0_Tx_flag;

unsigned char uart0_Buffer[UART0_BUFFSIZE];
unsigned char uart0_wd_Buffer[UART0_BUFFSIZE];

static unsigned char     uart0_rxSize;
static unsigned char     uart0_txSize;

static unsigned char     uartf0_rxSize;
static unsigned char     uartf0_txSize;

extern unsigned char s_rxBuffer[];
extern unsigned char s_txBuffer[];


static void Uartf0_RXD_CB( unsigned int size, unsigned char errStat );
static void Uartf0_TXD_CB( unsigned int size, unsigned char errStat );

static void s_procUartWrite_console( unsigned int size, unsigned char errStat );
static void s_procUartRead_console( unsigned int size, unsigned char errStat );


static void Uartf0_RXD_CB( unsigned int size, unsigned char errStat )
{
	errStat  = errStat;
	uartf0_rxSize = size;
    uartf0_Rx_flag = FLAG_SET;  /* End of communication */
}



static void Uartf0_TXD_CB( unsigned int size, unsigned char errStat )
{
	errStat  = errStat;
	uartf0_txSize = size;
	uartf0_Tx_flag = FLAG_SET;  /* End of communication */
}




static void s_procUartRead_console( unsigned int size, unsigned char errStat )
{
    errStat = errStat;
	/* get the size of valid data */
	uart0_rxSize = size;
    uart0_Rx_flag = FLAG_SET;
}


static void s_procUartWrite_console( unsigned int size, unsigned char errStat )
{
	errStat  = errStat;
	uart0_txSize = size;
	uart0_Tx_flag = FLAG_SET;  /* End of communication */
}



unsigned char TestMode_SigfoxTest(void)
{
    uart0_Rx_flag = FLAG_CLR;
    reply_count = 1;
    uart0_read( uart0_Buffer, UART0_BUFFSIZE, s_procUartRead_console );

//    while(TEST_MODE_D == 0)
    while(1)
    {
        wdt_clear();

        // Receive Console Command
        if(uart0_Rx_flag)
        {
            uart0_Rx_flag = FLAG_CLR;

            if(uart0_rxSize > 0)
            {
                uartf0_txSize = uart0_rxSize;
                memcpy(s_txBuffer, uart0_Buffer, uartf0_txSize);

                if(strncmp(uart0_Buffer, ":TMD-RF-END", 11) == 0)
                {
                    break;
                }
                else
                {
                    // TX to Module
                    uartf0_Tx_flag = FLAG_CLR;
                    uartf0_write( s_txBuffer, uartf0_txSize, Uartf0_TXD_CB );
                }
            }
        }

        // Tx to Module finished
        if(uartf0_Tx_flag)
        {
            uartf0_Tx_flag = FLAG_CLR;

            //Module receive
            uartf0_Rx_flag = FLAG_CLR;
            reply_count = 1;
            //uartf0_clearReadFifo();
            uartf0_read( s_rxBuffer, UARTF_RW_MAX, Uartf0_RXD_CB);
        }

        // receive Module data finish
        if(uartf0_Rx_flag == FLAG_SET)
        {
            if(uartf0_rxSize > 0)
            {
                uart0_txSize = uartf0_rxSize;
                memcpy(uart0_wd_Buffer, s_rxBuffer, uart0_txSize);
            }

            // Start Next Receive
            reply_count = 1;
            uartf0_Rx_flag = FLAG_CLR;
            uartf0_read( s_rxBuffer, UARTF_RW_MAX, Uartf0_RXD_CB);

            // Send to Console
            uart0_Tx_flag = FLAG_CLR;
            uart0_write( uart0_wd_Buffer, uart0_txSize, s_procUartWrite_console);
            while(uart0_Tx_flag == FLAG_CLR)
            {
                wdt_clear();
            }
        }

        // Start Read console
        if(uart0_Tx_flag)
        {
            uart0_Tx_flag = FLAG_CLR;

            uart0_Rx_flag = FLAG_CLR;
            uart0_read( uart0_Buffer, UART0_BUFFSIZE, s_procUartRead_console );
        }

    }

    return 0;
}

/*
unsigned char testMode_Read(void)
{
    uint8_t retVal = RET_OK;

    memset(uart0_Buffer, 0x00, UART0_BUFFSIZE);

    uart0_Rx_flag = FLAG_CLR;

    timeStamp = SysTickets;
    uart0_read( uart0_Buffer, UART0_BUFFSIZE, s_procUartRead );
    while(uart0_Rx_flag == FLAG_CLR)
    {
        wdt_clear();
        if(SysTickets - timeStamp > 5)
        {
            retVal = RET_ERROR;
            break;
        }
    }

	return retVal;
}*/

void testMode_Read(void)
{
    memset(uart0_Buffer, 0x00, UART0_BUFFSIZE);  

    uart0_Rx_flag = FLAG_CLR;
    uart0_read( uart0_Buffer, UART0_BUFFSIZE, s_procUartRead_console );
}

unsigned char testMode_getData(void)
{
    return uart0_Rx_flag;
}


void testMode_Write(uint8_t len)
{
    uart0_Tx_flag = FLAG_CLR;
	uart0_write( uart0_wd_Buffer, len, s_procUartWrite_console);
    while(uart0_Tx_flag == FLAG_CLR)
    {
        wdt_clear();
    }
}


/*******************************************************************************
	Routine Name	: write
	Form			: int write(int handle, unsigned char *buffer, unsigned int len)
	Parameters		: int handle
					  unsigned char *buffer
					  unsigned int len
	Return value	: int
	Initialization	: None.
	Description		: The write function writes len bytes of data from the area specified by buffer to UART0.
******************************************************************************/
int write(int handle, unsigned char *buffer, unsigned int len)
{
	uart0_Tx_flag = FLAG_CLR;
	uart0_write( buffer, len, s_procUartWrite_console);
    while(uart0_Tx_flag == FLAG_CLR)
    {
    }

	return len;
}










