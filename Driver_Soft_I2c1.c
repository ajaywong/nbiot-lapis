
#include "mcu.h"
#include "wdt.h"
#include "rdwr_reg.h"
#include "Driver_Soft_I2c1.h"


/* Software I2C Master 1 */
#define SWI2C1_SDA_PIN       P40D
#define SWI2C1_SDA_DIR       P40DIR
#define SWI2C1_SDA_C0        P40C0
#define SWI2C1_SDA_C1        P40C1
#define SWI2C1_SDA_MD0       P40MD0
#define SWI2C1_SDA_MD1       P40MD1

#define SWI2C1_SCL_PIN       P41D
#define SWI2C1_SCL_DIR       P41DIR
#define SWI2C1_SCL_C0        P41C0
#define SWI2C1_SCL_C1        P41C1
#define SWI2C1_SCL_MD0       P41MD0
#define SWI2C1_SCL_MD1       P41MD1

#define GPIODELAYCYCLES     2

#define I2CDELAY 			SWI2CMST_delay()    // Macro for GPIO change delay

#define SDA_H	            set_bit( SWI2C1_SDA_DIR );
#define SDA_L		        {clear_bit(SWI2C1_SDA_PIN);clear_bit( SWI2C1_SDA_DIR );}

#define SCL_H		        set_bit( SWI2C1_SCL_DIR );
#define SCL_L    		    {clear_bit(SWI2C1_SCL_PIN);clear_bit( SWI2C1_SCL_DIR );}

#define NACK    0
#define ACK     1
#define BIT7    0x80
#define BIT0    0x01


/*-----------------------------------------------------------------------------------------
 Local Function Prototype
-----------------------------------------------------------------------------------------*/
static void SWI2CMST_delay(void);
static void SWI2CMST_start(void);
static void SWI2CMST_stop(void);

static unsigned char SWI2CMST_txByte(unsigned char data);
static unsigned char SWI2CMST_rxByte(unsigned char ack);



/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
********************************************************************************************/
void swI2cMst1_init(void)
{
    /* SDA */
    clear_bit( SWI2C1_SDA_PIN );       /* Low */
    set_bit( SWI2C1_SDA_DIR );         /* Input  */
    clear_bit( SWI2C1_SDA_C0 );      /* N-channel open drain output */
    set_bit( SWI2C1_SDA_C1 );
    clear_bit( SWI2C1_SDA_MD0 );     /* General-purpose input/output mode */
    clear_bit( SWI2C1_SDA_MD1 );


    /* SCL */
    clear_bit(SWI2C1_SCL_PIN);       /* Low */
    set_bit(SWI2C1_SCL_DIR);         /* Input  */
    clear_bit( SWI2C1_SCL_C0 );      /* N-channel open drain output */
    set_bit( SWI2C1_SCL_C1 );
    clear_bit( SWI2C1_SCL_MD0 );     /* General-purpose input/output mode */
    clear_bit( SWI2C1_SCL_MD1 );
}


/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
********************************************************************************************/
void SWI2CMST_delay(void)
{
    //unsigned char delay = 2;
    unsigned char delay = 5;

    while(delay--);
}


/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
****************************************************************************/
void SWI2CMST_start(void)
{
    SDA_H;                                    // SDA = 1
    I2CDELAY;                                 // Quick delay
    SCL_H;                                    // SCL = 1
    I2CDELAY;                                 // Quick delay
    SDA_L;                                    // SDA = 0
    I2CDELAY;                                 // Quick delay
    SCL_L;                                    // SCL = 0
    I2CDELAY;                                 // Quick delay
}


/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
********************************************************************************************/
void SWI2CMST_stop(void)
{
    SDA_L;                                    // SDA = 0
    I2CDELAY;                                 // Quick delay
    SCL_H;                                    // SCL = 1
    I2CDELAY;                                 // Quick delay
    SDA_H;                                    // SDA = 1
    I2CDELAY;                                 // Quick delay
}



/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
********************************************************************************************/
unsigned char SWI2CMST_txByte(unsigned char data)
{
    unsigned char bits, temp, ack;
    unsigned int delay = I2C_TIMEOUT;

    SCL_L;                                    // SCL = 0
    temp = data;                              // Initialize temp variable
    bits = 0x08;                              // Load I2C bit counter
    while (bits != 0x00)                      // Loop until all bits are shifted
    {
        if (temp & BIT7)                        // Test data bit
        {
            SDA_H;                                // SDA = 1
        }
        else
        {
            SDA_L;                                // SDA = 0
        }

        I2CDELAY;                               // Quick delay
        SCL_H;                                  // SCL = 1

        for(;;)
        {
            delay--;
            if(delay == 0)
            {
                return I2C_ERROR_ACK;
            }
            if(get_bit(SWI2C1_SCL_PIN) == 1)    // Wait for any SCL clock stretching
            {
                break;
            }
        }

        I2CDELAY;                               // Quick delay
        temp = (unsigned char)(temp << 1);            // Shift bits 1 place to the left
        SCL_L;                                  // SCL = 0
        bits = (unsigned char)(bits - 1);             // Loop until 8 bits are sent
    }
    I2CDELAY;
    SDA_H;                                    // SDA = 1
    SCL_H;                                    // SCL = 1
    while (get_bit(SWI2C1_SCL_PIN) == 0);      // Wait for any SCL clock stretching
    I2CDELAY;                                 // Quick delay
    ack= get_bit(SWI2C1_SDA_PIN);
    SCL_L;                                    // SCL = 0
    if (ack)                                   // Return ACK state to calling app
    {
        return I2C_ERROR_ACK;
    }
    else
    {
        return I2C_OK;
    }
}



/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
********************************************************************************************/
unsigned char SWI2CMST_rxByte(unsigned char ack)
{
    unsigned char bits, data = 0;
    unsigned int delay = I2C_TIMEOUT;

    SDA_H;                                    // SDA = 1
    bits = 0x08;                              // Load I2C bit counter
    while (bits > 0)                          // Loop until all bits are read
    {
        SCL_H;                                  // SCL = 1
        // Wait for any SCL clock stretching
        for(;;)
        {
            delay--;
            if(delay == 0)
            {
                return I2C_ERROR_ACK;
            }
            if(get_bit(SWI2C1_SCL_PIN) == 1)    // Wait for any SCL clock stretching
            {
                break;
            }
        }

        I2CDELAY;                               // Quick delay
        data = (unsigned char) (data << 1);                     // Shift bits 1 place to the left
        if(get_bit(SWI2C1_SDA_PIN)) // Check digital input
        {
            data = (unsigned char)(data + 1);                  // If input is high, store a '1'
        }
        SCL_L;                                  // SCL = 0
        I2CDELAY;                               // Quick delay
        bits = (unsigned char)(bits - 1);                      // Decrement I2C bit counter
    }
    if (ack)                                  // Need to send ACK to Slave?
    {
        SDA_L;                                  // Yes, so pull SDA low
    }
    else
    {
        SDA_H;                                  // No, so keep SDA high
    }
    SCL_H;                                    // SCL = 1
    I2CDELAY;                                 // Equivalent to sending N(ACK)
    SCL_L;                                    // SCL = 0
    SDA_H;                                    // SDA = 1

    return (data);                            // Return 8-bit data byte
}




/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
********************************************************************************************/
unsigned char swI2cMst1_Read(unsigned char SlaveAddress, unsigned char reg, unsigned char* data, unsigned short len)
{
    unsigned short  i;
    unsigned char ret = I2C_ERROR_ACK;

    wdt_clear();

    SWI2CMST_start();                  // Send Start condition

    if(SWI2CMST_txByte( (unsigned char)((SlaveAddress) & (~BIT0)) ) == I2C_OK ) // [ADDR] + R/W bit = 0
    {
        if(SWI2CMST_txByte(reg) == I2C_OK )//
        {
            SWI2CMST_start();

            if(SWI2CMST_txByte( (unsigned char)((SlaveAddress) | BIT0) ) == I2C_OK)
            {
                for (i = 0; i < len; i++)
                {
                    if (i == (len - 1))
                    {
                        *data = SWI2CMST_rxByte(NACK);// Read last 8-bit data with no ACK
                    }
                    else
                    {
                        *data = SWI2CMST_rxByte(ACK);// Read 8-bit data & then send ACK
                    }
                    data++;
                }

                ret = I2C_OK;
            }
        }
    }

    SWI2CMST_stop();                   // Send Stop condition

    return ret;
}




/*******************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
********************************************************************************************/
unsigned char swI2cMst1_Write(unsigned char SlaveAddress, unsigned char reg, unsigned char* data, unsigned short len)
{
    unsigned short  i;
    unsigned char ret = I2C_OK;

     wdt_clear();

    SWI2CMST_start();

    if(SWI2CMST_txByte( (unsigned char)((SlaveAddress) & (~BIT0)) ) == I2C_OK) // [ADDR] + R/W bit = 0
    {
        if(SWI2CMST_txByte(reg) == I2C_OK)
        {
            for (i = 0; i < len; i++)
            {
                if(SWI2CMST_txByte(*(data)) == I2C_OK)        // Send data and ack
                {
                    data++;         // Increment pointer to next element
                }
                else
                {
                    ret = I2C_ERROR_ACK;
                    break;
                }
            }
        }
        else
        {
            ret = I2C_ERROR_ACK;
        }
    }
    else
    {
        ret = I2C_ERROR_ACK;
    }

    SWI2CMST_stop();                 // Yes, send STOP condition
    I2CDELAY;

    return ret;
}


