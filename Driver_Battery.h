/*****************************************************************************
 Driver_Battery.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _DRIVER_BATTERY_H_
#define _DRIVER_BATTERY_H_



/* I2C chip addresses */
#define BQ27541_ADDRESS		    0xAA

/* BQ27541 registers */
#define BQ27541_REG_CONTROL		(0x00)		/* Control register */

#define BQ27541_REG_DEVICE_TYPE (0x01)
#define BQ27541_REG_FW_VERSION	(0x02)
#define BQ27541_REG_TEMP		(0x06)		/* Temperature (in 0.1 K) */
#define BQ27541_REG_VOLTAGE		(0x08)		/* Voltage (in mV) */
#define BQ27541_REG_FLAGS		(0x0a)		/* Flags */
#define BQ27541_REG_FLAGS_DSG	(1 << 0)	/* Discharging */
#define BQ27541_REG_FLAGS_FC	(1 << 9)	/* Full charge */
#define BQ27541_REG_FLAGS_OTD	(1 << 14)	/* Over-temp under discharge */
#define BQ27541_REG_FLAGS_OTC	(1 << 15)	/* Over-temp under charge */

#define BQ27541_REG_NAC			(0x0C)		// Nom Available Capacity
#define BQ27541_REG_FAC			(0x0E)		// Full Available Capacity
#define BQ27541_REG_RMC			(0x10)		// Remaining Capacity
#define BQ27541_REG_FCC			(0x12)		// Full Charge Capacity

#define BQ27541_REG_CURRENT		(0x14)		/* Current (in mA) */
#define BQ27541_REG_SOC			(0x2C)		/* State of Charge (in %) */
#define BQ27541_REG_CC          (0x2A)
#define BQ27541_REG_TEMP        (0x06)
#define BQ27541_REG_INTTEMP     (0x28)


#define BQ27541_ID_H            0x05
#define BQ27541_ID_L            0x41


/* Flag */
#define FLAG_SET            ( 1 )
#define FLAG_CLR            ( 0 )

#define FLAG_ON             ( 1 )
#define FLAG_OFF            ( 0 )


#define I2C_OK              0
#define I2C_ERROR_ACK       1
#define I2C_ERROR_TIMEOUT   2




/*
extern short FG_STATUS;
extern short FG_ID;
extern short FG_FW_Version;
extern short FG_HW_version;
extern short FG_Int_Temp;
extern short FG_Bat_Temp;
extern short FG_Bat_Current;
extern short FG_Bat_Volt;
extern short FG_Bat_Cyc_Count;
*/

extern short FG_Bat_SOC;

/*
extern short FG_Bat_NAC;  // Nom Available Capacity
extern short FG_Bat_FAC;  // Full Available Capacity
extern short FG_Bat_RMC;  // Remaining Capacity
extern short FG_Bat_FCC;  // Full Charge Capacity
*/

/* Function*/
unsigned char bq27541_get_ID(short *fg_id);

unsigned char bq27541_get_FW_version(void);
unsigned char bq27541_get_HW_version(void);

unsigned char bq27541_get_NAC(void);
unsigned char bq27541_get_FAC(void);
unsigned char bq27541_get_RMC(void);
unsigned char bq27541_get_FCC(void);
unsigned char bq27541_get_status(void);

unsigned char bq27541_get_BatterySOC(short *fg_bat_soc);

unsigned char bq27541_get_BatteryCurrent(void);
unsigned char bq27541_get_BatteryCycleCount(void);
unsigned char bq27541_get_BatterInfo(void);

unsigned char bq27541_get_BatteryVolt(void);
unsigned char bq27541_get_BatteryTemp(void);

unsigned char bq27541_SET_HIBERNATE(void);

#endif
