/*****************************************************************************
Driver_TempSensor

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */

#include <stdio.h>
#include <String.h>
#include <stdlib.h>

/*  API  */
#include "mcu.h"
#include "wdt.h"
#include "irq.h"
#include "clock.h"
#include "i2c0.h"

/*  */
#include "User_System.h"
#include "User_Generic.h"

/*  */
#include "Driver_TempSensor.h"
#include "Driver_Soft_I2c.h"


#define READ_TEMP_SENSOR_COUNT 8


uint16_t SHT3x_temperature = 0;
uint16_t SHT3x_humidity = 0;
uint8_t SHT3x_temperature_crc = 0;
uint8_t SHT3x_humidity_crc = 0;

float SHT3x_temperature_C_ft = 0;
float SHT3x_temperature_F_ft = 0;
float SHT3x_humidity_ft = 0;

uint16_t SHT3x_chip_status = 0;
uint8_t SHT3x_chip_status_crc = 0;


//uint8_t SHT3x_s_Status;   /**< The status of i2c             */
//uint8_t SHT3x_dataLen;    /**< number of byte sended/recieved       */

/*############################################################################*/
/*#                                  Driver                                  #*/
/*############################################################################*/

uint8_t SHT3x_write_multi(uint8_t address, uint8_t *pdata, uint8_t count)
{

    uint8_t status, i;
    uint16_t retry = 0;

     i2c0_wrStatus = 0;

    //SHT3x_s_Status = 0;
    //SHT3x_dataLen = count;

    status = i2c0_write((unsigned char)address,
                        (unsigned char *)0, //will not use pointer to an address on this application
                        (unsigned int)0,    //address size is alwary 0 on this application
                        (unsigned char *)pdata,
                        (unsigned short)count,
                        (void *)0);

    retry = 0;
    while (( i2c0_wrStatus!= I2C_WRITE_END) || (status != I2C_R_OK))
    {
        wdt_clear();
        //Avoid dead loop here

        retry++;
        if (retry > 2000) //2000
        {
            status = 1;

            DebugPrint("!!I2CWriteLoopError!!\r\n");
            retry = 0;
            return status;
        }
    }

    return status;
}

uint8_t SHT3x_read_multi(uint8_t address, uint8_t index, uint8_t *pdata, uint8_t count)
{
    uint8_t status = STATUS_OK;
    uint16_t retry = 0;

    //SHT3x_s_Status = 0;
    i2c0_rdStatus = 0;

    //status = SHT3x_SendIndex(address, index);

    //if (status)
    //    return 1;

    //SHT3x_dataLen = count;

    //error on SendIndex
    // if (status) return 1;
    //Avoid count overflow, the max. size of i2c_rxData[] is 32
    if (count > I2C_DATA_SIZE_MAX)
    {
        return 1;
    }

    status = i2c0_read((unsigned char)address,
                       (unsigned char *)0,
                       (unsigned int)0,
                       (unsigned char *)pdata,
                       (unsigned short)count,
                       (void *)0);


    retry = 0;
    while ((i2c0_rdStatus != I2C_READ_END) || (status != I2C_R_OK))
    {
        wdt_clear();
        //Avoid dead loop in here
        retry++;
        if (retry > 2000)
        {
            status = 1;

            //Send error message
            DebugPrint("I2C Read Loop Error\r\n");
            retry = 0;
            return status;
        }
    }

    return status;
}

//1 byte data: send 1 byte register index only
static uint8_t SHT3x_SendIndex(uint8_t address, uint8_t index)
{
    uint8_t ret;
    i2c_txData[0] = index;

    //send 1byte index only, count=1
    ret = SHT3x_write_multi(address, i2c_txData, 1);
    return ret;
}


//send 2 byte
static uint8_t SHT3x_writeReg(uint8_t reg, uint8_t value)
{
    uint8_t status;
    i2c_txData[0] = reg;
    i2c_txData[1] = value;
    status = SHT3x_write_multi(SHT3x_I2C_SLAVE_ADDRESS_WRITE, i2c_txData, 2);
    return status;
}

static uint8_t SHT3x_writeReg16Bit(uint8_t reg, uint16_t data)
{
    uint8_t status = STATUS_OK;

    //uint8_t  buffer[BYTES_PER_WORD];

    // Split 16-bit word into MS and LS uint8_t
    i2c_txData[0] = reg;
    i2c_txData[1] = (uint8_t)(data >> 8);
    i2c_txData[2] = (uint8_t)(data & 0x00FF);

    // 1byte reg, 2 byte data
    status = SHT3x_write_multi(SHT3x_I2C_SLAVE_ADDRESS_WRITE, i2c_txData, 3);

    return status;
}

static uint8_t SHT3x_writeReg32Bit(uint8_t reg, uint32_t data)
{
    uint8_t status = STATUS_OK;
    // Split 32-bit word into MS ... LS bytes
    i2c_txData[0] = reg;
    i2c_txData[1] = (uint8_t)(data >> 24);
    i2c_txData[2] = (uint8_t)((data & 0x00FF0000) >> 16);
    i2c_txData[3] = (uint8_t)((data & 0x0000FF00) >> 8);
    i2c_txData[4] = (uint8_t)(data & 0x000000FF);

    // 1byte reg, 4 byte data
    status = SHT3x_write_multi(SHT3x_I2C_SLAVE_ADDRESS_WRITE, i2c_txData, 5);

    return status;
}

//Return the value
static uint8_t SHT3x_readReg(uint8_t reg)
{
    uint8_t status = STATUS_OK;
    //uint8_t value;

    status = SHT3x_read_multi(SHT3x_I2C_SLAVE_ADDRESS_READ, reg, i2c_rxData, 1);

    if (status)
    {
        DebugPrint("Err@readReg");
    }

    return i2c_rxData[0];
}

static uint16_t SHT3x_readReg16Bit(uint8_t reg)
{
    uint8_t status = STATUS_OK;
    uint16_t value;
    status = SHT3x_read_multi(SHT3x_I2C_SLAVE_ADDRESS_READ, reg, i2c_rxData, 2);

    value = ((uint16_t)(i2c_rxData[0])) << 8;
    value |= (uint16_t)i2c_rxData[1];

    return value;
}

static uint32_t SHT3x_readReg32Bit(uint8_t reg)
{
    uint8_t status = STATUS_OK;
    uint32_t value;

    status = SHT3x_read_multi(SHT3x_I2C_SLAVE_ADDRESS_READ, reg, i2c_rxData, 4);
    value = ((uint32_t)(i2c_rxData[0])) << 24; // value highest byte
    value |= ((uint32_t)(i2c_rxData[1])) << 16;
    value |= ((uint32_t)(i2c_rxData[2])) << 8;
    value |= i2c_rxData[3]; // value lowest byte

    return value;
}

static uint8_t SHT3x_writeMulti(uint8_t reg, uint8_t *src, uint8_t count)
{
    uint8_t status = STATUS_OK;
    uint8_t i;
    i2c_txData[0] = reg;
    //copy the src to the txData, as the SHT3x__write_multi() include the reg value
    //and src just the data.
    //need to tranfer to the right format of data
    for (i = 1; i <= count; i++)
    {
        i2c_txData[i] = *src;
        src++;
    }
    //Total data will add one
    status = SHT3x_write_multi(SHT3x_I2C_SLAVE_ADDRESS_WRITE, i2c_txData, (count + 1));

    return status;
}

static uint8_t SHT3x_readMulti(uint8_t reg, uint8_t *dst, uint8_t count)
{
    uint8_t status = STATUS_OK;
    //uint8_t value;

    status = SHT3x_read_multi(SHT3x_I2C_SLAVE_ADDRESS_READ, reg, dst, count);

    return status;
}


uint8_t SHT3x_start_measure(void)
{
	unsigned int i;
    uint8_t status = 0;

    // Without Stretching
    // HIGH repeat mode (most accurate) - need wait time 12.5ms typ. and 15ms max.
    //status = SHT3x_writeReg(SHT3x_START_MEASURE_NS_MSB, SHT3x_START_MEASURE_NSH_LSB);

	// about 15.5ms wait time
	//for(i=0; i<27000; i++)
	//	__asm("nop\n");


    // MEDIUM repeat mode - need wait time 4.5ms typ. and 6ms max.
	status = SHT3x_writeReg(SHT3x_START_MEASURE_NS_MSB, SHT3x_START_MEASURE_NSM_LSB);

	// about 6.5ms wait time (16MHz clock)
	for(i=0; i<11000; i++)
		__asm("nop\n");

	// LOW repeat mode - need wait time 2.5ms typ. and 4ms max.
	//status = SHT3x_writeReg(SHT3x_START_MEASURE_NS_MSB, SHT3x_START_MEASURE_NSL_LSB);

	// about 4.5ms wait time (16MHz clock)
	//for(i=0; i<8000; i++)
	//	__asm("nop\n");

    if (status == 1)
    {
        DebugPrint("Error: SHT3x Start measure...\r\n");
        return 1;
    }

    return 0;
}

uint8_t SHT3x_get_measure_data(uint16_t *temperature, uint16_t *humidity, uint8_t *temp_crc, uint8_t *hum_crc)
{
    uint8_t status = 0;
    uint32_t value;
    uint8_t crc_result;


    // Delay or retry

    status = SHT3x_read_multi(SHT3x_I2C_SLAVE_ADDRESS_READ, 0, i2c_rxData, 6);
	if(status == 1)
	{
		DebugPrint("Error: SHT3x Read measure data...\r\n");
		return 1;
	}

    crc_result = SHT3x_get_crc8(&i2c_rxData[0], 2);
    if(crc_result == i2c_rxData[2])
    {
		*temperature = ((uint16_t)(i2c_rxData[0])) << 8;
		*temperature |= ((uint16_t)(i2c_rxData[1]));
		*temp_crc = ((uint8_t)(i2c_rxData[2]));
    }
    else
    {
    	DebugPrint("Error: SHT3x temperature CRC...\r\n");
    	return 1;
	}

    crc_result = SHT3x_get_crc8(&i2c_rxData[3], 2);
    if(crc_result == i2c_rxData[5])
    {
		*humidity = ((uint16_t)(i2c_rxData[3])) << 8;
		*humidity |= ((uint16_t)(i2c_rxData[4]));
		*hum_crc = ((uint8_t)(i2c_rxData[5]));
	}
	else
	{
		DebugPrint("Error: SHT3x humidity CRC...\r\n");
		return 1;
	}

    return 0;
}


uint8_t SHT3x_get_crc8(uint8_t *data, int len)
{
/*
 *
 * CRC-8 formula from page 14 of SHT spec pdf
 *
 * Test data 0xBE, 0xEF should yield 0x92
 *
 * Initialization data 0xFF
 * Polynomial 0x31 (x8 + x5 +x4 +1)
 * Final XOR 0x00
 */
	uint8_t POLYNOMIAL = 0x31;
  	uint8_t crc = 0xFF;
	uint8_t i=0, j=0;

	for ( j = len; j; --j )
	{
	  crc ^= *data++;

	  for ( i = 8; i; --i )
	  {
			crc = ( crc & 0x80 ) ? (crc << 1) ^ POLYNOMIAL : (crc << 1);
	  }
	}

	return crc;
}


float SHT3x_get_temperature_C(uint16_t rawValue)
{
// calculate temperature [�XC]
// T = -45 + 175 * rawValue / (2^16-1)

	float result;


	result = (float)(rawValue)/65535;
	result = 175 * result;
	result = result - 45;

	//DebugPrint("temp[C] = %.2f\r\n", result);

	return result;

	//return ((175 * (float) (rawValue / 65535)) - 45);
}

float SHT3x_get_temperature_F(uint16_t rawValue)
{
// calculate temperature [�XF]
// T = -45 + 175 * rawValue / (2^16-1)

	float result;


	result = (float)(rawValue)/65535;
	result = 315 * result;
	result = result - 49;

	//DebugPrint("temp[F] = %.2f\r\n", result);

	return result;

	//return ((315 * (float) (rawValue / 65535)) - 49);
}


float SHT3x_get_humidity(uint16_t rawValue)
{
// calculate relative humidity [%RH]
// RH = rawValue / (2^16-1) * 100

	float result;


	result = (float)(rawValue)/65535;
	result = 100*result;

	//DebugPrint("hum = %.2f\r\n", result);

	return result;

	//return (100 * (float)(rawValue / 65535));
}


uint8_t SHT3x_read_status(uint16_t *chip_status, uint8_t *chip_status_crc)
{
	uint8_t crc_result;
	uint8_t status = 0;

	// With Stretching
    status = SHT3x_writeReg(SHT3x_READ_STATUS_MSB, SHT3x_READ_STATUS_LSB);
	if (status == 1)
   {
	   DebugPrint("Error: SHT3x Read status..\r\n");
	   return 1;
   }



	status = SHT3x_read_multi(SHT3x_I2C_SLAVE_ADDRESS_READ, 0, i2c_rxData, 3);
	if(status == 1)
	{
		DebugPrint("Error: SHT3x Read status 2...\r\n");
		return 1;
	}

	crc_result = SHT3x_get_crc8(&i2c_rxData[0], 2);
	if(crc_result == i2c_rxData[2])
	{
		*chip_status = ((uint16_t)(i2c_rxData[0])) << 8;
		*chip_status |= ((uint16_t)(i2c_rxData[1]));
		*chip_status_crc = ((uint8_t)(i2c_rxData[2]));
	}
	else
	{
		DebugPrint("Error: SHT3x read status CRC...\r\n");
		return 1;
	}



	DebugPrint("SHT3x status = 0x%x\r\n", *chip_status);


    return 0;
}

uint8_t SHT3x_clear_status(void)
{
    uint8_t status = 0;

	// With Stretching
    status = SHT3x_writeReg(SHT3x_CLEAR_STATUS_MSB, SHT3x_CLEAR_STATUS_LSB);

    if (status == 1)
    {
        DebugPrint("Error: SHT3x Clear status..\r\n");
        return 1;
    }

    return 0;
}

uint8_t SHT3x_soft_reset(void)
{
    uint8_t status = 0;

	// With Stretching
    status = SHT3x_writeReg(SHT3x_SOFT_RESET_MSB, SHT3x_SOFT_RESET_LSB);

    if (status == 1)
    {
        DebugPrint("Error: SHT3x soft reset..\r\n");
        return 1;
    }

    return 0;
}

uint8_t SHT3x_heater_enable(void)
{
    uint8_t status = 0;

	// With Stretching
    status = SHT3x_writeReg(SHT3x_HEATER_EN_MSB, SHT3x_HEATER_EN_LSB);

    if (status == 1)
    {
        DebugPrint("Error: SHT3x heater enable..\r\n");
        return 1;
    }

    return 0;
}

uint8_t SHT3x_heater_disable(void)
{
    uint8_t status = 0;

	// With Stretching
    status = SHT3x_writeReg(SHT3x_HEATER_DIS_MSB, SHT3x_HEATER_DIS_LSB);

    if (status == 1)
    {
        DebugPrint("Error: SHT3x heater disable..\r\n");
        return 1;
    }

    return 0;
}




/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
void TemperatureSensor_Initialize(void)
{
	//SHT3x_soft_reset();
}


uint8_t Run_TempSensorCtrl(void)
{
	unsigned char i=0;
	unsigned char status=0;
	float temp_c_ft=0;
	float temp_f_ft=0;
	float hum_ft=0;

	// Try to check status before and after heater on
	//SHT3x_read_status(&SHT3x_chip_status, &SHT3x_chip_status_crc);
	//SHT3x_clear_status();

	//SHT3x_heater_enable();  // why enable heater can work?

	//SHT3x_read_status(&SHT3x_chip_status, &SHT3x_chip_status_crc);
	//SHT3x_clear_status();


	// read one time
	/*
	SHT3x_start_measure();
	SHT3x_get_measure_data(&SHT3x_temperature, &SHT3x_humidity, &SHT3x_temperature_crc, &SHT3x_humidity_crc);

	SHT3x_temperature_C_ft = SHT3x_get_temperature_C(SHT3x_temperature);
	SHT3x_temperature_F_ft = SHT3x_get_temperature_F(SHT3x_temperature);
	SHT3x_humidity_ft = SHT3x_get_humidity(SHT3x_humidity);
	*/

	for(i=0; i<READ_TEMP_SENSOR_COUNT; i++)
	{
		SHT3x_start_measure();
		if(SHT3x_get_measure_data(&SHT3x_temperature, &SHT3x_humidity, &SHT3x_temperature_crc, &SHT3x_humidity_crc)==0)
		{

			temp_c_ft += SHT3x_get_temperature_C(SHT3x_temperature);
			temp_f_ft += SHT3x_get_temperature_F(SHT3x_temperature);
			hum_ft += SHT3x_get_humidity(SHT3x_humidity);

		}
		else
		{
			status = 1;
			break;
		}
	}

	if(status == 0)
	{
		SHT3x_temperature_C_ft = temp_c_ft/READ_TEMP_SENSOR_COUNT;
		SHT3x_temperature_F_ft = temp_f_ft/READ_TEMP_SENSOR_COUNT;
		SHT3x_humidity_ft = hum_ft/READ_TEMP_SENSOR_COUNT;

		//DebugPrint("C=%.2f, F=%.2f, H=%.2f\r\n", SHT3x_temperature_C_ft, SHT3x_temperature_F_ft, SHT3x_humidity_ft);

        return RET_OK;
	}
	else
	{
		DebugPrint("Error - read TEMP sensor\r\n");
	}

    return RET_ERROR;
}


void Power_On_TempSensorCtrl(void)
{
	TEMP_PWR_ON; // enable power to temperature sensor
}

void Power_Off_TempSensorCtrl(void)
{
	TEMP_PWR_OFF; // disable power to temperature sensor
}
