/*****************************************************************************
 Driver_Soft_I2c.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */

#ifndef _DRIVER_SOFT_I2C_H
#define _DRIVER_SOFT_I2C_H


/* Software I2C Master 2 */
#define SWI2C_SDA_PIN       P04D
#define SWI2C_SDA_DIR       P04DIR
#define SWI2C_SDA_C0        P04C0
#define SWI2C_SDA_C1        P04C1
#define SWI2C_SDA_MD0       P04MD0
#define SWI2C_SDA_MD1       P04MD1

#define SWI2C_SCL_PIN       P05D
#define SWI2C_SCL_DIR       P05DIR
#define SWI2C_SCL_C0        P05C0
#define SWI2C_SCL_C1        P05C1
#define SWI2C_SCL_MD0       P05MD0
#define SWI2C_SCL_MD1       P05MD1


#define GPIODELAYCYCLES     2

#define I2CDELAY 			SWI2CMST_delay()    // Macro for GPIO change delay

#define SDA_H	            set_bit( SWI2C_SDA_DIR );
#define SDA_L		        {clear_bit(SWI2C_SDA_PIN);clear_bit( SWI2C_SDA_DIR );}

#define SCL_H		        set_bit( SWI2C_SCL_DIR );
#define SCL_L    		    {clear_bit(SWI2C_SCL_PIN);clear_bit( SWI2C_SCL_DIR );}

#define NACK    0
#define ACK     1
#define BIT7    0x80
#define BIT0    0x01


//0xFFFF ~130ms @8MHz, 65Ms @16Mhz
//0xFF //~512us 5000~ 10ms @8MHz
#define I2C_TIMEOUT     ((unsigned int)0xF4240) //~200ms @8MHz

//#define I2C_TIMEOUT     ((unsigned int)0x1E8480) //~200ms @16MHz

#define I2C_OK              0
#define I2C_ERROR_ACK       1
#define I2C_ERROR_TIMEOUT   2


void swI2cMst0_init(void);
unsigned char swI2cMst0_Read(unsigned char SlaveAddress, unsigned char reg, unsigned char* data, unsigned short len);
unsigned char swI2cMst0_Write(unsigned char SlaveAddress, unsigned char reg, unsigned char* data, unsigned short len);

#endif
