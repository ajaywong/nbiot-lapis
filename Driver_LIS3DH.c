

#include <stdio.h>
#include <String.h>
#include <stdlib.h>

#include "rdwr_reg.h"
#include "mcu.h"
#include "clock.h"
#include "wdt.h"
#include "irq.h"

#include "ssio_common.h"
#include "ssio0.h"

/* User Include */
#include "User_System.h"
#include "User_Generic.h"

#include "Driver_LIS3DH.h"

// Chip select to G-sensor (LI3DH): Active-LOW = CHIP SELECT ENABLE
#define SPI_CS_OFF			P47D = 1
#define SPI_CS_ON			P47D = 0


int16_t LIS3DH_Raw_Data_X = 0;
int16_t LIS3DH_Raw_Data_Y = 0;
int16_t LIS3DH_Raw_Data_Z = 0;

float LIS3DH_X_G = 0;
float LIS3DH_Y_G = 0;
float LIS3DH_Z_G = 0;

uint8_t temp_data = 0;
//uint8_t reg_data[48];


uint8_t LIS3DH_write_multi(uint8_t address, uint8_t *pdata, uint8_t count)
{

    uint8_t status, i;
    uint16_t retry;
	uint8_t addr;
	uint8_t spi_data[SPI_DATA_SIZE_MAX];

	SPI_CS_ON;

	Ssio0_flag = 0;

	if(count < 1 || count > SPI_DATA_SIZE_MAX)
		return 1;

	addr = ~0x80 & address; // bit7 = 0: Write to 3G sensor
	if(count == 1)
		addr = ~0x40 & addr;  // bit6 = 0: not auto-increment address
	else
		addr = 0x40 | addr; // bit6 = 1: auto-increment address


	spi_data[0] = addr;
	memcpy(&spi_data[1], pdata, (SPI_DATA_SIZE_MAX-1));

	status = ssio0_start( SSIO_MODE2, (void *)0, (unsigned char *)spi_data, (count+1), (void *)0 );

    retry = 0;
    while ((Ssio0_flag != 1) || (status != SSIO_R_OK))
    {
        wdt_clear();
        //Avoid dead loop here
        retry++;
        if (retry > 2000)
        {
            status = 1;

            DebugPrint("!!SPI WriteLoopError!!\r\n");
            retry = 0;
            return status;
        }
    }

	SPI_CS_OFF;

    return status;
}



uint8_t LIS3DH_read_multi(uint8_t address, uint8_t *pdata, uint8_t count)
{

    uint8_t status, i;
    uint16_t retry;
	uint8_t addr;
	uint8_t spi_data[SPI_DATA_SIZE_MAX];

	SPI_CS_ON;

	Ssio0_flag = 0;

	if(count < 1 || count > SPI_DATA_SIZE_MAX)
		return 1;

	addr = 0x80 | address; // bit7 = 1: Write to 3G sensor
	if(count == 1)
		addr = ~0x40 & addr;  // bit6 = 0: not auto-increment address
	else
		addr = 0x40 | addr; // bit6 = 1: auto-increment address

	memset(spi_data, 0, SPI_DATA_SIZE_MAX);
	spi_data[0] = addr;

	status = ssio0_start( SSIO_MODE3, (unsigned char *)pdata, (unsigned char *)spi_data, (count+1), (void *)0 );

    retry = 0;
    while ((Ssio0_flag != 1) || (status != SSIO_R_OK))
    {
        wdt_clear();
        //Avoid dead loop here
        retry++;
        if (retry > 2000)
        {
            status = 1;

            DebugPrint("!!SPI ReadLoopError!!\r\n");
            retry = 0;
            return status;
        }
    }

	SPI_CS_OFF;

    return status;
}



uint8_t LIS3DH_writeReg8(uint8_t reg, uint8_t value)
{
    uint8_t status;
    Spi_txData[0] = value;

    status = LIS3DH_write_multi(reg, Spi_txData, 1); // actually send 2 byte (address and data) in SPI
    if(status == 1)
    {
		DebugPrint("!!SPI Write Reg8!!\r\n");
    }

    return status;
}

uint8_t LIS3DH_readReg8(uint8_t reg)
{
    uint8_t status;

    status = LIS3DH_read_multi(reg, Spi_rxData, 1);  // actually send 1 byte (address) and read 1 byte (data) in SPI
    if(status == 1)
    {
		DebugPrint("!!SPI Read Reg8!!\r\n");
    }

    return Spi_rxData[1];
}



uint8_t LIS3DH_init(void)
{
	/* Check connection */
	uint8_t i;
	uint8_t deviceid = LIS3DH_readReg8(LIS3DH_REG_WHOAMI);

	if (deviceid != 0x33)
	{
		/* No LIS3DH detected ... return false */
		return 1;
	}

	// enable all axes, normal mode
	LIS3DH_writeReg8(LIS3DH_REG_CTRL1, 0x07); //0x07

	temp_data = LIS3DH_readReg8(LIS3DH_REG_CTRL1);

	// 400Hz rate
	//setDataRate(LIS3DH_DATARATE_400_HZ);
	LIS3DH_setDataRate(LIS3DH_DATARATE_50_HZ); //LIS3DH_DATARATE_10_HZ

	// No FIFO used
	LIS3DH_setFifoMode(LIS3DH_FIFO_BYPASS);

	// High res & BDU enabled & +/-2G range
	LIS3DH_writeReg8(LIS3DH_REG_CTRL4, 0x88);

	temp_data = LIS3DH_readReg8(LIS3DH_REG_CTRL4);


	// DRDY on INT1
	//LIS3DH_writeReg8(LIS3DH_REG_CTRL3, 0x10);

	// No signal on INT1 - use polling register
	LIS3DH_writeReg8(LIS3DH_REG_CTRL3, 0x00);

	temp_data = LIS3DH_readReg8(LIS3DH_REG_CTRL3);

	// Turn on orientation config
	//LIS3DH_writeReg8(LIS3DH_REG_PL_CFG, 0x40);

	// enable adcs
	LIS3DH_writeReg8(LIS3DH_REG_TEMPCFG, 0x80);

	temp_data = LIS3DH_readReg8(LIS3DH_REG_TEMPCFG);

	return 0;
}


void LIS3DH_setDataRate(lis3dh_dataRate_t dataRate)
{
  uint8_t ctl1 = LIS3DH_readReg8(LIS3DH_REG_CTRL1);

  ctl1 &= ~(0xF0); // mask off bits
  ctl1 |= (dataRate << 4);
  LIS3DH_writeReg8(LIS3DH_REG_CTRL1, ctl1);
}

lis3dh_dataRate_t LIS3DH_getDataRate(void)
{
  return (lis3dh_dataRate_t)((LIS3DH_readReg8(LIS3DH_REG_CTRL1) >> 4)& 0x0F);
}


void LIS3DH_setRange(lis3dh_range_t range)
{
  uint8_t r = LIS3DH_readReg8(LIS3DH_REG_CTRL4);
  r &= ~(0x30);
  r |= range << 4;
  LIS3DH_writeReg8(LIS3DH_REG_CTRL4, r);
}


lis3dh_range_t LIS3DH_getRange(void)
{
  /* Read the data format register to preserve bits */
  return (lis3dh_range_t)((LIS3DH_readReg8(LIS3DH_REG_CTRL4) >> 4) & 0x03);
}


void LIS3DH_setFifoMode(lis3dh_fifo_mode_t mode)
{
  uint8_t m = LIS3DH_readReg8(LIS3DH_REG_FIFOCTRL);
  m &= ~(0xC0);
  m |= mode << 6;
  LIS3DH_writeReg8(LIS3DH_REG_FIFOCTRL, m);
}


lis3dh_fifo_mode_t LIS3DH_getFifoMode(void)
{
   return (lis3dh_fifo_mode_t)((LIS3DH_readReg8(LIS3DH_REG_FIFOCTRL) >> 6) & 0x03);
}


uint8_t LIS3DH_get_measure_XYZ(float *X_G, float *Y_G, float *Z_G)
{
	uint8_t status=0;
	uint8_t range;
	uint16_t divider;

	status = LIS3DH_readReg8(LIS3DH_REG_STATUS2) & 0x08; // check if XYZ data available

	if(status)
	{
/*
		LIS3DH_Raw_Data_X =	LIS3DH_readReg8(LIS3DH_REG_OUT_X_L);
		LIS3DH_Raw_Data_X |= ((uint16_t)LIS3DH_readReg8(LIS3DH_REG_OUT_X_H)) << 8;
		LIS3DH_Raw_Data_Y = LIS3DH_readReg8(LIS3DH_REG_OUT_Y_L);
		LIS3DH_Raw_Data_Y |= ((uint16_t)LIS3DH_readReg8(LIS3DH_REG_OUT_Y_H)) << 8;
		LIS3DH_Raw_Data_Z =	LIS3DH_readReg8(LIS3DH_REG_OUT_Z_L);
		LIS3DH_Raw_Data_Z |= ((uint16_t)LIS3DH_readReg8(LIS3DH_REG_OUT_Z_H)) << 8;
*/
		LIS3DH_read_multi(LIS3DH_REG_OUT_X_L, Spi_rxData, 6);

		LIS3DH_Raw_Data_X = Spi_rxData[1];
		LIS3DH_Raw_Data_X |= ((uint16_t)Spi_rxData[2]) << 8;
		LIS3DH_Raw_Data_Y = Spi_rxData[3];
		LIS3DH_Raw_Data_Y |= ((uint16_t)Spi_rxData[4]) << 8;
		LIS3DH_Raw_Data_Z = Spi_rxData[5];
		LIS3DH_Raw_Data_Z |= ((uint16_t)Spi_rxData[6]) << 8;

		range = LIS3DH_getRange();
		divider = 1;

  		//if (range == LIS3DH_RANGE_16_G) divider = 1365; // different sensitivity at 16g
  		//if (range == LIS3DH_RANGE_8_G) divider = 4096;
  		//if (range == LIS3DH_RANGE_4_G) divider = 8190;
  		//if (range == LIS3DH_RANGE_2_G) divider = 16380;


		// convert sensitivity level from mg to g
		// e.g. +/-2G = total 4G, at 16-bit = 65535 level, 4G/65535 = each LSB equals 0.00006g change
  		if (range == LIS3DH_RANGE_16_G) divider = 1370;      // 1/(0.73/1000)
  		else if (range == LIS3DH_RANGE_8_G) divider = 4167;  // 1/(0.24/1000)
  		else if (range == LIS3DH_RANGE_4_G) divider = 8333;  // 1/(0.12/1000)
  		else if (range == LIS3DH_RANGE_2_G) divider = 16667; // 1/(0.06/1000)

		//LIS3DH_X_G = (float)LIS3DH_Raw_Data_X / divider;
  		//LIS3DH_Y_G = (float)LIS3DH_Raw_Data_Y / divider;
		//LIS3DH_Z_G = (float)LIS3DH_Raw_Data_Z / divider;

		*X_G = (float)LIS3DH_Raw_Data_X / divider;
  		*Y_G = (float)LIS3DH_Raw_Data_Y / divider;
		*Z_G = (float)LIS3DH_Raw_Data_Z / divider;

		//DebugPrint("X=%.2f, Y=%.2f, Z=%.2f\r\n", *X_G, *Y_G, *Z_G);


		if((*X_G < -2 || *X_G >2) ||
			(*Y_G < -2 || *Y_G >2) ||
			(*Z_G < -2 || *Z_G >2))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 1;
	}
}




