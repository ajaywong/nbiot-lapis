/*****************************************************************************
 User_Board.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _USR_BOARD_H_
#define _USR_BOARD_H_

#include <stdio.h>
#include <string.h>

/* MCU */
#include "mcu.h"
#include "rdwr_reg.h"

#include "Gpio_Common.h"
#include "User_Generic.h"

/*############################################################################*/
#define CONFIG_MAIN_CLK_4MHZ
//#define CONFIG_MAIN_CLK_8MHZ
//#define CONFIG_MAIN_CLK_16MHZ


/*############################################################################*/
/* Low-Speed Time Base Counter */
#define TBC_T128HZ  0x00
#define TBC_T64HZ   0x01
#define TBC_T32HZ   0x02
#define TBC_T16HZ   0x03
#define TBC_T8HZ    0x04
#define TBC_T4HZ    0x05
#define TBC_T2HZ    0x06
#define TBC_T1HZ    0x07

//
#define TBC_0_INTERVAL  TBC_T128HZ
#define TBC_1_INTERVAL  TBC_T2HZ
#define TBC_2_INTERVAL  TBC_T1HZ

#define USE_DEBUG_CONSOLE
/*  */
#ifdef USE_DEBUG_CONSOLE
#define DebugPrint  printf
#else
#define DebugPrint  //
#endif

/*############################################################################*/
#if defined(CONFIG_MAIN_CLK_16MHZ)
#define US_DELAY_COUNT  1060   //~1Ms @16MHz
#define NOP_COUNT       {__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");}
#define CLK_SYSC        CLK_SYSC_OSCLK
#elif defined(CONFIG_MAIN_CLK_8MHZ)
#define US_DELAY_COUNT  1100   //~1Ms @8MHz
#define NOP_COUNT       {__asm("nop");}
#define CLK_SYSC        CLK_SYSC_OSCLK_DIV2
#elif defined(CONFIG_MAIN_CLK_4MHZ)
#define US_DELAY_COUNT  550   //~1Ms @4MHz
#define NOP_COUNT       {__asm("nop");} //5us @4MHZ
#define CLK_SYSC        CLK_SYSC_OSCLK_DIV4
#endif //
#define DEBUG_BOARD


/*############################################################################*/
/* Board GPIO Common Used*/
/*############################################################################*/
/*      */
#define GREEN_LED_PIN   P42
#define GREEN_LED       P42D // Power

#define RED_LED_PIN     P46
#define RED_LED         P46D// Low Battery
//
#define blinkLED(led)   write_bit( led, ~(get_bit( led )) )

/* Battery Detect */
#define BATT_SEN_EN_PIN     P21
#define BATT_SEN_EN_D       P21D

#define BATT_ADC_PIN        P20
#define NH3_COUNTER_PIN     P57     



#define TRIGGER_COUNTER_PIN   P56 // WATER_GAS_PIN
#ifdef DEBUG_BOARD
#define SENSOR_ANALOG_PIN   P34 // WATER_GAS_PIN
#endif


/* Switch Pin */
#define SW0_PIN         P31
#define SW0_PIN_D       P31D
#define SW0_EXT_PIN     EXIn_PORT_SEL_P31

/* SIGFOX  Pin */
#define SIGFOX_PWR_PIN      P11
#define SIGFOX_PWR_D        P11D

#define SIGFOX_RST_PIN      P57
#define SIGFOX_RST_D        P57D

#define SIGFOX_WAKEUP_PIN   P56
#define SIGFOX_WAKEUP_D     P56D

/* UARTF0 */
#define UARTF0_TXD_PIN      P55
#define UARTF0_RXD_PIN      P54


/*############################################################################*/
/* Test Mode Pin */
#define TEST_MODE_PIN       P32
#define TEST_MODE_D         P32D
#define TEST_EXI_PIN        EXIn_PORT_SEL_P32

/* Temperature Sensor Power Pin */
#define TEMP_SEN_PWR_PIN    P30
#define TEMP_SEN_PWR_D      P30D

/* Motion Sensor Voltage Level Converter  */
#define MOT_SEN_VLT_PIN     P53
#define MOT_SEN_VLT_D       P53D

/* Distance Sensor Power Pin */
#define DIST_SEN_PWR_PIN    P35
#define DIST_SEN_PWR_D      P35D

/* Distance Sensor Voltage Level Converter */
#define DIST_SEN_VLT_PIN    P37
#define DIST_SEN_VLT_D      P37D

/* Distance Sensor SHT */
#define DIST_SEN_SHT_PIN        P36
#define DIST_SEN_SHT_EXT_PIN    EXIn_PORT_SEL_P36

/* RTC Alarm */
#define RTC_ALARM_PIN       P43
#define RTC_ALARM_D         P43D
#define RTC_EXI_PIN         EXIn_PORT_SEL_P43

/* I2C0 */
#define I2C0_SDA_PIN        P40
#define I2C0_SCL_PIN        P41

/* I2C0 */
#define I2C1_SDA_PIN        P44
#define I2C1_SCL_PIN        P45

/* SSIO SPI0 */
#define SSIO_CS_PIN         P47
#define SSIO_SOUT_PIN       P50
#define SSIO_SIN_PIN        P51
#define SSIO_SCK_PIN        P52

/* UART0 */
#define UART0_TXD_PIN       P01
#define UART0_RXD_PIN       P00


/* Not used Pin */
#define UN_PIN_02   P02
#define UN_PIN_03   P03
#define UN_PIN_04   P04
#define UN_PIN_05   P05

#define UN_PIN_10   P10
#define UN_PIN_22   P22
#define UN_PIN_23   P23

#define UN_PIN_33   P33
#define UN_PIN_34   P34


#endif
