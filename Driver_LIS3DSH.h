/*****************************************************************************
 Driver_LIS3DSH.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/

#ifndef _DRIVER_LIS3DSH_H_
#define _DRIVER_LIS3DSH_H_


/* User Include */
#include "User_System.h"
#include "User_Generic.h"


/*############################################################################*/
/*#                               I2C Related                                #*/
/*############################################################################*/

// Use SPI mode to read/write LIS3DSH

#if 0
#define LIS3DSH_REG_STATUS1       0x07
#define LIS3DSH_REG_OUTADC1_L     0x08
#define LIS3DSH_REG_OUTADC1_H     0x09
#define LIS3DSH_REG_OUTADC2_L     0x0A
#define LIS3DSH_REG_OUTADC2_H     0x0B
#define LIS3DSH_REG_OUTADC3_L     0x0C
#define LIS3DSH_REG_OUTADC3_H     0x0D
#define LIS3DSH_REG_INTCOUNT      0x0E
#endif

#define LIS3DSH_REG_OUT_TEMP      0x0C
#define LIS3DSH_REG_WHOAMI        0x0F

//#define LIS3DSH_REG_TEMPCFG       0x1F

//#define LIS3DSH_REG_CTRL1         0x21
//#define LIS3DSH_REG_CTRL2         0x22
#define LIS3DSH_REG_CTRL3         0x23
#define LIS3DSH_REG_CTRL4         0x20
#define LIS3DSH_REG_CTRL5         0x24
#define LIS3DSH_REG_CTRL6         0x25

//#define LIS3DSH_REG_REFERENCE     0x26
#define LIS3DSH_REG_STATUS        0x27
#define LIS3DSH_REG_OUT_X_L       0x28
#define LIS3DSH_REG_OUT_X_H       0x29
#define LIS3DSH_REG_OUT_Y_L       0x2A
#define LIS3DSH_REG_OUT_Y_H       0x2B
#define LIS3DSH_REG_OUT_Z_L       0x2C
#define LIS3DSH_REG_OUT_Z_H       0x2D
#define LIS3DSH_REG_FIFOCTRL      0x2E
#define LIS3DSH_REG_FIFOSRC       0x2F

#if 0
#define LIS3DSH_REG_INT1CFG       0x30
#define LIS3DSH_REG_INT1SRC       0x31
#define LIS3DSH_REG_INT1THS       0x32
#define LIS3DSH_REG_INT1DUR       0x33
#define LIS3DSH_REG_CLICKCFG      0x38
#define LIS3DSH_REG_CLICKSRC      0x39
#define LIS3DSH_REG_CLICKTHS      0x3A
#define LIS3DSH_REG_TIMELIMIT     0x3B
#define LIS3DSH_REG_TIMELATENCY   0x3C
#define LIS3DSH_REG_TIMEWINDOW    0x3D
#define LIS3DSH_REG_ACTTHS        0x3E
#define LIS3DSH_REG_ACTDUR        0x3F
#endif


typedef enum
{
  LIS3DSH_RANGE_16_G          = 0x4,   // +/- 16g
  LIS3DSH_RANGE_8_G           = 0x3,   // +/- 8g
  LIS3DSH_RANGE_6_G           = 0x2,   // +/- 6g
  LIS3DSH_RANGE_4_G           = 0x1,   // +/- 4g
  LIS3DSH_RANGE_2_G           = 0x0    // +/- 2g (default value)
} lis3dsh_range_t;

typedef enum
{
  LIS3DSH_AXIS_X         = 0x0,
  LIS3DSH_AXIS_Y         = 0x1,
  LIS3DSH_AXIS_Z         = 0x2
} lis3dsh_axis_t;

typedef enum
{
  LIS3DSH_FIFO_BYPASS     = 0x0   // BYPASS mode (default value)
  // Other modes not used
} lis3dsh_fifo_mode_t;


/* Used with register 0x2A (LIS3DSH_REG_CTRL_REG1) to set bandwidth */
typedef enum
{
  LIS3DSH_DATARATE_1K6_HZ  	  = 0x9,
  LIS3DSH_DATARATE_800_HZ     =	0x8,
  LIS3DSH_DATARATE_400_HZ     = 0x7,
  LIS3DSH_DATARATE_100_HZ     = 0x6,
  LIS3DSH_DATARATE_50_HZ      = 0x5,
  LIS3DSH_DATARATE_25_HZ      = 0x4,
  LIS3DSH_DATARATE_12_5_HZ    = 0x3,
  LIS3DSH_DATARATE_6_25_HZ    = 0x2,
  LIS3DSH_DATARATE_3_125_HZ   = 0x1,
  LIS3DSH_DATARATE_POWERDOWN  = 0x0

} lis3dsh_dataRate_t;


extern int16_t LIS3DSH_Raw_Data_X;
extern int16_t LIS3DSH_Raw_Data_Y;
extern int16_t LIS3DSH_Raw_Data_Z;

extern float LIS3DSH_X_G;
extern float LIS3DSH_Y_G;
extern float LIS3DSH_Z_G;



uint8_t LIS3DSH_write_multi(uint8_t address, uint8_t *pdata, uint8_t count);
uint8_t LIS3DSH_read_multi(uint8_t address, uint8_t *pdata, uint8_t count);
uint8_t LIS3DSH_writeReg8(uint8_t reg, uint8_t value);
uint8_t LIS3DSH_readReg8(uint8_t reg);

uint8_t LIS3DSH_init(void);



void LIS3DSH_setDataRate(lis3dsh_dataRate_t dataRate);
lis3dsh_dataRate_t LIS3DSH_getDataRate(void);
void LIS3DSH_setRange(lis3dsh_range_t range);
lis3dsh_range_t LIS3DSH_getRange(void);
void LIS3DSH_setFifoMode(lis3dsh_fifo_mode_t mode);
lis3dsh_fifo_mode_t LIS3DSH_getFifoMode(void);
uint8_t LIS3DSH_get_measure_XYZ(float *X_G, float *Y_G, float *Z_G);


#endif
