/*****************************************************************************
 MAG3110.c

 Copyright (C) 2015 Defond Co., LTD.
 All rights reserved.

 History
	2015.09.2 ver.1.00

******************************************************************************/
#include <stdio.h>
#include <String.h>
#include <stdlib.h>
#include <math.h>

/*  API  */
#include "mcu.h"
#include "irq.h"

#include "User_System.h"
#include "User_Generic.h"


/*  */
#include "Driver_Soft_I2c1.h"
#include "Driver_MAG3110.h"

#define CAL_SAMPLE_COUNT    10
#define SAMPLE_COUNT        5
#define THR_VAL             2500

#define CAR_IN       0xAA
#define CAR_OUT      0x55

/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/

/*
int16_t pre_val_int[3][3];
int16_t sum_int[3] = {0, 0, 0};
int16_t avg_int[3] = {0, 0, 0};
*/

uint8_t car_detected_count = 0;
uint8_t no_car_detected_count = 0;
uint8_t car_detected = CAR_OUT;


/*  */
uint8_t MAG3110_ID = 0;
uint8_t MAG3110_active_mode = 0;
uint8_t MAG3110_raw_mode = 0;
uint8_t MAG3110_auto_mrst = 0;


int16_t MAG3110_X = 0;
int16_t MAG3110_Y = 0;
int16_t MAG3110_Z = 0;

uint32_t MAG3110_old_X = 0;
uint32_t MAG3110_old_Y = 0;
uint32_t MAG3110_old_Z = 0;



/*############################################################################*/
/*#                               	Driver
/*############################################################################*/

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/

void Power_On_MagneticSensorCtrl(void)
{
    write_bit( P35D, 1);
    write_bit( P37D, 1);
    delay_Ms(100);
}

void Power_Off_MagneticSensorCtrl(void)
{
    write_bit( P35D, 0);
    write_bit( P37D, 0);
}


uint8_t MAG3110_get_ID(uint8_t *mag3110_id)
{
    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2] = { 0x00, 0x00 };

    *mag3110_id = 0;
    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_WHO_AM_I, cmd, 1);
    if( ret == I2C_OK )
    {
        *mag3110_id = cmd[0];
        DebugPrint("MAG3110 ID OK = 0x%x\r\n", cmd[0]);
    }
    else
    {
        DebugPrint("Get ID Error\r\n");
    }


    return ret;
}



uint8_t MAG3110_start()
{
    uint8_t ret = I2C_ERROR_ACK;

    ret = MAG3110_get_ID(&MAG3110_ID);

    if(MAG3110_ID == 0xC4)
    {
        ret = MAG3110_enterStandby();

        ret = MAG3110_setAutoMrst(1);

        ret = MAG3110_setDR_OS(MAG3110_DR_OS_0_31_32); //MAG3110_DR_OS_0_63_16 //MAG3110_DR_OS_20_16

        ret = MAG3110_exitStandby();
    }
    else
        ret = I2C_ERROR_ACK;

    return ret;
}



uint8_t MAG3110_enterStandby()
{

    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2] = { 0x00, 0x00 };

    MAG3110_active_mode = 0;

    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1);
    if( ret == I2C_OK )
    {
        cmd[0] = (cmd[0] & ~(0x3));

        //DebugPrint("MAG3110 REG1 = 0x%x\r\n", cmd[0]);
        ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1);  //Clear bits 0 and 1 to enter low power standby mode
    }

    return ret;



}



uint8_t MAG3110_exitStandby()
{

    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2] = { 0x00, 0x00 };

    MAG3110_active_mode = 1;

    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1);
    if( ret == I2C_OK )
    {
        // enable ACTIVE mode
        cmd[0] = cmd[0]&0xFC;
        cmd[0] = cmd[0] | MAG3110_ACTIVE_MODE;
        //DebugPrint("MAG3110 REG1 = 0x%x\r\n", cmd[0]);
        ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1);
    }

    return ret;
}



uint8_t MAG3110_dataReady(uint8_t *data_ready)
{
    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2] = { 0x00, 0x00 };

    *data_ready = 0;

    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_DR_STATUS, cmd, 1);
    if( ret == I2C_OK )
    {
        *data_ready = (cmd[0] & 0x8) >> 3;
    }

    return ret;
}


uint8_t MAG3110_readMag(int16_t *x, int16_t *y, int16_t *z)
{
    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[7];
    int16_t z_data;

    //Read each axis
    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_OUT_X_MSB, cmd, 7);
    if( ret == I2C_OK )
    {

        //*x = ((int16_t)cmd[0]<<8) | cmd[1];;
        //*y = ((int16_t)cmd[2]<<8) | cmd[3];
        //*z = ((int16_t)cmd[4]<<8) | cmd[5];

        *x = (((int16_t)cmd[0] & 0x00FF)<<8) | ((int16_t)cmd[1] & 0x00FF);
        *y = (((int16_t)cmd[2] & 0x00FF)<<8) | ((int16_t)cmd[3] & 0x00FF);
        *z = (((int16_t)cmd[4] & 0x00FF)<<8) | ((int16_t)cmd[5] & 0x00FF);

        //DebugPrint("1) X= 0x%04x, Y= 0x%04x, Z= 0x%04x\r\n", *x, *y, *z);
        //DebugPrint("X= %d, Y= %d, Z= %d\r\n", *x, *y, *z);
    }

    return ret;
}



uint8_t MAG3110_readMicroTeslas(float* x, float* y, float* z)
{
    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[7];

    //Read each axis and scale to Teslas
    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_OUT_X_MSB, cmd, 7);

    if( ret == I2C_OK )
    {
        //*x = (float)(((uint16_t)cmd[0]<<8) | cmd[1]) * 0.1f;
        //*y = (float)(((uint16_t)cmd[2]<<8) | cmd[3]) * 0.1f;
        //*z = (float)(((uint16_t)cmd[4]<<8) | cmd[5]) * 0.1f;

        *x = (float)((((int16_t)cmd[0] & 0x00FF)<<8) | ((int16_t)cmd[1] & 0x00FF)) * 0.1f;
        *y = (float)((((int16_t)cmd[2] & 0x00FF)<<8) | ((int16_t)cmd[3] & 0x00FF)) * 0.1f;
        *z = (float)((((int16_t)cmd[4] & 0x00FF)<<8) | ((int16_t)cmd[5] & 0x00FF)) * 0.1f;

        DebugPrint("X= %.2f, Y= %.2f, Z= %.2f\r\n", *x, *y, *z);
    }

    return ret;
}



uint8_t MAG3110_setDR_OS(uint8_t DROS)
{

    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2];
    uint16_t i;

    uint8_t wasActive = MAG3110_active_mode;

    if(MAG3110_active_mode)
        MAG3110_enterStandby(); //Must be in standby to modify CTRL_REG1

    //If we attempt to write to CTRL_REG1 right after going into standby
    //It might fail to modify the other bits
    for(i=0; i<1000; i++);

    //Get the current control register
    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1);
    if( ret == I2C_OK )
    {

        cmd[0] = (cmd[0] & 0x07) | DROS; //And chop off the 5 MSB

       // DebugPrint("MAG3110 REG1 = 0x%x\r\n", cmd[0]);

        ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1); //Write back the register with new DR_OS set

    }


    for(i=0; i<1000; i++);

    //Start sampling again if we were before
    if(wasActive)
        MAG3110_exitStandby();

    return ret;
}



uint8_t MAG3110_triggerMeasurement(void)
{

    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2] = { 0x00, 0x00 };


    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1);
    if( ret == I2C_OK )
    {
        cmd[0] = cmd[0] | 0x02;

        DebugPrint("MAG3110 REG1 = 0x%x\r\n", cmd[0]);
        ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG1, cmd, 1);
    }

    return ret;
}


//Note that AUTO_MRST_EN will always read back as 0
//Therefore we must explicitly set this bit every time we modify CTRL_REG2
uint8_t MAG3110_rawData(uint8_t raw)
{

    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2] = { 0x00, 0x00 };


    if(raw) //Turn on raw (non-user corrected) mode
    {
        MAG3110_raw_mode = 1;
        cmd[0] = MAG3110_AUTO_MRST_EN | (0x01 << 5);
        ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG2, cmd, 1);

    }
    else //Turn off raw mode
    {
        MAG3110_raw_mode = 0;
        cmd[0] = MAG3110_AUTO_MRST_EN & ~(0x01 << 5);
        ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG2, cmd, 1);
    }

    return ret;
}

//Note that AUTO_MRST_EN is recommended to set as 1
uint8_t MAG3110_setAutoMrst(uint8_t enable)
{

    uint8_t ret = I2C_ERROR_ACK;
    uint8_t cmd[2] = { 0x00, 0x00 };

    ret = swI2cMst1_Read(MAG3110_ADDRESS, MAG3110_CTRL_REG2, cmd, 1);
    if( ret == I2C_OK )
    {
        if(enable) //Turn on auto reset mode
        {
            MAG3110_auto_mrst = 1;

            cmd[0] = cmd[0] | MAG3110_AUTO_MRST_EN;
            ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG2, cmd, 1);

        }
        else //Turn off raw mode
        {
            MAG3110_auto_mrst = 0;

            cmd[0] = cmd[0] & ~MAG3110_AUTO_MRST_EN;
            ret = swI2cMst1_Write(MAG3110_ADDRESS, MAG3110_CTRL_REG2, cmd, 1);
        }
    }

    return ret;
}



/*############################################################################*/
/*#                               	API
/*############################################################################*/


/*
void get_Moving_Avg_Int(int16_t new_val[])
{
    int i;

    // Loop for X, Y, Z data of Magnetic sensor
    for(i=0; i<3; i++)
    {
        sum_int[i] = new_val[i] + pre_val_int[i][0] + pre_val_int[i][1] + pre_val_int[i][2];
        avg_int[i] = sum_int[i]/4;

        pre_val_int[i][2] = pre_val_int[i][1];
        pre_val_int[i][1] = pre_val_int[i][0];
        pre_val_int[i][0] = new_val[i];

        new_val[i] = avg_int[i];
    }

}
*/

void MagneticSensor_Cal(void)
{
    uint8_t count;

    MAG3110_old_X = 0;
    MAG3110_old_Y = 0;
    MAG3110_old_Z = 0;

    car_detected_count = 0;
    no_car_detected_count = 0;

    for(count = 0; count < CAL_SAMPLE_COUNT; count++)
    {
        while(get_bit(P36D) == 0);

        MAG3110_readMag(&MAG3110_X, &MAG3110_Y, &MAG3110_Z);
        MAG3110_old_X += abs(MAG3110_X);
        MAG3110_old_Y += abs(MAG3110_Y);
        MAG3110_old_Z += abs(MAG3110_Z);
        blinkLED(RED_LED);
        blinkLED(GREEN_LED);
        wdt_clear();
    }

    MAG3110_old_X = MAG3110_old_X / CAL_SAMPLE_COUNT;
    MAG3110_old_Y = MAG3110_old_Y / CAL_SAMPLE_COUNT;
    MAG3110_old_Z = MAG3110_old_Z / CAL_SAMPLE_COUNT;

    offLED(GREEN_LED);
    offLED(RED_LED);
    DebugPrint("MagneticSensor Base X:%04d, Y:%04d, Z:%04d\r\n", (int)MAG3110_old_X, (int)MAG3110_old_Y, (int)MAG3110_old_Z);
}



uint8_t Run_MagneticSensorCtrl(void)
{
    int16_t X, Y, Z;
    uint8_t state = 0;

#ifdef PACKING_SENSOR_TEST
    if(MAG3110_readMag(&X, &Y, &Z) == I2C_OK)
    {
        MAG3110_X = abs(X);
        MAG3110_Y = abs(Y);
        MAG3110_Z = abs(Z);

        MAG3110_X = abs(MAG3110_old_X - MAG3110_X);
        MAG3110_Y = abs(MAG3110_old_Y - MAG3110_Y);
        MAG3110_Z = abs(MAG3110_old_Z - MAG3110_Z);
        DebugPrint("X:%04d, Y:%04d, Z:%04d\r\n", (int)MAG3110_X, (int)MAG3110_Y, (int)MAG3110_Z);
    }
#else // PACKING_SENSOR_TEST



    MAG3110_readMag(&X, &Y, &Z);
    MAG3110_X = abs(X);
    MAG3110_Y = abs(Y);
    MAG3110_Z = abs(Z);

    if((MAG3110_X > MAG3110_old_X + THR_VAL) || (MAG3110_Y > MAG3110_old_Y + THR_VAL) || (MAG3110_Z > MAG3110_old_Z + THR_VAL))
    {
        if(car_detected == CAR_OUT) // No Car
        {
            car_detected_count ++;
            if(car_detected_count > SAMPLE_COUNT)
            {
                car_detected_count = 0;
                car_detected = CAR_IN;
                offLED(GREEN_LED);
                offLED(RED_LED);
                state = 1;
            }
            else
            {
                blinkLED(GREEN_LED);
                blinkLED(RED_LED);

                delay_Ms(250);

                blinkLED(GREEN_LED);
                blinkLED(RED_LED);
            }
        }

        DebugPrint("car detected: Yes\r\n");
        //DebugPrint("X:%04d, Y:%04d, Z:%04d\r\n", (int)MAG3110_X, (int)MAG3110_Y, (int)MAG3110_Z);
        no_car_detected_count = 0;
    }
    else
    {
        if(car_detected == CAR_IN) // Yes Car
        {
            no_car_detected_count ++;
            if(no_car_detected_count > SAMPLE_COUNT)
            {
                offLED(RED_LED);
                offLED(GREEN_LED);
                no_car_detected_count = 0;
                state = 1;
                car_detected = CAR_OUT;
                DebugPrint("car detected: No\r\n");
            }
            else
            {
                blinkLED(RED_LED);
                blinkLED(GREEN_LED);
            }
        }
        else if(car_detected_count > 0)
        {
            car_detected_count = 0;
            offLED(RED_LED);
            offLED(GREEN_LED);
        }
    }

    DebugPrint("X:%04d, Y:%04d, Z:%04d\r\n", (int)MAG3110_X, (int)MAG3110_Y, (int)MAG3110_Z);
#endif // PACKING_SENSOR
    return  state;
}










