

/*  */
#include <string.h>
#include <stdio.h>

/* MCU */
#include "rdwr_reg.h"
#include "mcu.h"

#include "clock.h"
#include "wdt.h"
#include "irq.h"
#include "gpio_common.h"

#include "uart0.h"
#include "uartf0.h"
#include "i2c0.h"
#include "ssio0.h"
#include "ssio_common.h"
#include "vls.h"

/*   */
#include "Test_Mode.h"
#include "User_System.h"
#include "User_Generic.h"
#include "User_Console.h"
#include "User_Board.h"

#include "Driver_DataFlash.h"
#include "Driver_MotionSensor.h"

/* Driver */
#include "Driver_Sigfox.h"
#include "Driver_Adc.h"
#include "Driver_Soft_I2c.h"
#include "Driver_Soft_I2c1.h"
#include "Driver_Charger.h"


/* Low-Speed Time Base Counter */
#define TBC_T128HZ  0x00
#define TBC_T64HZ   0x01
#define TBC_T32HZ   0x02
#define TBC_T16HZ   0x03
#define TBC_T8HZ    0x04
#define TBC_T4HZ    0x05
#define TBC_T2HZ    0x06
#define TBC_T1HZ    0x07

#define KEY_DEBOUNCE_TIME   1
#define KEY_HOLD_TIME_CARRIER   2
#define KEY_HOLD_TIME_DOWNLINK  9


#ifdef WATER_LEAKAGE_SENSOR
unsigned char waterOldState = WATER_NO_LEAK;
unsigned char waterState = WATER_NO_LEAK;
#endif // WATER_LEAKAGE_SENSOR

#ifdef  GAS_COUNTER
unsigned int gasCounter = 0;
unsigned int gasOldCounter = 0;
#endif // GAS_COUNTER

unsigned char MAG3110_Int = 0;


/*############################################################################*/
/*#                   Global  Variable
/*############################################################################*/
unsigned long SysTickets = 0; // TBC_T1HZ
unsigned char SysTicketFlag = FLAG_CLR;

unsigned long schedule_Time = SCHEDUCLE_TIME;
unsigned long schedule_Stamp = 0;

unsigned char keyState = STATE_KEY_RELEASED;
unsigned char keyOldState = STATE_KEY_RELEASED;
unsigned char keyEvent = KEY_EVENT_NONE;
unsigned char keyOldEvent = KEY_EVENT_NONE;

unsigned char batteryCapacity = 0;

/* Global Buffer  */
unsigned char i2c_rxData[I2C_DATA_SIZE_MAX];
unsigned char i2c_txData[I2C_DATA_SIZE_MAX];

unsigned char Spi_rxData[SPI_DATA_SIZE_MAX];    /**< The data of read           */
unsigned char Spi_txData[SPI_DATA_SIZE_MAX];    /**< The data of write          */



/*############################################################################*/
/*#                   Local  Variable
/*############################################################################*/
static unsigned char key_Hold_Timestamp = 0;
static unsigned int key_timestamp = 0; //
static unsigned int key_timepress = 0;
static unsigned int key_timerelease = 0;




/********************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*********************************************************************************************************/
void delay_Us(unsigned short value)
{
    while(value--)
    {
        NOP_COUNT
    }
}

/********************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
*********************************************************************************************************/
void delay_Ms(unsigned short value)
{
    while(value--)
    {
        delay_Us(US_DELAY_COUNT);
    }
}



/**
 * Initialize peripherals
 *
 * @param           None
 * @return          None
 */
void s_initPeri( void )
{
    /*** WDT */
    wdt_init( WDTMOD_WDT1|WDTMOD_WDT0 ); //WDT_RESET_8S
    wdt_clear();

    /*############################################################################*/
    /*** disable interrupt */
    //__DI();

    /*############################################################################*/
    /*** Disable unused block*/
    set_reg16(BLKCON01, 0xFFFF); //Disable timer0-7, FTM 0-3
    set_bit(DMD0);  // Disable operating the melody driver 0
    set_bit(DCMP0); // Disable operating the analog comparator 0
    set_bit(DCMP1); // Disable operating the analog comparator 1
  
    vls_off();
    vls_init(0x04, VLS_RST_ENA_INT_DIS1, VLS_SINGLE_1);    //Initialize VLS, below thershold reset, single mode
    vls_on();

    //set_bit(DLLD);  // Disable operating LLD (initial value)
    clear_bit(DLLD);  // Enable operating LLD (initial value)
    set_bit(DRAD);  // Disable operating the RC oscillation type A/D converter


    /*############################################################################*/
    /*** Initialize Clock */
    setHsRcClock(CLK_SYSC, CLK_LOSCON_DIS);

    /*############################################################################*/
    /*** Low battery LED Red*/
    clear_bit(P46D);
    clear_bit( P46DIR );			/* Output */
    set_bit( P46C0 );				/* CMOS */
    set_bit( P46C1 );
    clear_bit( P46MD0 );			/* GPIO mode */
    clear_bit( P46MD1 );

    /*** Power LED green*/
    clear_bit(P42D);
    clear_bit( P42DIR );			/* Output */
    set_bit( P42C0 );				/* CMOS */
    set_bit( P42C1 );
    clear_bit( P42MD0 );			/* GPIO mode */
    clear_bit( P42MD1 );

    /*############################################################################*/
    /*** Initialize Time Base Counter */
    set_reg8(LTBINTL, TBC_T1HZ | TBC_T64HZ); // LTB0-1Hz, LTB1-TBC_T64HZ
#ifdef P31_SW_ENABLE
    /*############################################################################*/
    /*** P31 - SW KEY */
    set_bit( P31DIR );  /* Input Hi-Z */
    irq_ext3_init(EXIn_EDGE_BOTH, EXIn_SAMPLING_ENA, EXIn_FILTER_ENA, EXIn_PORT_SEL_P31);
#endif    

    /*############################################################################*/
    /*** Sogfox Control Pin*/
    
    /* P56 - WAKEUP Sigfox */
    clear_bit( P56DIR );			/* Output */
    set_bit( P56C0 );				/* CMOS */
    set_bit( P56C1 );
    clear_bit( P56MD0 );			/* GPIO mode */
    clear_bit( P56MD1 );
    
    P56D = 1;               /* if the Sigfox Module not use, pull it up */
 
    /* P57 - RESET Sigfox */
    clear_bit( P57DIR );			/* Output */
    set_bit( P57C0 );				/* CMOS */
    set_bit( P57C1 );
    clear_bit( P57MD0 );			/* GPIO mode */
    clear_bit( P57MD1 );
 

    /* P11 - PWR Sigfox */
    clear_bit( P11DIR );			/* Output */
    set_bit( P11C0 );				/* CMOS */
    set_bit( P11C1 );

    /*** Sigfox UART */
    uartf0_init( UARTF_PARAM_MODE, UARTF_PARAM_CLOCK, UARTF_PARAM_DLR );

    /*############################################################################*/
    #if defined (GAS_COUNTER) || defined (WATER_LEAKAGE_SENSOR)    
        GPIO_INPUT_HI_Z( TRIGGER_COUNTER_PIN );  /* Input Hi-Z */
        irq_ext4_init(EXIn_EDGE_BOTH, EXIn_SAMPLING_ENA, EXIn_FILTER_ENA, INT_PORT_SEL(TRIGGER_COUNTER_PIN));
        
        GPIO_INPUT_HI_Z( NH3_COUNTER_PIN );
        irq_ext5_init(EXIn_EDGE_BOTH, EXIn_SAMPLING_ENA, EXIn_FILTER_ENA, INT_PORT_SEL(NH3_COUNTER_PIN));
    #endif // defined

    /*############################################################################*/
    
    /*############################################################################*/
    #ifdef USE_ADC_VOLTAGE_DETECT
    SA_Adc_Initialize();
    irq_sad_clearIRQ();
    irq_sad_ena();
    #endif // USE_ADC_VOLTAGE_DETECT

    /* first check the pin 32 is pull low, if yes, go to test mode */
    set_bit( P32DIR );        /* Input */
    clear_bit( P32C0 );         /* Internal Pull-high input */
    set_bit( P32C1 );
    write_bit( P32D, 1 );    
    
    if(get_bit(P32D) == 0) {
        // go to test Mode
        onLED(GREEN_LED);
        onLED(RED_LED);
        uart0_init( UART_PARAM_MODE0, UART_PARAM_MODE1, UART0_115200BPS );
        __EI();        
        Test_Mode();
        __DI();
    }    
    #ifdef USE_DEBUG_CONSOLE
    /*** Console UART  Init*/
    uart0_init_txOnly( UART_PARAM_MODE0, UART_PARAM_MODE1, UART0_115200BPS );
    #endif // USE_DEBUG_CONSOLE


#ifdef P31_SW_ENABLE
    /*** enable interrupt */
    /* P31 SW KEY */
    irq_ext3_clearIRQ();
    irq_ext3_ena(); //
#endif    

    /* P34 - WATER DETECT */
    irq_ext4_clearIRQ();
    irq_ext4_ena();
    
    /* P34 - WATER DETECT */
    irq_ext5_clearIRQ();
    irq_ext5_ena();

    /* TBC0 1s */
    irq_tbc0_clearIRQ();
    irq_tbc0_ena();

    /* TBC1 15ms*/
    irq_tbc1_clearIRQ();
    irq_tbc1_dis();

    __EI();

}



/**
 * P31 SW KEY
 *
 */
void smpl_procExi3Int(void)
{
#ifdef P31_SW_ENABLE
    key_timestamp = 0;

    if (P31D == FLAG_CLR)
    {
        /* Key Press */
        if (keyState == STATE_KEY_RELEASED)
        {
            key_timepress = key_timestamp;
            keyState = STATE_KEY_DEBOUNCE;
        }
    }
    else
    {
        /* Key Release */
        key_timerelease = key_timestamp;
        keyState = STATE_KEY_DEBOUNCE;
    }

    irq_tbc1_ena();
#endif    
}

#ifdef P31_SW_ENABLE
void get_Key(void)
{
    if(keyState == STATE_KEY_CARRIER)
    {
        key_Hold_Timestamp++;
        if(key_Hold_Timestamp >= KEY_HOLD_TIME_DOWNLINK)
        {
            keyState = STATE_KEY_DOWNLINK;
            keyEvent = KEY_EVENT_DOWNLINK;
        }
    }
    else if (keyState == STATE_KEY_PRESSED)
    {
        key_Hold_Timestamp++;
        if (key_Hold_Timestamp >= KEY_HOLD_TIME_CARRIER)
        {
            keyState = STATE_KEY_CARRIER;
            keyEvent = KEY_EVENT_CARRIER;
        }
    }
}

/**
 *
 *
 */
void check_Key(void)
{
    /** Determine the key state */
    if(P31D == FLAG_CLR)
    {
        if (keyState == STATE_KEY_DEBOUNCE)
        {
            if (key_timestamp - key_timepress > KEY_DEBOUNCE_TIME)
            {
                keyState = STATE_KEY_PRESSED;
                keyEvent = KEY_EVENT_PRESSED;
                key_Hold_Timestamp = 0;
                irq_tbc1_clearIRQ();
                irq_tbc1_dis();
            }
        }
    }
    else
    {
        /*  */
        if (keyState == STATE_KEY_DEBOUNCE)
        {
            if (key_timestamp-key_timerelease > KEY_DEBOUNCE_TIME)
            {
                keyState = STATE_KEY_RELEASED;
                keyEvent = KEY_EVENT_RELEASED;

                irq_tbc1_clearIRQ();
                irq_tbc1_dis();
                key_timestamp = 0;
            }
        }
    }
}
#endif



/**
 * Wdt interrupt routine
 *
 */
void smpl_procWdtHandle( void )
{

}

/**
 * LTBC0 interrupt routine
 *
 */
void smpl_LTBC0_Handle( void )
{
    SysTickets++;
    SysTicketFlag = FLAG_SET;
#ifdef P31_SW_ENABLE    
    get_Key();
#endif    
}


/**
 * LTBC1 interrupt routine
 *
 */
void smpl_LTBC1_Handle( void )
{
#ifdef P31_SW_ENABLE
    key_timestamp++;
    check_Key();
#endif    
}

/**
 * LTBC2 interrupt routine
 *
 */
void smpl_LTBC2_Handle( void )
{

}



/**
 * P34 - WATER DETECT or Gas Counter
 *
 */
void smpl_procExi4Int(void)
{

#ifdef WATER_LEAKAGE_SENSOR
    if(GET_BIT(TRIGGER_COUNTER_PIN) == 1)
    {
        waterState = WATER_LEAK;
    }
    else
    {
        waterState = WATER_NO_LEAK;
    }
#endif // WATER_LEAKAGE_SENSOR

}

void smpl_procExi5Int(void)
{


    if(GET_BIT(NH3_COUNTER_PIN) == 1)
    {
        waterState = WATER_LEAK;
    }
    else {
        waterState = WATER_NO_LEAK;
    }


}

void smpl_procExi6Int(void)
{
    MAG3110_Int = 1;
}









