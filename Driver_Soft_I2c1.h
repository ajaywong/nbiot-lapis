/*****************************************************************************
 Driver_Soft_I2c1.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */

#ifndef _DRIVER_SOFT_I2C1_H
#define _DRIVER_SOFT_I2C1_H





//0xFFFF ~130ms @8MHz, 65Ms @16Mhz
//0xFF //~512us 5000~ 10ms @8MHz
#define I2C_TIMEOUT     ((unsigned int)0xF4240) //~200ms @8MHz

//#define I2C_TIMEOUT     ((unsigned int)0x1E8480) //~200ms @16MHz

#define I2C_OK              0
#define I2C_ERROR_ACK       1
#define I2C_ERROR_TIMEOUT   2


void swI2cMst1_init(void);
unsigned char swI2cMst1_Read(unsigned char SlaveAddress, unsigned char reg, unsigned char* data, unsigned short len);
unsigned char swI2cMst1_Write(unsigned char SlaveAddress, unsigned char reg, unsigned char* data, unsigned short len);

#endif
