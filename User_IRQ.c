/*****************************************************************************
 User_IRQ.c

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 * @file    010_UARTF\UartFCommunicateSample\irq_UartFCommunicateSample.c
 *
 * This module is interrupt routine for UartFCommunicateSample.
 *
 */

 /* System Include */
#include "mcu.h"
#include "rdwr_reg.h"
#include "irq.h"

#include "uart0.h"
#include "i2c0.h"
#include "ssio0.h"

/* User Include */
#include "User_System.h"

/* Driver */
#include "Driver_Sigfox.h"
#include "Driver_Adc.h"


/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/

/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/

/*############################################################################*/
/*#                               Prototype                                  #*/
/*############################################################################*/
static void s_handlerWDTINT( void );
static void s_handlerEXI0INT( void );
static void s_handlerEXI1INT( void );
static void s_handlerEXI2INT( void );
static void s_handlerEXI3INT( void );
static void s_handlerEXI4INT( void );
static void s_handlerEXI5INT( void );
static void s_handlerEXI6INT( void );
static void s_handlerEXI7INT( void );
static void s_handlerSIO0INT( void );
static void s_handlerSIOF0INT( void );
static void s_handlerI2C0INT( void );
static void s_handlerI2C1INT( void );
static void s_handlerUA0INT( void );
static void s_handlerUA1INT( void );
static void s_handlerUAF0INT( void );
static void s_handlerLOSCINT( void );
static void s_handlerVLSINT( void );
static void s_handlerMD0INT( void );
static void s_handlerSADINT( void );
static void s_handlerRADINT( void );
static void s_handlerCMP0INT( void );
static void s_handlerCMP1INT( void );
static void s_handlerTM0INT( void );
static void s_handlerTM1INT( void );
static void s_handlerTM2INT( void );
static void s_handlerTM3INT( void );
static void s_handlerTM4INT( void );
static void s_handlerTM5INT( void );
static void s_handlerTM6INT( void );
static void s_handlerTM7INT( void );
static void s_handlerFTM0INT( void );
static void s_handlerFTM1INT( void );
static void s_handlerFTM2INT( void );
static void s_handlerFTM3INT( void );
static void s_handlerLTBC0INT( void );
static void s_handlerLTBC1INT( void );
static void s_handlerLTBC2INT( void );

/*=== set Interrupt Vector ===*/
/* If enables multiple interrupts,              */
/* specify '2' in the INTERRUPT category field. */
#pragma INTERRUPT   s_handlerWDTINT     0x0008  1
#pragma INTERRUPT   s_handlerEXI0INT    0x0010  1
#pragma INTERRUPT   s_handlerEXI1INT    0x0012  1
#pragma INTERRUPT   s_handlerEXI2INT    0x0014  1
#pragma INTERRUPT   s_handlerEXI3INT    0x0016  1
#pragma INTERRUPT   s_handlerEXI4INT    0x0018  1
#pragma INTERRUPT   s_handlerEXI5INT    0x001A  1
#pragma INTERRUPT   s_handlerEXI6INT    0x001C  1
#pragma INTERRUPT   s_handlerEXI7INT    0x001E  1
#pragma INTERRUPT   s_handlerSIO0INT    0x0020  1
#pragma INTERRUPT   s_handlerSIOF0INT   0x0022  1
#pragma INTERRUPT   s_handlerI2C0INT    0x0024  1
#pragma INTERRUPT   s_handlerI2C1INT    0x0026  1
#pragma INTERRUPT   s_handlerUA0INT     0x0028  1
#pragma INTERRUPT   s_handlerUA1INT     0x002A  1
#pragma INTERRUPT   s_handlerUAF0INT    0x002C  1
#pragma INTERRUPT   s_handlerLOSCINT    0x003A  1
#pragma INTERRUPT   s_handlerVLSINT     0x003C  1
#pragma INTERRUPT   s_handlerMD0INT     0x003E  1
#pragma INTERRUPT   s_handlerSADINT     0x0040  1
#pragma INTERRUPT   s_handlerRADINT     0x0042  1
#pragma INTERRUPT   s_handlerCMP0INT    0x0048  1
#pragma INTERRUPT   s_handlerCMP1INT    0x004A  1
#pragma INTERRUPT   s_handlerTM0INT     0x0050  1
#pragma INTERRUPT   s_handlerTM1INT     0x0052  1
#pragma INTERRUPT   s_handlerTM2INT     0x0054  1
#pragma INTERRUPT   s_handlerTM3INT     0x0056  1
#pragma INTERRUPT   s_handlerTM4INT     0x0058  1
#pragma INTERRUPT   s_handlerTM5INT     0x005A  1
#pragma INTERRUPT   s_handlerTM6INT     0x005C  1
#pragma INTERRUPT   s_handlerTM7INT     0x005E  1
#pragma INTERRUPT   s_handlerFTM0INT    0x0060  1
#pragma INTERRUPT   s_handlerFTM1INT    0x0062  1
#pragma INTERRUPT   s_handlerFTM2INT    0x0064  1
#pragma INTERRUPT   s_handlerFTM3INT    0x0066  1
#pragma INTERRUPT   s_handlerLTBC0INT   0x0070  1
#pragma INTERRUPT   s_handlerLTBC1INT   0x0072  1
#pragma INTERRUPT   s_handlerLTBC2INT   0x0074  1


/*############################################################################*/
/*#                          Interrupt handler                               #*/
/*############################################################################*/
static void s_handlerWDTINT( void )
{
	smpl_procWdtHandle();
}

static void s_handlerEXI0INT( void )
{
	/* No process */
}

static void s_handlerEXI1INT( void )
{
	/* No process */
}

static void s_handlerEXI2INT( void )
{
	/* No process */
}

static void s_handlerEXI3INT( void )
{
	smpl_procExi3Int();
}

static void s_handlerEXI4INT( void )
{
	smpl_procExi4Int();
}

static void s_handlerEXI5INT( void )
{
	smpl_procExi5Int();
}

static void s_handlerEXI6INT( void )
{
	smpl_procExi6Int();
}

static void s_handlerEXI7INT( void )
{
	/* No process */
}

static void s_handlerSIO0INT( void )
{
	/* No process */
	smpl_procSio0Int();
}

static void s_handlerSIOF0INT( void )
{
	/* No process */
}

static void s_handlerI2C0INT( void )
{
	/* No process */
    smpl_procI2C0Int();
}

static void s_handlerI2C1INT( void )
{
	/* No process */
}

static void s_handlerUA0INT( void )
{
	uart0_continueRead();
}

static void s_handlerUA1INT( void )
{
	uart0_continueWrite();
}

static void s_handlerUAF0INT( void )
{
    smpl_procUartfInt();
}

static void s_handlerLOSCINT( void )
{
	/* No process */
}

static void s_handlerVLSINT( void )
{
	/* No process */
}

static void s_handlerMD0INT( void )
{
	/* No process */
}

static void s_handlerSADINT( void )
{
	smpl_procSaAdcInt();
}

static void s_handlerRADINT( void )
{
	/* No process */
}

static void s_handlerCMP0INT( void )
{
	/* No process */
}

static void s_handlerCMP1INT( void )
{
	/* No process */
}

static void s_handlerTM0INT( void )
{
	/* No process */
}

static void s_handlerTM1INT( void )
{
	/* No process */
}

static void s_handlerTM2INT( void )
{
	/* No process */
}

static void s_handlerTM3INT( void )
{
	/* No process */
}

static void s_handlerTM4INT( void )
{
	/* No process */
}

static void s_handlerTM5INT( void )
{
	/* No process */
}

static void s_handlerTM6INT( void )
{
	/* No process */
}

static void s_handlerTM7INT( void )
{
	/* No process */
}

static void s_handlerFTM0INT( void )
{
	/* No process */
}

static void s_handlerFTM1INT( void )
{
	/* No process */
}

static void s_handlerFTM2INT( void )
{
	/* No process */
}

static void s_handlerFTM3INT( void )
{
	/* No process */
}

static void s_handlerLTBC0INT( void )
{
	smpl_LTBC0_Handle();
}

static void s_handlerLTBC1INT( void )
{
#ifdef P31_SW_ENABLE
	smpl_LTBC1_Handle();
#endif  
}

static void s_handlerLTBC2INT( void )
{
	smpl_LTBC2_Handle();
}

