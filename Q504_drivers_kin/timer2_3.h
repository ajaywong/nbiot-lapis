/*****************************************************************************
 timer2_3.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    timer2_3.h
 *
 * This file is API definition for timer (channel No.2 and No.3).
 *
 */
#ifndef _TIMER2_3_H_
#define _TIMER2_3_H_

#include "rdwr_reg.h"
#include "timer_common.h"

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Initialize timer2
 *
 * @param[in]   tmncon  A value of setting TM2CON register. <br>
 *                      Specify the logical sum of the following items.
 * @arg                     Clock source      : TM_CS_LSCLK, or TM_CS_OSCLK, or TM_CS_EXTCLK
 * @arg                     Division ratio    : TM_DIV1, or TM_DIV4, or TM_DIV8, or TM_DIV16, or TM_DIV32, or TM_DIV64
 * @arg                     Timer             : TM_MODE_8BIT, or TM_MODE_16BIT
 * @arg                     Mode              : TM_OST_REROAD, or TM_OST_ONESHOT
 * @return      None
 */
#define         timer2_init( tmncon )   write_reg8( TM2CON, tmncon )
/**
 * Initialize timer3
 *
 * @param[in]   tmncon  A value of setting TM3CON register. <br>
 *                      Specify the logical sum of the following items.
 * @arg                     Clock source      : TM_CS_LSCLK, or TM_CS_OSCLK, or TM_CS_EXTCLK
 * @arg                     Division ratio    : TM_DIV1, or TM_DIV4, or TM_DIV8, or TM_DIV16, or TM_DIV32, or TM_DIV64
 * @arg                     Timer             : Only TM_MODE_8BIT
 * @arg                     Mode              : TM_OST_REROAD, or TM_OST_ONESHOT
 * @return      None
 */
#define         timer3_init( tmncon )   write_reg8( TM3CON, tmncon )
/**
 * Start timer2
 *
 * @param       -
 * @return      None
 */
#define         timer2_start()          set_bit( T2RUN )
/**
 * Start timer3
 *
 * @param       -
 * @return      None
 */
#define         timer3_start()          set_bit( T3RUN )
/**
 * Stop timer2
 *
 * @param       -
 * @return      None
 */
#define         timer2_stop()           set_bit( T2STP )
/**
 * Stop timer3
 *
 * @param       -
 * @return      None
 */
#define         timer3_stop()           set_bit( T3STP )

void            timer2_setCnt( unsigned short cnt );
void            timer3_setCnt( unsigned short cnt );

#endif /*_TIMER2_3_H_*/

