/*****************************************************************************
 timer2_3.c

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    timer2_3.c
 *
 * This module is Timer drivers(channel No.2 and No.3).
 *
 */
#include "mcu.h"
#include "rdwr_reg.h"
#include "timer2_3.h"
/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/

/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/

/*############################################################################*/
/*#                               Prototype                                  #*/
/*############################################################################*/

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Initialize the value to be compared with the timer2 counter and an 8-bit/16-bit binary counter
 * @note Set TMnD when the timer stops.
 *
 * @param[in]   cnt     If timer2 is 8bit mode, then this program set compare value to TM2D register.<br>
 *                      If timer2 is 16bit mode, then this program set compare value to TM23D register.
 * @return      None
 */
void timer2_setCnt( unsigned short cnt )
{
	/*--- mode select ---*/
	if( get_bit( T23M16 ) == 0 ) {
	    /*--- 8bit timer mode ---*/
	    write_reg8( TM2C, 0x00 );                           /* counter clear */
	    write_reg8( TM2D, (unsigned char)cnt );             /* compare value set */
	}
	else {
	    /*--- 16bit timer mode ---*/
	    write_reg16( TM23C, 0x0000 );                       /* counter clear */
	    write_reg16( TM23D, (unsigned short)cnt );          /* compare value set */
	}
}

/**
 * Initialize the value to be compared with the timer3 counter and an 8-bit binary counter
 * @note Set TMnD when the timer stops.
 *
 * @param[in]   cnt     the value of TM3D register
 * @return      None
 */
void timer3_setCnt( unsigned short cnt )
{
	/*--- Only 8bit timer mode ---*/
	write_reg8( TM3C, 0x00 );                               /* counter clear */
	write_reg8( TM3D, (unsigned char)cnt );                 /* compare value set */
}

