/*****************************************************************************
 uartf0.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    uartf0.h
 *
 * This file is API definition for 'UARTF(UART with FIFO)'(channel No.0) drivers.
 *
 */

#ifndef _UARTF0_H_
#define _UARTF0_H_

#include "rdwr_reg.h"
#include "uartf_common.h"

/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Getting reception data
 *
 * @param       -
 * @return      A reception data <br>
 *              (A value of UAF0BUFL register)
 */
#define         uartf0_getc()           read_reg8( UAF0BUFL )

/**
 * Sending transmission of data
 *
 * @param[in]   data    A transmission data
 *              (Setting value of UAF0BUFL register)
 * @return  None
 */
#define         uartf0_putc( data )     write_reg8( UAF0BUFL, (unsigned char)(data) )

/**
 * Checking UARTF(Writing) busy status
 *
 * @param       -
 * @retval      0   busy status : Normal(able to write)
 * @retval      1   busy status : Busy
 */
#define         uartf0_checkWriteBusy() ( (unsigned char)( ! get_bit( UF0THRE )) )

/**
 * Checking UARTF(Reading) ready status
 *
 * @param       -
 * @retval      0   ready status : Normal
 * @retval      1   ready status : Ready(able to read, and reception data is valid)
 */
#define         uartf0_checkReadReady() ( (unsigned char)get_bit( UF0DR ) )

/**
 * Trig interrupt
 *
 * Request UARTF interrupt.
 *
 * @param       -
 * @return      None
 */
#define         uartf0_trigIRQ()        set_bit( UF0IRQ )

/**
 * Getting interrupt cause
 *
 * Get causes which UARTF interrupt is requested.
 *
 * @param       -
 * @return      Interrupt causes <br>
 *              (A value of UAF0IIR register)
 */
#define         uartf0_getIntCause()    read_reg16( UAF0IIR )

/**
 * Getting UARTF status
 *
 *
 * @param       -
 * @return      UARTF line status <br>
 *              (A value of UAF0LSR register)
 */
#define         uartf0_getStatus()      read_reg16( UAF0LSR )

/**
 * Clear RBR interrupt cause
 *
 * Read RBR to clear interrupt cause. <br>
 * ex. Character timeout is occurred.
 *
 * @param       -
 * @return      RBR stored data(Dummy read) <br>
 *              (A value of UAF0BUFL register)
 */
#define         uartf0_clearRbrIntCause() uartf0_getc()

/**
 * Crear Write FIFO
 *
 * @param       -
 * @return      None
 */
 #define        uartf0_clearWriteFifo()   set_bit( UF0TFR );

/**
 * Crear Read FIFO
 *
 * @param       -
 * @return      None
 */
 #define        uartf0_clearReadFifo()    set_bit( UF0RFR );

void            uartf0_init( unsigned short uafnmod, unsigned short uafncaj, unsigned short brDivisorLatch );
void            uartf0_write( unsigned char *data, unsigned int size, cbfUartF_t func );
void            uartf0_read( unsigned char *data, unsigned int size, cbfUartF_t func );
int             uartf0_continueWrite( void );
int             uartf0_continueRead( void );


/*############################################################################*/
/*#                            User Code                                     #*/
/*############################################################################*/
extern unsigned char reply_count;

#endif /*_UARTF0_H_*/

