/*****************************************************************************
 timer0_1.c

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    timer0_1.c
 *
 * This module is Timer drivers(channel No.0 and No.1).
 *
 */
#include "mcu.h"
#include "rdwr_reg.h"
#include "timer0_1.h"
/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/

/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/

/*############################################################################*/
/*#                               Prototype                                  #*/
/*############################################################################*/

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Initialize the value to be compared with the timer0 counter and an 8-bit/16-bit binary counter
 * @note Set TMnD when the timer stops.
 *
 * @param[in]   cnt     If timer0 is 8bit mode, then this program set compare value to TM0D register.<br>
 *                      If timer0 is 16bit mode, then this program set compare value to TM01D register.
 * @return      None
 */
void timer0_setCnt( unsigned short cnt )
{
	/*--- mode select ---*/
	if( get_bit( T01M16 ) == 0 ) {
	    /*--- 8bit timer mode ---*/
	    write_reg8( TM0C, 0x00 );                           /* counter clear */
	    write_reg8( TM0D, (unsigned char)cnt );             /* compare value set */
	}
	else {
	    /*--- 16bit timer mode ---*/
	    write_reg16( TM01C, 0x0000 );                       /* counter clear */
	    write_reg16( TM01D, (unsigned short)cnt );          /* compare value set */
	}
}

/**
 * Initialize the value to be compared with the timer1 counter and an 8-bit binary counter
 * @note Set TMnD when the timer stops.
 *
 * @param[in]   cnt     the value of TM1D register
 * @return      None
 */
void timer1_setCnt( unsigned short cnt )
{
	/*--- Only 8bit timer mode ---*/
	write_reg8( TM1C, 0x00 );                               /* counter clear */
	write_reg8( TM1D, (unsigned char)cnt );                 /* compare value set */
}

