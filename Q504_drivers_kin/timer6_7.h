/*****************************************************************************
 timer6_7.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    timer6_7.h
 *
 * This file is API definition for timer (channel No.6 and No.7).
 *
 */
#ifndef _TIMER6_7_H_
#define _TIMER6_7_H_

#include "rdwr_reg.h"
#include "timer_common.h"

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Initialize timer6
 *
 * @param[in]   tmncon  A value of setting TM6CON register. <br>
 *                      Specify the logical sum of the following items.
 * @arg                     Clock source      : TM_CS_LSCLK, or TM_CS_OSCLK, or TM_CS_EXTCLK
 * @arg                     Division ratio    : TM_DIV1, or TM_DIV4, or TM_DIV8, or TM_DIV16, or TM_DIV32, or TM_DIV64
 * @arg                     Timer             : TM_MODE_8BIT, or TM_MODE_16BIT
 * @arg                     Mode              : TM_OST_REROAD, or TM_OST_ONESHOT
 * @return      None
 */
#define         timer6_init( tmncon )   write_reg8( TM6CON, tmncon )
/**
 * Initialize timer7
 *
 * @param[in]   tmncon  A value of setting TM7CON register. <br>
 *                      Specify the logical sum of the following items.
 * @arg                     Clock source      : TM_CS_LSCLK, or TM_CS_OSCLK, or TM_CS_EXTCLK
 * @arg                     Division ratio    : TM_DIV1, or TM_DIV4, or TM_DIV8, or TM_DIV16, or TM_DIV32, or TM_DIV64
 * @arg                     Timer             : Only TM_MODE_8BIT
 * @arg                     Mode              : TM_OST_REROAD, or TM_OST_ONESHOT
 * @return      None
 */
#define         timer7_init( tmncon )   write_reg8( TM7CON, tmncon )
/**
 * Start timer6
 *
 * @param       -
 * @return      None
 */
#define         timer6_start()          set_bit( T6RUN )
/**
 * Start timer7
 *
 * @param       -
 * @return      None
 */
#define         timer7_start()          set_bit( T7RUN )
/**
 * Stop timer6
 *
 * @param       -
 * @return      None
 */
#define         timer6_stop()           set_bit( T6STP )
/**
 * Stop timer7
 *
 * @param       -
 * @return      None
 */
#define         timer7_stop()           set_bit( T7STP )

void            timer6_setCnt( unsigned short cnt );
void            timer7_setCnt( unsigned short cnt );

#endif /*_TIMER6_7_H_*/

