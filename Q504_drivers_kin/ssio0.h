/*****************************************************************************
 ssio0.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00
    2014.12.26 ver.1.20  support ES2 or later

******************************************************************************/
/**
 * @file    ssio0.h
 *
 * This file is API definition for 'SSIO(Synchronous Serial Port)'(channel No.0) drivers.
 *
 */

#ifndef _SSIO0_H_
#define _SSIO0_H_

#include "rdwr_reg.h"
#include "ssio_common.h"

/*############################################################################*/
/*#                                  User Define                             #*/
/*############################################################################*/
// This works for LIS3DSH sensor
//#define SSIO_PARAM_MODE   ( SSIO_DIR_MSB          | SSIO_LG_8BIT       | SSIO_CLK_HS32     | SSIO_CKT_HI        | SSIO_NEG_NEG       )

// This works for LIS3DH sensor
#define SSIO_PARAM_MODE   ( SSIO_DIR_MSB          | SSIO_LG_8BIT       | SSIO_CLK_HS32     | SSIO_CKT_HI        | SSIO_NEG_POS       )



/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Initialize SSIO
 *
 * @param[in]   sionmod     Setting value of SIO0MOD register <br>
 *                          Specify the logical sum of the following items.
 * @arg                         Endian                  : SSIO_DIR_LSB, or SSIO_DIR_MSB
 * @arg                         Buffer mode             : SSIO_LG1, or SSIO_LG2
 * @arg                         Clock                   : SSIO_CLK_LS, or SSIO_CLK_LS2, or SSIO_CLK_HS4, or SSIO_CLK_HS8, or SSIO_CLK_HS16, or SSIO_CLK_HS32, or SSIO_CLK_EX
 * @arg                         Clock type              : SSIO_CKT_0, or SSIO_CKT_1
 * @arg                         Logical value           : SSIO_NEG_POS, or SSIO_NEG_NEG
 *
 * @return      None
 *
 * @note        SCK0 Max clock input frequency is 1/4 of SYSCLK or 2MHz at slave mode.          <br>
 *              SCK0 Max clock output frequency is 2MHz if using P02 as SCK0 in master mode.
 */
#define         ssio0_init( sionmod )   write_reg16( SIO0MOD, (unsigned short)(sionmod) )

/**
 * Writing transmission of data (for BYTE access)
 *
 * @param[in]   data    A transmission data <br>
 *                      (Setting value of SIO0BUFL register)
 * @return      None
 */
#define         ssio0_putcByte( data )  write_reg8( SIO0BUFL, (unsigned char)(data) )

/**
 * Writing transmission of data (for WORD access)
 *
 * @param[in]   data    A transmission data <br>
 *                      (Setting value of SIO0BUF register)
 * @return      None
 */
#define         ssio0_putcWord( data )  write_reg16( SIO0BUF, (unsigned short)(data) )

/**
 * Reading reception data (for BYTE access)
 *
 * @param       -
 * @return      A reception data <br>
 *              (A value of SIO0BUFL register)
 */
#define         ssio0_getcByte()        read_reg8( SIO0BUFL )

/**
 * Reading reception data (for WORD access)
 *
 * @param       -
 * @return      A reception data <br>
 *              (A value of SIO0BUF register)
 */
#define         ssio0_getcWord()        read_reg16( SIO0BUF )

/**
 * Stopping SSIO communication
 *
 * @param       -
 * @return      None
 */
#define         ssio0_stop()            clear_bit( S0EN )

/**
 * Checking SSIO busy status
 *
 * @param       -
 * @retval      0   busy status : Ready(able to read/write next operation)
 * @retval      1   busy status : Busy
 */
#define         ssio0_checkReady()      ( (unsigned char)get_bit( S0EN ) )

int             ssio0_start( unsigned short sionmod, void *rxData, void *txData, unsigned int dataSize, cbfSsio_t func );
int             ssio0_continue( void );



/*############################################################################*/
/*#                            User Code
/*############################################################################*/
extern unsigned char Ssio0_flag;        /**< The status of transmit     */

void ssio0_initilize(void);
void smpl_procSio0Int( void );


#endif /*_SSIO0_H_*/

