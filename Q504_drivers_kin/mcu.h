/*****************************************************************************
 mcu.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/

#ifndef _MCU_H_
#define _MCU_H_

#if defined(_ML620Q504)
	#include <ml620504F.h>
#else
#error "Unknown target MCU"
#endif /* _ML620Q504 */

#endif /* _MCU_H */


