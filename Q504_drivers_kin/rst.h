/*****************************************************************************
 rst.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    rst.h
 *
 * This file is API definition for 'Reset generation circuit' drivers.
 *
 */
#ifndef _RST_H_
#define _RST_H_

/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/
/*=== bit field of registers ===*/
/* RSTAT */
#define RSTAT_POR           ( 0x01 )        /**< RSTAT Register POR bit       */
#define RSTAT_WDTR          ( 0x04 )        /**< RSTAT Register WDTR bit      */
#define RSTAT_VLSR          ( 0x08 )        /**< RSTAT Register VLSR bit      */
#define RSTAT_LLDR          ( 0x10 )        /**< RSTAT Register LLDR bit      */

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Get reset causes
 * 
 * Get causes which system-reset is generated.
 *
 * @param       -
 * @return      The value of RSTAT register
 * @see         MCU Users manual 'Reset Function'
 */

#define         rst_getResetCause()             read_reg8( RSTAT )

#endif /*_RST_H_*/

