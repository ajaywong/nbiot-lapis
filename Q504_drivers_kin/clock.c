/*****************************************************************************

******************************************************************************/
/**
 *
 *
 *
 *
 */
#include "mcu.h"
#include "rdwr_reg.h"
#include "clock.h"
#include "wdt.h"

/*############################################################################*/
/*#                              Subroutine                                  #*/
/*############################################################################*/
/**
 * Set HSCLK setting (RC oscillation 16MHz)
 *
 * @param           loscon              Configration of low-speed oscillation.  <br>
 *                                      See also, clk_setHsclk().
 * @return          None
 * @note            Division of OSC frequency is 1/1 OSCLK.                     <br>
 *                  And division of OUTC frequency is 1/1 OSCLK.
 */
void setHsRcClock(unsigned char osclk, unsigned char loscon )
{
    /* Disables HSCLK */
    clk_disHsclk();
    /* Configration of HSCLK */
    clk_setHsclk( (unsigned char)(osclk | CLK_OSCM_RC | osclk), loscon );
    /* Enable HSCLK */
    clk_enaHsclk();

    /* Wait stables high-speed oscillation */
    __asm("nop");
    __asm("nop");

    /* Set SYSCLK */
    clk_setSysclk( CLK_SYSCLK_HSCLK );
}



/**
 * Set Crystal setting (Crystal oscillation 16MHz)
 *
 * @param           loscon              Configration of low-speed oscillation.  <br>
 *                                      See also, clk_setHsclk().
 * @return          None
 * @note            Division of OSC frequency is 1/1 OSCLK.                     <br>
 *                  And division of OUTC frequency is 1/1 OSCLK.
 */
void setHsCrystal16Mhz( unsigned char loscon )
{
    /* Disables HSCLK */
    clk_disHsclk();
    /* Configration of HSCLK */
    clk_setHsclk( (unsigned char)(CLK_SYSC_OSCLK | CLK_OSCM_CRYSTAL | CLK_OUTC_OSCLK), loscon );
    /* Enable HSCLK */
    clk_enaHsclk();

    /* Wait stables high-speed oscillation */
    while( clk_checkHsOscStable() == 0 )
    {
        wdt_clear();
    };

    /* Set SYSCLK */
    clk_setSysclk( CLK_SYSCLK_HSCLK );
}



/**
 * Set LSCLK setting (External crystal oscillation 32KHz)
 *
 * @param           -
 * @return          None
 */
void setLsCrystal32Khz( void )
{
    /* Set SYSCLK */
    clk_setSysclk( CLK_SYSCLK_LSCLK );
    /* Set oscillation mode */
    clk_setLsclk( CLK_XTM_CRYSTAL );    /* crystal/ceramic oscillation */

    /* Wait stables crystal oscillation */
    while( (clk_getClkStatus() & FSTAT_LOSCS) != 0 )
    {
        wdt_clear();
    }
}




















