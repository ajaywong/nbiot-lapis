/*****************************************************************************
 i2c0.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00
    2014.12.26 ver.1.20  support ES2 or later

******************************************************************************/
/**
 * @file    i2c0.h
 *
 * This file is API definition for 'I2C'(channel No.0) drivers.
 *
 */

#ifndef _I2C0_H_
#define _I2C0_H_

#include "rdwr_reg.h"
#include "i2c_common.h"


/*############################################################################*/
/*#                                  User Define                             #*/
/*############################################################################*/
/*  */
#define I2C0_PARAM_MODE     ( I2C_MOD_STD | I2C_DW_ATTENUAT_NONE | I2C_CD_OSCLK )


/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
/**
 * Setting slave address
 *
 * @param[in]   i2cnsa      Slave address                                       <br>
 *                          (Setting value of I2C0SA register)                  <br>
 *                          Bit0(=I20RW) means read/write mode.                 <br>
 * @return      None
 */
#define         i2c0_setSlaveAdrs( i2cnsa ) write_reg8( I2C0SA, i2cnsa )

/**
 * Writing transmission of data
 *
 * @param[in]   data        A transmission data <br>
 *                          (Setting value of I2C0TD register)
 * @return      None
 */
#define         i2c0_putc( data )           write_reg8( I2C0TD, data )

/**
 * Reading reception data
 *
 * @param       -
 * @return      A reception data <br>
 *              (A value of I2C0RD register)
 */
#define         i2c0_getc()                 read_reg8( I2C0RD )

/**
 * Trig I2C
 *
 * Trig the operation of I2C.                                                   <br>
 * I20ST = 0 to 1 : exec StartCondition                                         <br>
 * I20ST = 1 to 1 : restart operation of I2C                                    <br>
 *
 * @param       -
 * @return      None
 */
#define         i2c0_trigStart()            set_bit( I20ST )

/**
 * Trig StopCondition
 *
 * @param       -
 * @return      None
 */
#define         i2c0_trigStopCondition()    set_bit( I20SP )

/**
 * Trig RestartCondition
 *
 * @param       -
 * @return      None
 */
#define         i2c0_trigRestartCondition() set_bit( I20RS )

/**
 * Getting Ack/Nack status
 *
 * @param       -
 * @retval      0   Ack status : Received ack '0'
 * @retval      1   Ack status : Received ack '1'
 */
#define         i2c0_getReceivedAck()       ( (unsigned char)get_bit( I20ACR ) )

/**
 * Checking I2C send-error status
 *
 * @param       -
 * @retval      0   send-error : Normal
 * @retval      1   send-error : Error
 */
#define         i2c0_checkError()           ( (unsigned char)get_bit( I20ER ) )

/**
 * Stop I2C
 *
 * @param       -
 * @return      None
 */
#define         i2c0_stop()                 clear_bit( I20EN )


void            i2c0_init( unsigned short i2cnmod );
int             i2c0_write( unsigned char slaveAddr, unsigned char *addr, unsigned int addrSize, unsigned char *buf, unsigned short size, cbfI2c_t func );
int             i2c0_read( unsigned char slaveAddr, unsigned char *addr, unsigned int addrSize, unsigned char *buf, unsigned short size, cbfI2c_t func );
int             i2c0_continue( void );


/*############################################################################*/
/*#                            User coid
/*############################################################################*/
extern unsigned char i2c0_rdStatus; /**< The status of reading data    */
extern unsigned char i2c0_wrStatus; /**< The status of writing data    */


void smpl_procI2C0Int( void );



#endif /*_I2C0_H_*/
