/*****************************************************************************
 timer_common.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    timer_common.h
 *
 * This file is common definition for 'Timer' drivers.
 *
 */
#ifndef _TIMER_COMMON_H_
#define _TIMER_COMMON_H_

/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/
/*=== bit field of registers ===*/
/* TMnCON */
#define TMnCON_TnCS0        ( 0x01 )       /**< TMnCON Register TnCS0 bit  */
#define TMnCON_TnCS1        ( 0x02 )       /**< TMnCON Register TnCS1 bit  */
#define TMnCON_TnDIV0       ( 0x08 )       /**< TMnCON Register TnDIV0 bit */
#define TMnCON_TnDIV1       ( 0x10 )       /**< TMnCON Register TnDIV1 bit */
#define TMnCON_TnDIV2       ( 0x20 )       /**< TMnCON Register TnDIV2 bit */
#define TMnCON_TmM16        ( 0x40 )       /**< TMnCON Register Tn1M16 bit */
#define TMnCON_TnOST        ( 0x80 )       /**< TMnCON Register TnOST bit  */

/*=== API parameters value ===*/
/* Initial configuration parameters for timerX_init() */
#define TM_CS_LSCLK         (               0 |               0 )                      /**< Clock source   :LSCLK        */
#define TM_CS_OSCLK         (               0 |    TMnCON_TnCS0 )                      /**< Clock source   :OSCLK        */
#define TM_CS_EXTCLK        (    TMnCON_TnCS1 |    TMnCON_TnCS0 )                      /**< Clock source   :EXTCLK       */
#define TM_DIV1             (               0 |               0 |               0 )    /**< Division ratio :Not divide   */
#define TM_DIV2             (               0 |               0 |   TMnCON_TnDIV0 )    /**< Division ratio :1/2          */
#define TM_DIV4             (               0 |   TMnCON_TnDIV1 |               0 )    /**< Division ratio :1/4          */
#define TM_DIV8             (               0 |   TMnCON_TnDIV1 |   TMnCON_TnDIV0 )    /**< Division ratio :1/8          */
#define TM_DIV16            (   TMnCON_TnDIV2 |               0 |               0 )    /**< Division ratio :1/16         */
#define TM_DIV32            (   TMnCON_TnDIV2 |               0 |   TMnCON_TnDIV0 )    /**< Division ratio :1/32         */
#define TM_DIV64            (   TMnCON_TnDIV2 |   TMnCON_TnDIV1 |               0 )    /**< Division ratio :1/64         */
#define TM_MODE_8BIT        (               0 )                                        /**< Timer mode     :8-bit timer  */
#define TM_MODE_16BIT       (    TMnCON_TmM16 )                                        /**< Timer mode     :16-bit timer */
#define TM_OST_REROAD       (               0 )                                        /**< Mode           :auto reload  */
#define TM_OST_ONESHOT      (    TMnCON_TnOST )                                        /**< Mode           :one shout    */

#endif /*_TIMER_COMMON_H_*/

