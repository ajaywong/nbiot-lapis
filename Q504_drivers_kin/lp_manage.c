/*****************************************************************************
 lp_manage.c

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00
    2014.12.26 ver.1.20  support ES2 or later

******************************************************************************/
/**
 * @file    lp_manage.c
 *
 * This module is 'Low Power management' drivers.
 *
 */
#include "mcu.h"
#include "rdwr_reg.h"
#include "lp_manage.h"

/**
 * Setting STOP mode
 *
 * @param       -
 * @return      None
 */
void lp_setStopMode( void )
{
	/* set StopCode Accepter */
	write_reg8( STPACP, 0x50 );
	write_reg8( STPACP, 0xA0 );

	/* The CPU mode is changed to the STOP mode. */
	/* When the mode switch to STOP mode at High speed oscillator is
	 * used, Frequency Status Register (FSTAT) HOSCS bit must be "0". */
	set_bit( STP );
	__asm("nop\n");
	__asm("nop\n");
}

/**
 * Setting HALT mode
 *
 * @param       -
 * @return      None
 */
void lp_setHaltMode( void )
{
	/* The CPU mode is changed to the HALT mode. */
	set_bit( HLT );
	__asm("nop\n");
	__asm("nop\n");
}

/**
 * Setting HALT-H mode
 *
 * @param       -
 * @return      None
 */
void lp_setHaltHMode( void )
{
	/* The CPU mode is changed to the HALT-H mode. */
	/* When the mode switch to HALT-H mode at High speed oscillator 
	 * is used, Frequency Status Register (FSTAT) HOSCS bit must be
	 * "0". */
	set_bit( HLTH );
	__asm("nop\n");
	__asm("nop\n");
}

/**
 * Setting DEEP-HALT mode
 *
 * @param       -
 * @return      None
 */
void lp_setDeepHaltMode( void )
{
	/* The CPU mode is changed to the DEEP-HALT mode. */
	/* When High speed crystal/ceramic oscillator or Low speed 
	 * crystal/ceramic oscillator is used and the mode switch to 
	 * DEEP - HALT mode, Frequency Status Register (FSTAT) HOSCS bit
	 * must be "0". */
	set_bit( DHLT );
	__asm("nop\n");
	__asm("nop\n");
}

