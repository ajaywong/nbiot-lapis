/*****************************************************************************
 lp_manage.h

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00

******************************************************************************/
/**
 * @file    lp_manage.h
 *
 * This file is API definition for 'Low Power management' drivers.
 *
 */
#ifndef _LP_MANAGE_H_
#define _LP_MANAGE_H_

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
void            lp_setStopMode( void );
void            lp_setHaltMode( void );
void            lp_setHaltHMode( void );
void            lp_setDeepHaltMode( void );

#endif /*_LP_MANAGE_H_*/

