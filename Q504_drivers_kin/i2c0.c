/*****************************************************************************
 i2c0.c

 Copyright (C) 2014 LAPIS Semiconductor Co., LTD.
 All rights reserved.

 This software is provided "as is" and any expressed or implied
  warranties, including, but not limited to, the implied warranties of
  merchantability and fitness for a particular purpose are disclaimed.
 LAPIS SEMICONDUCTOR shall not be liable for any direct, indirect,
 consequential or incidental damages arising from using or modifying
 this software.
 You (customer) can modify and use this software in whole or part on
 your own responsibility, only for the purpose of developing the software
 for use with microcontroller manufactured by LAPIS SEMICONDUCTOR.

 History
    2014.07.03 ver 1.00
    2014.12.26 ver.1.20  support ES2 or later
    2015.03.09 ver.1.30  bug fix

******************************************************************************/
/**
 * @file    i2c0.c
 *
 * This module is 'I2C' drivers(channel No.0).
 *
 */
#include "mcu.h"
#include "rdwr_reg.h"
#include "irq.h"

#include "i2c0.h"
#include "i2c_common.h" //kin

/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/

#define FLAG_SET  ( 1 )
#define FLAG_CLR  ( 0 )

/*--- Operation status ---*/
#define I2C_COMMUNICATION_END       ( 0 )           /**< Operation status : communication completion              */
#define I2C_TRANS_SLAVE_ADDRESS     ( 1 )           /**< Operation status : slave address send mode               */
#define I2C_TRANS_ADDRESS           ( 2 )           /**< Operation status : write/read address send mode          */
#define I2C_WRITE_DATA              ( 3 )           /**< Operation status : data write mode                       */
#define I2C_READ_DATA               ( 4 )           /**< Operation status : data read mode                        */

/**
 * Control parameter for I2C
 */
typedef struct {
    unsigned char   mode;                           /**< transfer mode(0:write, 1:read) after send slave address  */
    unsigned char   *addr;                          /**< pointer to area where the send/receive address is stored */
    unsigned int    addrSize;                       /**< size of send address                                     */
    unsigned char   *data;                          /**< pointer to area where the write/read data is stored      */
    unsigned int    dataSize;                       /**< size of write/read data                                  */
    unsigned int    cnt;                            /**< size of data which is writing/reading                    */
    cbfI2c_t        callBack;                       /**< callback function                                        */
    unsigned char   errStat;                        /**< error status                                             */
    unsigned char   status;                         /**< writing/reading operation status                         */
} I2cCtrlParam_t;



/*############################################################################*/
/*#                              User  Variable                              #*/
/*############################################################################*/
unsigned char i2c0_rdStatus; /**< The status of reading data    */
unsigned char i2c0_wrStatus; /**< The status of writing data    */

/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/
static I2cCtrlParam_t       s_ctrlParam;            /**< control parameter for I2C    */

/*############################################################################*/
/*#                               Prototype                                  #*/
/*############################################################################*/

/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/

/**
 * Initialize I2C
 *
 * @param[in]   i2cnmod     Configuration of I2C mode , and so on               <br>
 *                          (Setting value of I2C0MOD register)                 <br>
 *                          Specify the logical sum of the following items.
 * @arg                         Transmission mode     : I2C_MOD_STD, or I2C_MOD_FST
 * @arg                         Attenuation rate      : I2C_DW_ATTENUAT_NONE, or I2C_DW_ATTENUAT_10, or I2C_DW_ATTENUAT_20, or I2C_DW_ATTENUAT_30
 * @arg                         Clock division        : I2C_CD_OSCLK, or I2C_CD_OSCLK2, or I2C_CD_OSCLK4
 *
 * @return      None
 *
 * @note
 *              This is set so that the communication speed becomes 100 kbps/400 kbps when the operating frequency of I2C is 4MHz. <br>
 *              Set the operating frequency of I2C in I2nCD0 and I2nCD1.
 */
void i2c0_init( unsigned short i2cnmod ) {

    /*** I2C-0  Sensor Initialize*/
    /* I2C0 SDA */
    // P40 - SDA0
    //write_bit( P40D, 1 );
    clear_bit( P40DIR );			/* Output */
    clear_bit( P40C0 );				/* N-ch open drain */
    set_bit( P40C1 );
    set_bit( P40MD0 );				/* I2C mode */
    clear_bit( P40MD1 );

    /* I2C0 SCL */
    // P41 - SCL0
    //write_bit( P41D, 1 );
    clear_bit( P41DIR );			/* Output */
    clear_bit( P41C0 );				/* N-ch open drain */
    set_bit( P41C1 );
    set_bit( P41MD0 );				/* I2C mode */
    clear_bit( P41MD1 );


	/*=== Register setting ===*/
	/*--- i2c communication action stop ---*/
	i2c0_stop();
	/*--- Communication setting ---*/
	write_reg16( I2C0MOD, (unsigned short)I2CnMOD_I2nEN | i2cnmod );

    /* I2C0 Enable Interrupt */
    irq_i2c0_clearIRQ();
    irq_i2c0_ena();

}

/**
 * Start to send writing data
 *
 * @param[in]   slaveAddr   Slave address                                       <br>
 *                          (Setting I2C0SA register value)                     <br>
 *                          Bit0(=I20RW) means read/write mode.
 *
 * @param[in]   *addr       Pointer to area where the adress data is stored
 *
 * @param[in]   addrSize    Size of data to adress (unit is byte)               <br>
 *                          Exclude the size of slave address.                  <br>
 *                          If you not need to transmit address data, Set this parameter '0'.
 *
 * @param[in]   *buf        Pointer to area where the write data is stored
 *
 * @param[in]   size        Size of data to write (unit is byte)                <br>
 *                          Exclude the size of slave address.
 *
 * @param[in]   func        Pointer to callback function
 *
 * @retval      I2C_R_OK(=0)               : I2C communication is started.      (success)
 */
int i2c0_write( unsigned char slaveAddr, unsigned char *addr, unsigned int addrSize, unsigned char *buf, unsigned short size, cbfI2c_t func )
{
	/*=== Transmission of a message system order parameter setting. ===*/
	s_ctrlParam.mode       = 0;    /* write */
	s_ctrlParam.addr       = addr;
	s_ctrlParam.addrSize   = addrSize;
	s_ctrlParam.data       = buf;
	s_ctrlParam.dataSize   = size;
	s_ctrlParam.cnt        = 0;
	s_ctrlParam.callBack   = func;
	s_ctrlParam.errStat    = 0;
	s_ctrlParam.status     = 0;

	/*=== transmit a adddress, and it is worked to start. ===*/
	s_ctrlParam.status = I2C_TRANS_SLAVE_ADDRESS;

	slaveAddr &= ~I2CnSA_I2nRW;
	i2c0_setSlaveAdrs( slaveAddr | I2C_RW_WRITE );  /* Data transmit mode (data write) */
	i2c0_trigStart();                               /* Start condition */
	return ( I2C_R_OK );
}

/**
 * Start to receive reading data
 *
 * @param[in]   slaveAddr   Slave address                                       <br>
 *                          (Setting I2C0SA register value)                     <br>
 *                          Bit0(=I20RW) means read/write mode.
 *
 * @param[in]   *addr       Pointer to area where the adress data is stored
 *
 * @param[in]   addrSize    Size of data to adress (unit is byte)               <br>
 *                          Exclude the size of slave address.                  <br>
 *                          If you not need to transmit address data, Set this parameter '0'.
 *
 * @param[in]   *buf        Pointer to area where the receive data is stored
 *
 * @param[in]   size        Size of data to read (unit is byte)                 <br>
 *                          Exclude the size of slave address.
 *
 * @param[in]   func        Pointer to callback function
 *
 * @retval      I2C_R_OK(=0)               : I2C communication is started.      (success)
 */
int i2c0_read( unsigned char slaveAddr, unsigned char *addr, unsigned int addrSize, unsigned char *buf, unsigned short size, cbfI2c_t func )
{
	/*=== Reception mode setting ===*/
	/*=== Transmission of a message system order parameter setting. ===*/
	s_ctrlParam.mode        = 1;    /* read */
	s_ctrlParam.addr        = addr;
	s_ctrlParam.addrSize    = addrSize;
	s_ctrlParam.data        = buf;
	s_ctrlParam.dataSize    = size;
	s_ctrlParam.cnt         = 0;
	s_ctrlParam.callBack    = func;
	s_ctrlParam.errStat     = 0;
	s_ctrlParam.status      = 0;

	/*=== I receive it, and it is worked to start. ===*/
	s_ctrlParam.status = I2C_TRANS_SLAVE_ADDRESS;

	clear_bit( I20ACT );                                /* Reset acknowledgment data */
	slaveAddr &= ~I2CnSA_I2nRW;
	if( addrSize == 0 ){
		i2c0_setSlaveAdrs( slaveAddr | I2C_RW_READ );   /* Data receive mode (data read) */
	}
	else{
		i2c0_setSlaveAdrs( slaveAddr | I2C_RW_WRITE );  /* Data transmit mode (data write) */
	}
	i2c0_trigStart();                                   /* Start condition */
	return ( I2C_R_OK );
}

/**
 * Continue to communicate of writing/reading data
 *
 * @param       -
 *
 * @retval      I2C_R_TRANS_FIN(=1)        : Communication of writing/reading data is finished  (success/failure) <br>
 *                                           The callback function parameter has the causes of this failure .
 * @retval      I2C_R_TRANS_CONT_OK(=0)    : Communication of writing/reading data is continued (success)
 */

int i2c0_continue( void )
{
	switch( s_ctrlParam.status ){
		case I2C_TRANS_SLAVE_ADDRESS:
			/* transmission error check */
			if( i2c0_checkError() == 1 ){
				s_ctrlParam.status = I2C_COMMUNICATION_END;
				s_ctrlParam.errStat = I2C_ERR_SEND_ERR;
				write_reg16( I2C0CON, 0x0000 ); /* Stop operation of I2C */
				if( s_ctrlParam.callBack != (void *)0 ){
					s_ctrlParam.callBack( s_ctrlParam.cnt, s_ctrlParam.errStat );
				}
				return ( I2C_R_TRANS_FIN );
			}
			/* NACK reception check */
			else if( i2c0_getReceivedAck() == 1 ){
				s_ctrlParam.status = I2C_COMMUNICATION_END;
				s_ctrlParam.errStat = I2C_ERR_ACR;
				i2c0_trigStopCondition();       /* Stop condition */
			}
			/* Slave address transmission success */
			else{
				if( s_ctrlParam.addrSize > 0 ){
					s_ctrlParam.status = I2C_TRANS_ADDRESS;
					i2c0_putc( (unsigned char)*s_ctrlParam.addr );
					s_ctrlParam.addr++;
					s_ctrlParam.addrSize--;
					i2c0_trigStart();               /* Next operation start */
				}
				else{
					if( s_ctrlParam.mode != 0 ){
						s_ctrlParam.status = I2C_READ_DATA;
						if( ( s_ctrlParam.dataSize - 1) == s_ctrlParam.cnt ){
							set_bit( I20ACT );
						}
						i2c0_trigStart();           /* Next operation start */
					}
					else{
						s_ctrlParam.status = I2C_WRITE_DATA;
						i2c0_putc( (unsigned char)*s_ctrlParam.data );
						s_ctrlParam.data++;
						s_ctrlParam.cnt++;
						i2c0_trigStart();           /* Next operation start */
					}
				}
			}
			return(I2C_R_TRANS_CONT_OK);

		case I2C_TRANS_ADDRESS:
			if( i2c0_getReceivedAck() == 1 ){
				s_ctrlParam.status = I2C_COMMUNICATION_END;
				s_ctrlParam.errStat = I2C_ERR_ACR;
				i2c0_trigStopCondition();       /* Stop condition */
			}
			else{
				if( s_ctrlParam.addrSize > 0 ){
					i2c0_putc( (unsigned char)*s_ctrlParam.addr );
					s_ctrlParam.addr++;
					s_ctrlParam.addrSize--;
					i2c0_trigStart();           /* Next operation start */
				}
				/* Address transmission success */
				else{
					if( s_ctrlParam.dataSize == 0 ){
						s_ctrlParam.status = I2C_COMMUNICATION_END;
						i2c0_trigStopCondition();       /* Stop condition */
					}
					else{
						if( s_ctrlParam.mode != 0 ){    /* Read mode */
							s_ctrlParam.status = I2C_TRANS_SLAVE_ADDRESS;
							set_bit( I20RW );           /* Read mode */
							i2c0_trigRestartCondition();/* Restart condition */
						}
						else{/* send mode */
							s_ctrlParam.status = I2C_WRITE_DATA;
							i2c0_putc( (unsigned char)*s_ctrlParam.data );
							s_ctrlParam.data++;
							s_ctrlParam.cnt++;
							i2c0_trigStart();           /* Next operation start */
						}
					}
				}
			}
			return ( I2C_R_TRANS_CONT_OK );

		case I2C_WRITE_DATA:
			/* NACK reception check */
			if( i2c0_getReceivedAck() == 1 ){
				s_ctrlParam.status = I2C_COMMUNICATION_END;
				s_ctrlParam.errStat = I2C_ERR_ACR;
				i2c0_trigStopCondition();       /* Stop condition */
			}
			/* Check to communication finish */
			else if( s_ctrlParam.dataSize > s_ctrlParam.cnt ){
				i2c0_putc( (unsigned char)*s_ctrlParam.data );
				s_ctrlParam.data++;
				s_ctrlParam.cnt++;
				i2c0_trigStart();               /* Next operation start */
			}
			else{
				s_ctrlParam.status = (unsigned char)I2C_COMMUNICATION_END;
				/*--- Stop condition send ? ---*/
				i2c0_trigStopCondition();       /* Stop condition */
			}
			return ( I2C_R_TRANS_CONT_OK );

		case I2C_READ_DATA:
			*s_ctrlParam.data = i2c0_getc();
			s_ctrlParam.data++;
			s_ctrlParam.cnt++;
			/* Check to communication finish */
			if( s_ctrlParam.dataSize > s_ctrlParam.cnt ){
				if( ( s_ctrlParam.dataSize - 1) == s_ctrlParam.cnt ){
					set_bit( I20ACT );
				}
				i2c0_trigStart();               /* Next operation start */
			}
			else{
				s_ctrlParam.status = (unsigned char)I2C_COMMUNICATION_END;
				i2c0_trigStopCondition();       /* Stop condition */
			}
			return ( I2C_R_TRANS_CONT_OK );

		case I2C_COMMUNICATION_END:
		default:
			/* End of communication */
			if( s_ctrlParam.callBack != (void *)0 ){
				s_ctrlParam.callBack( s_ctrlParam.cnt, s_ctrlParam.errStat );
			}
			break;
	}
	return ( I2C_R_TRANS_FIN );
}



/*############################################################################*/
/*#                                User Code                                 #*/
/*############################################################################*/
/**
 * i2c interrupt routine
 *
 * @param           -
 * @return          None
 */
void smpl_procI2C0Int( void )
{
    signed int  state;
    signed char mode  = I2C_INIT_STATE;

    state = i2c0_continue();
    if (state == I2C_R_TRANS_FIN)
    {
        mode = get_bit( I20RW );
        if(mode == I2C_RW_READ)
        {
            i2c0_rdStatus = I2C_READ_END;
        }
        else
        {
             i2c0_wrStatus = I2C_WRITE_END;
        }
    }
}

