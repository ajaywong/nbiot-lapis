/*****************************************************************************
 Driver_Sigfox.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _DRIVER_SIGFOX_H_
#define _DRIVER_SIGFOX_H_

#include "User_System.h"


/* Parameters for UARTF control */
#define UARTF_PARAM_MODE0   ( UARTF_LG_8BIT     | UARTF_STP_1BIT | UARTF_PT_NON | UARTF_BC_DIS | UARTF_DLAB_RBR_THR )
#define UARTF_PARAM_MODE1   ( UARTF_RFR_KEEP    | UARTF_TFR_KEEP | UARTF_FTL_1BYTE )
#define UARTF_PARAM_MODE    ( UARTF_PARAM_MODE0 | UARTF_PARAM_MODE1 )
#define UARTF_PARAM_CLOCK   ( 0x000D )
//#define UARTF_PARAM_DLR     ( 0x0018 )  // 0x0060 // 16Mhz 9600,  0x0018 // 4Mhz  9600
#define UARTF_PARAM_DLR     ( 0x0002 )  // 0x0008 // 16Mhz 115200,  0x002 // 4Mhz  115200

#define UARTF_MASK_INT          ( UAFnIIR_UFnIRID2 | UAFnIIR_UFnIRID1 | UAFnIIR_UFnIRID0 ) /**< Mask for checking interrupt status */
#define UARTF_MASK_INT_RECEIVE  ( UARTF_IRID_READ_REQ & UARTF_IRID_CHAR_TIMEOUT & UARTF_IRID_DATA_ERR ) /**< Mask for the receive interrupt */
#define UARTF_MASK_PENDING      ( UAFnLSR_UFnDR  ) /**< Mask for checking interrupt pending */
#define UARTF_RW_MAX            ( 50 )             /**< The size of communication */


/** Wakeup to Sigfox module: Active-LOW = Wakeup */
#define SIGFOX_WAKEUP_OFF	P56D = 1
#define SIGFOX_WAKEUP_ON	P56D = 0

/**  Reset to Sigfox module: Active-LOW = RESET */
#define SIGFOX_RESET_OFF	P57D = 1
#define SIGFOX_RESET_ON		P57D = 0


#define NH3_LED_OFF	      P11D = 0
#define NH3_LED_ON		    P11D = 1

/**  Power to Sigfox module */
#define SIGFOX_PWR_ON		P11D = 1
#define SIGFOX_PWR_OFF		P11D = 0


/** Sleep Mode option for for the firmware */
#define FIRMWARE_MODULE_SLEEP_MODE SIGFOX_SLEEP


#define SIGFOX_SOFT_RESET       0
#define SIGFOX_SLEEP            1
#define SIGFOX_DEEP_SLEEP       2
#define SIGFOX_STANDBY	        3


#define SIGFOX_REG_300          300
#define SIGFOX_REG_302          302
#define SIGFOX_REG_400          400
#define SIGFOX_REG_410          410

#define INFO_DEVICE_ID          10
#define INFO_PAC                11


extern unsigned char replyOption;
/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/
//void Sigfox_Reset(void);
void Sigfox_HW_Reset(void);
void Sigfox_Wakeup(void);
void Sigfox_PowerOn(void);
void Sigfox_PowerOff(void);

/*  */
void smpl_procUartfInt( void );

/*  */
unsigned char Sigfox_config(void);
unsigned char Sigfox_ATCloseSock(void);

unsigned char Sigfox_SetCarrier(void);
unsigned char Sigfox_CarrierMode(void);
unsigned char Sigfox_SetPowerMode(unsigned char uint);
unsigned char Sigfox_GetInfo(unsigned char uint);
unsigned char Sigfox_GetInfoVerbose(unsigned char uint);

unsigned char Sigfox_SendFrameInit(void);
unsigned char Sigfox_SendFrameCheck(void);
unsigned char Sigfox_SetRegister(unsigned short reg, unsigned short uint);

unsigned char Sigfox_GetRxFreq(void);
unsigned char Sigfox_GetRxFreqVerbose(void);

unsigned char Sigfox_Parser_Packet(void);
unsigned char Sigfox_OOB(void);
unsigned char Sigfox_Set_OOB(unsigned short oob_period);
unsigned char Sigfox_FCC_Channel_set(unsigned char fcc_enable);

unsigned char Sigfox_GetRegister(unsigned short reg);
unsigned char Sigfox_GetRegisterVerbose(unsigned short reg);

unsigned char Sigfox_ATClose(void); 

#endif /*_SIGFOX_H_*/



