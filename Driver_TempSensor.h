/*****************************************************************************
 Driver_TempSensor.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _DRIVER_H_
#define _DRIVER_H_

/* MCU */
#include "mcu.h"
#include "rdwr_reg.h"

#include "User_Generic.h"

/*############################################################################*/
/*#                               I2C Related                                #*/
/*############################################################################*/

// if ADDR = HIGH
//#define SHT3x_I2C_SLAVE_ADDRESS_WRITE (0x45<<1) //=0x8A
//#define SHT3x_I2C_SLAVE_ADDRESS_READ (0x45<<1 | 0x1)  //=0x8B

// if ADDR = LOW
#define SHT3x_I2C_SLAVE_ADDRESS_WRITE (0x44<<1) //=0x88
#define SHT3x_I2C_SLAVE_ADDRESS_READ (0x44<<1 | 0x1)  //=0x89



// With Stretching
#define SHT3x_START_MEASURE_S_MSB				0x2C
#define SHT3x_START_MEASURE_SH_LSB				0x06
#define SHT3x_START_MEASURE_SM_LSB				0x0D
#define SHT3x_START_MEASURE_SL_LSB				0x10

// No Stretching
#define SHT3x_START_MEASURE_NS_MSB				0x24
#define SHT3x_START_MEASURE_NSH_LSB				0x00
#define SHT3x_START_MEASURE_NSM_LSB				0x0B
#define SHT3x_START_MEASURE_NSL_LSB				0x16

#define SHT3x_READ_STATUS_MSB       0xF3
#define SHT3x_READ_STATUS_LSB       0x2D

#define SHT3x_CLEAR_STATUS_MSB      0x30
#define SHT3x_CLEAR_STATUS_LSB      0x41

#define SHT3x_SOFT_RESET_MSB        0x30
#define SHT3x_SOFT_RESET_LSB        0xA2

#define SHT3x_HEATER_EN_MSB         0x30
#define SHT3x_HEATER_EN_LSB         0x6D

#define SHT3x_HEATER_DIS_MSB 	    0x30
#define SHT3x_HEATER_DIS_LSB        0x66

// Power to temperature sensor: Active-HIGH = POWER ON
#define TEMP_PWR_ON			P30D = 1
#define TEMP_PWR_OFF		P30D = 0

/*
extern uint16_t SHT3x_temperature;
extern uint16_t SHT3x_humidity;

extern uint8_t SHT3x_temperature_crc;
extern uint8_t SHT3x_humidity_crc;
*/

extern float SHT3x_temperature_C_ft;
extern float SHT3x_temperature_F_ft;
extern float SHT3x_humidity_ft;

extern uint16_t SHT3x_chip_status;
extern uint8_t SHT3x_chip_status_crc;



/*#                                Variable                                  #*/
//extern uint8_t SHT3x_s_Status;   /**< The status of i2c             */
//extern uint8_t SHT3x_dataLen;    /**< number of byte sended/recieved       */

/*#                               Function                                 #*/
uint8_t SHT3x_write_multi(uint8_t address, uint8_t *pdata, uint8_t count);
uint8_t SHT3x_read_multi(uint8_t address, uint8_t index, uint8_t *pdata, uint8_t count);
uint8_t SHT3x_SendIndex(uint8_t address, uint8_t index);

static uint8_t SHT3x_writeReg(uint8_t reg, uint8_t value);
static uint8_t SHT3x_writeReg16Bit(uint8_t reg, uint16_t value);
static uint8_t SHT3x_writeReg32Bit(uint8_t reg, uint32_t value);
static uint8_t SHT3x_readReg(uint8_t reg);
static uint16_t SHT3x_readReg16Bit(uint8_t reg);
static uint32_t SHT3x_readReg32Bit(uint8_t reg);
static uint8_t SHT3x_writeMulti(uint8_t reg, uint8_t *src, uint8_t count);
static uint8_t SHT3x_readMulti(uint8_t reg, uint8_t *dst, uint8_t count);
uint8_t SHT3x_start_measure(void);
uint8_t SHT3x_get_measure_data(uint16_t *temperature, uint16_t *humidity, uint8_t *temp_crc, uint8_t *hum_crc);
uint8_t SHT3x_get_crc8(uint8_t *data, int len);
float SHT3x_get_temperature_C(uint16_t rawValue);
float SHT3x_get_temperature_F(uint16_t rawValue);
float SHT3x_get_humidity(uint16_t rawValue);
uint8_t SHT3x_read_status(uint16_t *chip_status, uint8_t *chip_status_crc);
uint8_t SHT3x_clear_status(void);
uint8_t SHT3x_soft_reset(void);
uint8_t SHT3x_heater_enable(void);
uint8_t SHT3x_heater_disable(void);



void TemperatureSensor_Initialize(void);

uint8_t Run_TempSensorCtrl(void);
void Power_On_TempSensorCtrl(void);
void Power_Off_TempSensorCtrl(void);



#endif
