//******************************************************************************
//   SWI2C_Master_0.h
//******************************************************************************


#ifndef __SWI2C_MASTER_0_H
#define __SWI2C_MASTER_0_H



/* FG IC  */

// Need to move to P33 and P46 later for software I2C

#define SWI2C_SDA_PIN       P04D
#define SWI2C_SDA_DIR       P04DIR
#define SWI2C_SDA_C0        P04C0
#define SWI2C_SDA_C1        P04C1
#define SWI2C_SDA_MD0       P04MD0
#define SWI2C_SDA_MD1       P04MD1

#define SWI2C_SCL_PIN       P05D
#define SWI2C_SCL_DIR       P05DIR
#define SWI2C_SCL_C0        P05C0
#define SWI2C_SCL_C1        P05C1
#define SWI2C_SCL_MD0       P05MD0
#define SWI2C_SCL_MD1       P05MD1


/*
#define SWI2C_SDA_PIN       P40D
#define SWI2C_SDA_DIR       P40DIR
#define SWI2C_SDA_C0        P40C0
#define SWI2C_SDA_C1        P40C1
#define SWI2C_SDA_MD0       P40MD0
#define SWI2C_SDA_MD1       P40MD1

#define SWI2C_SCL_PIN       P41D
#define SWI2C_SCL_DIR       P41DIR
#define SWI2C_SCL_C0        P41C0
#define SWI2C_SCL_C1        P41C1
#define SWI2C_SCL_MD0       P41MD0
#define SWI2C_SCL_MD1       P41MD1
*/

/*
#define SWI2C_SDA_PIN       P34D
#define SWI2C_SDA_DIR       P34DIR
#define SWI2C_SDA_C0        P34C0
#define SWI2C_SDA_C1        P34C1
#define SWI2C_SDA_MD0       P34MD0
#define SWI2C_SDA_MD1       P34MD1

#define SWI2C_SCL_PIN       P35D
#define SWI2C_SCL_DIR       P35DIR
#define SWI2C_SCL_C0        P35C0
#define SWI2C_SCL_C1        P35C1
#define SWI2C_SCL_MD0       P35MD0
#define SWI2C_SCL_MD1       P35MD1

*/


#define GPIODELAYCYCLES     2

#define I2CDELAY 			SWI2CMST_delay()    // Macro for GPIO change delay

#define SDA_H	            set_bit( SWI2C_SDA_DIR );
#define SDA_L		        {clear_bit(SWI2C_SDA_PIN);clear_bit( SWI2C_SDA_DIR );}

#define SCL_H		        set_bit( SWI2C_SCL_DIR );
#define SCL_L    		    {clear_bit(SWI2C_SCL_PIN);clear_bit( SWI2C_SCL_DIR );}

#define NACK    0
#define ACK     1
#define BIT7    0x80
#define BIT0    0x01


//0xFFFF ~130ms @8MHz, 65Ms @16Mhz
//0xFF //~512us 5000~ 10ms @8MHz
#define I2C_TIMEOUT     ((uint32_t)0xF4240) //~200ms @8MHz

//#define I2C_TIMEOUT     ((uint32_t)0x1E8480) //~200ms @16MHz




void swI2cMst0_init(void);
uint8_t swI2cMst0_Read(uint8_t SlaveAddress, uint8_t reg, uint8_t* data, uint16_t len);
uint8_t swI2cMst0_Write(uint8_t SlaveAddress, uint8_t reg, uint8_t* data, uint16_t len);

#endif
