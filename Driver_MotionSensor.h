/*****************************************************************************
 Driver_MotionSensor.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */

#ifndef _DRIVER_MOTIONSENSOR_H_
#define _DRIVER_MOTIONSENSOR_H_

extern unsigned char Bin_CoverClosed;

uint8_t Run_MotionSensorCtrl(void);


#endif
