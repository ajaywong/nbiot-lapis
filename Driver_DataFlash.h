/*****************************************************************************
 Driver_DataFlash.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _DRIVER_DATAFLASH_H_
#define _DRIVER_DATAFLASH_H_

void DataFlash_BlockErase(void);
//void DataFlash_GetBlankAddress(unsigned int* data);
//void DataFlash_Write(unsigned int data);
void UserDataFlash_Write(uint16_t *data,uint16_t size);
void UserDataFlash_Read(uint16_t *data, uint16_t size);

#endif // _DRIVER_DATAFLASH_H_










