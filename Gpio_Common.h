/*****************************************************************************
 Gpio_Common.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */

#ifndef _GPIO_COMMON_H_
#define _GPIO_COMMON_H_

#include "mcu.h"
#include "rdwr_reg.h"


/*############################################################################*/
#define _D(pin) pin##D
#define _DIR(pin) pin##DIR
#define _C0(pin) pin##C0
#define _C1(pin) pin##C1
#define _MD0(pin) pin##MD0
#define _MD1(pin) pin##MD1
#define _EXIn_PORT_SEL(pin) EXIn_PORT_SEL_##pin

#define INT_PORT_SEL(pin) _EXIn_PORT_SEL(pin)


#define SET_BIT(pin)    set_bit(_D(pin))
#define CLEAR_BIT(pin)  clear_bit(_D(pin))
#define GET_BIT(pin)    get_bit(_D(pin))
#define WRITE_BIT(pin, defval)  write_bit(_D(pin), defval)

/*############################################################################*/
#define GPIO_OUTPUT_HI_Z(pin)	        clear_bit( _DIR(pin) ); /* Output */\
										clear_bit( _C0(pin) );  /* N-channel open drain output */\
										clear_bit( _C1(pin) )

/*############################################################################*/
#define GPIO_OUTPUT_P_OD(pin, defval)  WRITE_BIT(pin, defval);\
										clear_bit( _DIR(pin) ); /* Output */\
										set_bit( _C0(pin) );  /* N-channel open drain output */\
										clear_bit( _C1(pin) )

/*############################################################################*/
#define GPIO_OUTPUT_N_OD(pin, defval) 	WRITE_BIT(pin, defval);\
										clear_bit( _DIR(pin) ); /* Output */\
										clear_bit( _C0(pin) );  /* N-channel open drain output */\
										set_bit( _C1(pin) )

/*############################################################################*/
#define GPIO_OUTPUT_COMS(pin, defval)   WRITE_BIT(pin, defval);\
                                        clear_bit( _DIR(pin) ); /* Output */\
										set_bit( _C0(pin) );  /* N-channel open drain output */\
										set_bit( _C1(pin) )

/*############################################################################*/
#define GPIO_INPUT_HI_Z(pin)	set_bit( _DIR(pin) ); /* Input */\
								clear_bit( _C0(pin) );  /* N-channel open drain output */\
								clear_bit( _C1(pin) )

/*############################################################################*/
#define GPIO_INPUT_PUTUP(pin)   set_bit( _DIR(pin) ); /*  */\
								clear_bit( _C0(pin) );  /* N-channel open drain output */\
								set_bit( _C1(pin) )

/*############################################################################*/
#define GPIO_INPUT_PUTDOWN(pin) set_bit( _DIR(pin) );\
								set_bit( _C0(pin) );\
								clear_bit( _C1(pin) );

/*############################################################################*/
/* General-purpose input/output mode */
#define GPIO_INPUT_OUTPUT_MODE(pin)	clear_bit( _MD0(pin) );clear_bit( _MD1(pin) )


/*############################################################################*/
/* set UART port (RXD0) */
#define GPIO_UART0_RXD(pin)	set_bit( _DIR(pin) );  /* Input */\
							clear_bit( _C0(pin) ); /*  */\
							set_bit( _C1(pin) );\
							set_bit( _MD0(pin) );  /* UART RXD function */\
							set_bit( _MD1(pin) )\

/* set UART port (TXD0) */
#define GPIO_UART0_TXD(pin)	clear_bit( _DIR(pin) ); /* Output */\
							set_bit( _C0(pin) );    /* CMOS */\
							set_bit( _C1(pin) );\
							set_bit( _MD0(pin) );   /* UART TXD function */\
							set_bit( _MD1(pin) )\


/*############################################################################*/
/* set UARTF port (RXDF0) */
#define GPIO_UARTF0_RXD(pin)	set_bit( _DIR(pin) );  /* Input */\
								clear_bit( _C0(pin) ); /* Hi Impedance */\
								clear_bit( _C1(pin) );\
								set_bit( _MD0(pin) );  /* UART TXD function */\
								set_bit( _MD1(pin) )

/* set UARTF port (TXD) */
#define GPIO_UARTF0_TXD(pin)	clear_bit( _DIR(pin) ); /* Output */\
								set_bit( _C0(pin) );	   /* CMOS */\
								set_bit( _C1(pin) );\
								set_bit( _MD0(pin) );   /* UART RXD function */\
								set_bit( _MD1(pin) )\


/*############################################################################*/
/* port I2C set (SDA0) */
#define GPIO_I2C_SDA(pin)	set_bit(_MD0(pin));\
							clear_bit(_MD1(pin));\
							clear_bit(_DIR(pin));\
							clear_bit(_C0(pin));\
							set_bit(_C1(pin))

/* port I2c set (SCL0) */
#define GPIO_I2C_SCL(pin)	set_bit(_MD0(pin));\
							clear_bit(_MD1(pin));\
							clear_bit(_DIR(pin));\
							clear_bit(_C0(pin));\
							set_bit(_C1(pin))


/*############################################################################*/
/* set SSIO port(Tx mode) */
/* SOUT0 */
#define GPIO_SSIO_MASTER_SOUT(pin)	clear_bit( _DIR(pin) );\
									set_bit( _C0(pin));\
									set_bit( _C1(pin));\
									clear_bit( _MD0(pin) );\
									set_bit( _MD1(pin) )
	/* SIN0  */
#define GPIO_SSIO_MASTER_SIN(pin)	set_bit( _DIR(pin) );\
									set_bit( _C0(pin));\
									set_bit( _C1(pin));\
									clear_bit( _MD0(pin) );\
									set_bit( _MD1(pin) )
	/* SCK0  */
#define GPIO_SSIO_MASTER_SCK(pin,defval)	write_bit( _D(pin), defval);\
                                    clear_bit( _DIR(pin) );\
									set_bit( _C0(pin));\
									set_bit( _C1(pin));\
									clear_bit( _MD0(pin) );\
									set_bit( _MD1(pin) )
	/* CS    */
#define GPIO_SSIO_MASTER_CS(pin, defval)	write_bit( _D(pin), defval);\
											clear_bit( _DIR(pin) );\
											set_bit( _C0(pin));\
											set_bit( _C1(pin));\
											clear_bit( _MD0(pin) );\
											clear_bit( _MD1(pin) )

/*############################################################################*/
/* set SSIOF port(Rx Mode) */
/* SOUTF0 */
#define GPIO_SSIOF_SLAVE_SOUT(pin)	clear_bit( _DIR(pin) );\
									set_bit( _C0(pin));\
									set_bit( _C1(pin));\
									clear_bit( _MD0(pin) );\
									set_bit( _MD1(pin) )
/* SINF0  */
#define GPIO_SSIOF_SLAVE_SIN(pin)	set_bit( _DIR(pin) );\
									set_bit( _C0(pin));\
									set_bit( _C1(pin));\
									clear_bit( _MD0(pin) );\
									set_bit( _MD1(pin) )
	/* SCKF0  */
#define GPIO_SSIOF_SLAVE_SCK(pin)	set_bit( _DIR(pin) );\
									set_bit( _C0(pin));\
									set_bit( _C1(pin));\
									clear_bit( _MD0(pin) );\
									set_bit( _MD1(pin) )
	/* SSF0   */
#define GPIO_SSIOF_SLAVE_SS(pin)	set_bit( _DIR(pin) );\
									set_bit( _C0(pin));\
									set_bit( _C1(pin));\
									clear_bit( _MD0(pin) );\
									set_bit( _MD1(pin) )


#endif


