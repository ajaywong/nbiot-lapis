/*****************************************************************************
 User_Generic.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _USER_GENERIC_H_
#define _USER_GENERIC_H_




/*############################################################################*/
/*#                                  Macro                                   #*/
/*############################################################################*/
/* Flag */
#define FLAG_SET  ( 1 )
#define FLAG_CLR  ( 0 )

typedef char    int8_t;
typedef short   int16_t;
typedef long    int32_t;

typedef unsigned char   uint8_t;
typedef unsigned short  uint16_t;
typedef unsigned long   uint32_t;


/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/

#endif /*_USER_GENERIC_H_*/



