
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* MCU */
#include "rdwr_reg.h"
#include "mcu.h"
#include "clock.h"
#include "wdt.h"
#include "irq.h"

#include "i2c0.h"

#include "User_System.h"
#include "User_Generic.h"

#include "Driver_VL53L0X.h"

// Defines /////////////////////////////////////////////////////////////////////

// The Arduino two-wire interface uses a 7-bit number for the address,
// and sets the last bit correctly based on reads and writes
#define ADDRESS_DEFAULT 0x00

#define TIMEOUT		5000

#define IO_2V8

// Record the current time to check an upcoming timeout against
//#define startTimeout() (timeout_start_ms = millis())
#define startTimeout()  timeout_start_ms = 55000; io_timeout = 0 // 55000 ~ 490Ms

// Check if timeout is enabled (set to nonzero value) and has expired
//#define checkTimeoutExpired() (io_timeout > 0 && ((uint16_t)millis() - timeout_start_ms) > io_timeout)
#define checkTimeoutExpired() (io_timeout++ > timeout_start_ms)


// Decode VCSEL (vertical cavity surface emitting laser) pulse period in PCLKs
// from register value
// based on decode_vcsel_period()
#define decodeVcselPeriod(reg_val)      (((reg_val) + 1) << 1)

// Encode VCSEL pulse period register value from period in PCLKs
// based on encode_vcsel_period()
#define encodeVcselPeriod(period_pclks) (((period_pclks) >> 1) - 1)

// Calculate macro period in *nanoseconds* from VCSEL period in PCLKs
// based on calc_macro_period_ps()
// PLL_period_ps = 1655; macro_period_vclks = 2304
#define calcMacroPeriod(vcsel_period_pclks) ((((uint32_t)2304 * (vcsel_period_pclks) * 1655) + 500) / 1000)


struct SequenceStepEnables
{
    uint8_t tcc, msrc, dss, pre_range, final_range;
};

struct SequenceStepTimeouts
{
    uint16_t pre_range_vcsel_period_pclks, final_range_vcsel_period_pclks;

    uint16_t msrc_dss_tcc_mclks, pre_range_mclks, final_range_mclks;
    uint32_t msrc_dss_tcc_us,    pre_range_us,    final_range_us;
};




uint16_t io_timeout = 0;
uint16_t timeout_start_ms;

uint8_t did_timeout = 0;
uint8_t stop_variable; // read by init and used when starting measurement; is StopVariable field of DevData_t structure in API
uint32_t measurement_timing_budget_us;



/* Local Function Prototype */
void getSequenceStepEnables(struct SequenceStepEnables * enables);
void getSequenceStepTimeouts(struct SequenceStepEnables const * enables, struct SequenceStepTimeouts * timeouts);
uint32_t timeoutMsToMclks(uint32_t timeout_period_us, uint8_t vcsel_period_pclks);
uint32_t timeoutMclksToMs(uint16_t timeout_period_mclks, uint8_t vcsel_period_pclks);
uint16_t encodeTimeout(uint16_t timeout_mclks);
uint16_t decodeTimeout(uint16_t reg_val);
uint8_t performSingleRefCalibration(uint8_t vhv_init_byte);


//
uint8_t i2c_wr_data(uint8_t *pdata, uint8_t length)
{
    uint32_t timeout = TIMEOUT;
    int status;

    i2c0_wrStatus = 0;

    status = i2c0_write((unsigned char)I2C_SLAVE_ADDRESS_WRITE,
                        (unsigned char *)0, //will not use pointer to an address on this application
                        (unsigned int)0,    //address size is alwary 0 on this application
                        (unsigned char *)pdata,
                        (unsigned short)length,
                        (void *)0);


    while (( i2c0_wrStatus!= I2C_WRITE_END) || (status != I2C_R_OK))
    {
        if(!timeout --)
        {
            return 0;
        }
    }

    return 1;
}

//
uint8_t i2c_rd_data(uint8_t *pdata, uint8_t length)
{
    uint32_t timeout = TIMEOUT;
    int status;

    i2c0_rdStatus = 0;
    status = i2c0_read((unsigned char)I2C_SLAVE_ADDRESS_READ,
                       (unsigned char *)0,
                       (unsigned int)0,
                       (unsigned char *)pdata,
                       (unsigned short)length,
                       (void *)0);

    while ((i2c0_rdStatus != I2C_READ_END) || (status != I2C_R_OK))
    {
        if(!timeout --)
        {
            return 0;
        }
    }

    return 1;
}


// Write an 8-bit register
void writeReg(uint8_t reg, uint8_t value)
{
    i2c_txData[0] = reg;
    i2c_txData[1] = value;

    i2c_wr_data(i2c_txData, 2);
}


// Write a 16-bit register
void writeReg16Bit(uint8_t reg, uint16_t value)
{
    i2c_txData[0] = reg;
    i2c_txData[1] = (value >> 8) & 0xFF; // value high byte
    i2c_txData[2] = value & 0xFF; // value low byte

    i2c_wr_data(i2c_txData, 3);
}


// Write a 32-bit register
void writeReg32Bit(uint8_t reg, uint32_t value)
{
    i2c_txData[0] = reg;
    i2c_txData[1] = (value >> 24) & 0xFF; // value highest byte
    i2c_txData[2] = (value >> 16) & 0xFF;
    i2c_txData[3] = (value >>  8) & 0xFF;
    i2c_txData[4] = value & 0xFF; // value lowest byte

    i2c_wr_data(i2c_txData, 5);
}


// Read an 8-bit register
uint8_t readReg(uint8_t reg)
{
    i2c_txData[0] = reg;
    i2c_wr_data(i2c_txData, 1);

    i2c_rd_data(i2c_rxData, 1);

    return i2c_rxData[0];
}

// Read a 16-bit register
uint16_t readReg16Bit(uint8_t reg)
{
    uint16_t value;

    i2c_txData[0] = reg;
    i2c_wr_data(i2c_txData, 1);

    i2c_rd_data(i2c_rxData, 2);

    value = i2c_rxData[0];
    value = value << 8; // value high byte
    value |= i2c_rxData[1]; // value low byte

    return value;
}

// Read a 32-bit register
uint32_t readReg32Bit(uint8_t reg)
{
    uint32_t value;

    //
    i2c_txData[0] = reg;
    i2c_wr_data(i2c_txData, 1);

    //
    i2c_rd_data(i2c_rxData, 4);

    value = i2c_rxData[0];  // value highest byte
    value = value <<8;

    value |= i2c_rxData[1];
    value = value <<8;

    value |= i2c_rxData[2];
    value = value <<8;

    value |= i2c_rxData[3]; // value lowest byte

    return value;
}

// Write an arbitrary number of bytes from the given array to the sensor,
// starting at the given register
void writeMulti(uint8_t reg, uint8_t const * src, uint8_t count)
{
    i2c_txData[0] = reg;

    memcpy(i2c_txData + 1, src, count);

    i2c_wr_data(i2c_txData, count + 1);
}

// Read an arbitrary number of bytes from the sensor, starting at the given
// register, into the given array
void readMulti(uint8_t reg, uint8_t * dst, uint8_t count)
{
    i2c_txData[0] = reg;
    i2c_wr_data(i2c_txData, 1);

    i2c_rd_data(i2c_rxData, count);

    memcpy(dst, i2c_rxData, count);
}

// setAddress
void setAddress(uint8_t new_addr)
{
    //writeReg(I2C_SLAVE_DEVICE_ADDRESS, new_addr & 0x7F);
    //address = new_addr;
}



// Get sequence step enables
// based on GetSequenceStepEnables()
void getSequenceStepEnables(struct SequenceStepEnables * enables)
{
    uint8_t sequence_config = readReg(SYSTEM_SEQUENCE_CONFIG);

    enables->tcc          = (sequence_config >> 4) & 0x1;
    enables->dss          = (sequence_config >> 3) & 0x1;
    enables->msrc         = (sequence_config >> 2) & 0x1;
    enables->pre_range    = (sequence_config >> 6) & 0x1;
    enables->final_range  = (sequence_config >> 7) & 0x1;
}


// Set the return signal rate limit check value in units of MCPS (mega counts
// per second). "This represents the amplitude of the signal reflected from the
// target and detected by the device"; setting this limit presumably determines
// the minimum measurement necessary for the sensor to report a valid reading.
// Setting a lower limit increases the potential range of the sensor but also
// seems to increase the likelihood of getting an inaccurate reading because of
// unwanted reflections from objects other than the intended target.
// Defaults to 0.25 MCPS as initialized by the ST API and this library.
uint8_t setSignalRateLimit(float limit_Mcps)
{
    if (limit_Mcps < 0 || limit_Mcps > 511.99)
    {
        return 0;
    }

    // Q9.7 fixed point format (9 integer bits, 7 fractional bits)
    writeReg16Bit(FINAL_RANGE_MIN_RATE_RTN_LIMIT, (uint16_t)limit_Mcps * (1 << 7));

    return 1;
}

// Get the return signal rate limit check value in MCPS
float getSignalRateLimit(void)
{
    return (float)readReg16Bit(FINAL_RANGE_MIN_RATE_RTN_LIMIT) / (1 << 7);
}

// Set the measurement timing budget in microseconds, which is the time allowed
// for one measurement; the ST API and this library take care of splitting the
// timing budget among the sub-steps in the ranging sequence. A longer timing
// budget allows for more accurate measurements. Increasing the budget by a
// factor of N decreases the range measurement standard deviation by a factor of
// sqrt(N). Defaults to about 33 milliseconds; the minimum is 20 ms.
// based on set_measurement_timing_budget_micro_seconds()
uint8_t setMeasureTimingBudget(uint32_t budget_us)
{
    struct SequenceStepEnables enables;
    struct SequenceStepTimeouts timeouts;

    /*
    uint16_t const StartOverhead      = 1320; // note that this is different than the value in get_
    uint16_t const EndOverhead        = 960;
    uint16_t const MsrcOverhead       = 660;
    uint16_t const TccOverhead        = 590;
    uint16_t const DssOverhead        = 690;
    uint16_t const PreRangeOverhead   = 660;
    uint16_t const FinalRangeOverhead = 550;
    uint32_t const MinTimingBudget = 20000;
    */

    uint16_t  StartOverhead      = 1320; // note that this is different than the value in get_
    uint16_t  EndOverhead        = 960;
    uint16_t  MsrcOverhead       = 660;
    uint16_t  TccOverhead        = 590;
    uint16_t  DssOverhead        = 690;
    uint16_t  PreRangeOverhead   = 660;
    uint16_t  FinalRangeOverhead = 550;
    uint32_t  MinTimingBudget = 20000;

    uint32_t final_range_timeout_us;
    uint16_t final_range_timeout_mclks;
    uint32_t used_budget_us = StartOverhead + EndOverhead;

    if (budget_us < MinTimingBudget)
    {
        return 0;
    }

    getSequenceStepEnables(&enables);
    getSequenceStepTimeouts(&enables, &timeouts);

    if (enables.tcc)
    {
        used_budget_us += (timeouts.msrc_dss_tcc_us + TccOverhead);
    }

    if (enables.dss)
    {
        used_budget_us += 2 * (timeouts.msrc_dss_tcc_us + DssOverhead);
    }
    else if (enables.msrc)
    {
        used_budget_us += (timeouts.msrc_dss_tcc_us + MsrcOverhead);
    }

    if (enables.pre_range)
    {
        used_budget_us += (timeouts.pre_range_us + PreRangeOverhead);
    }

    if (enables.final_range)
    {
        used_budget_us += FinalRangeOverhead;

        // "Note that the final range timeout is determined by the timing
        // budget and the sum of all other timeouts within the sequence.
        // If there is no room for the final range timeout, then an error
        // will be set. Otherwise the remaining time will be applied to
        // the final range."

        if (used_budget_us > budget_us)
        {
            // "Requested timeout too big."
            return 0;
        }

        final_range_timeout_us = budget_us - used_budget_us;

        // set_sequence_step_timeout() begin
        // (SequenceStepId == SEQUENCESTEP_FINAL_RANGE)

        // "For the final range timeout, the pre-range timeout
        //  must be added. To do this both final and pre-range
        //  timeouts must be expressed in macro periods MClks
        //  because they have different vcsel periods."
        final_range_timeout_mclks = timeoutMsToMclks(final_range_timeout_us, timeouts.final_range_vcsel_period_pclks);
        if (enables.pre_range)
        {
            final_range_timeout_mclks += timeouts.pre_range_mclks;
        }

        writeReg16Bit(FINAL_RANGE_TIMEOUT_MACROP_HI, encodeTimeout(final_range_timeout_mclks));

        // set_sequence_step_timeout() end
        measurement_timing_budget_us = budget_us; // store for internal reuse
    }

    return 1;
}

// Get the measurement timing budget in microseconds
// based on get_measurement_timing_budget_micro_seconds()
// in us
uint32_t getMeasureTimingBudget(void)
{
    struct SequenceStepEnables enables;
    struct SequenceStepTimeouts timeouts;

    /*
    uint16_t const StartOverhead     = 1910; // note that this is different than the value in set_
    uint16_t const EndOverhead        = 960;
    uint16_t const MsrcOverhead       = 660;
    uint16_t const TccOverhead        = 590;
    uint16_t const DssOverhead        = 690;
    uint16_t const PreRangeOverhead   = 660;
    uint16_t const FinalRangeOverhead = 550;
    */
    uint16_t  StartOverhead     = 1910; // note that this is different than the value in set_
    uint16_t  EndOverhead        = 960;
    uint16_t  MsrcOverhead       = 660;
    uint16_t  TccOverhead        = 590;
    uint16_t  DssOverhead        = 690;
    uint16_t  PreRangeOverhead   = 660;
    uint16_t  FinalRangeOverhead = 550;
    // "Start and end overhead times always present"
    uint32_t budget_us = StartOverhead + EndOverhead;

    getSequenceStepEnables(&enables);
    getSequenceStepTimeouts(&enables, &timeouts);

    if (enables.tcc)
    {
        budget_us += (timeouts.msrc_dss_tcc_us + TccOverhead);
    }

    if (enables.dss)
    {
        budget_us += 2 * (timeouts.msrc_dss_tcc_us + DssOverhead);
    }
    else if (enables.msrc)
    {
        budget_us += (timeouts.msrc_dss_tcc_us + MsrcOverhead);
    }

    if (enables.pre_range)
    {
        budget_us += (timeouts.pre_range_us + PreRangeOverhead);
    }

    if (enables.final_range)
    {
        budget_us += (timeouts.final_range_us + FinalRangeOverhead);
    }

    measurement_timing_budget_us = budget_us; // store for internal reuse
    return budget_us;
}

// Set the VCSEL (vertical cavity surface emitting laser) pulse period for the
// given period type (pre-range or final range) to the given value in PCLKs.
// Longer periods seem to increase the potential range of the sensor.
// Valid values are (even numbers only):
//  pre:  12 to 18 (initialized default: 14)
//  final: 8 to 14 (initialized default: 10)
// based on set_vcsel_pulse_period()
uint8_t setVcselPulsePeriod(enum vcselPeriodType type, uint8_t period_pclks)
{
    uint8_t vcsel_period_reg = encodeVcselPeriod(period_pclks);
    uint16_t new_pre_range_timeout_mclks;
    uint16_t new_msrc_timeout_mclks;
    uint16_t new_final_range_timeout_mclks;
    uint8_t sequence_config;

    struct SequenceStepEnables enables;
    struct SequenceStepTimeouts timeouts;

    getSequenceStepEnables(&enables);
    getSequenceStepTimeouts(&enables, &timeouts);

    // "Apply specific settings for the requested clock period"
    // "Re-calculate and apply timeouts, in macro periods"

    // "When the VCSEL period for the pre or final range is changed,
    // the corresponding timeout must be read from the device using
    // the current VCSEL period, then the new VCSEL period can be
    // applied. The timeout then must be written back to the device
    // using the new VCSEL period.
    //
    // For the MSRC timeout, the same applies - this timeout being
    // dependant on the pre-range vcsel period."


    if (type == VcselPeriodPreRange)
    {
        // "Set phase check limits"
        switch (period_pclks)
        {
        case 12:
            writeReg(PRE_RANGE_VALID_PHASE_HIGH, 0x18);
            break;

        case 14:
            writeReg(PRE_RANGE_VALID_PHASE_HIGH, 0x30);
            break;

        case 16:
            writeReg(PRE_RANGE_VALID_PHASE_HIGH, 0x40);
            break;

        case 18:
            writeReg(PRE_RANGE_VALID_PHASE_HIGH, 0x50);
            break;

        default:
            // invalid period
            return 0;
        }
        writeReg(PRE_RANGE_VALID_PHASE_LOW, 0x08);

        // apply new VCSEL period
        writeReg(PRE_RANGE_CONFIG_VCSEL_PERIOD, vcsel_period_reg);

        // update timeouts

        // set_sequence_step_timeout() begin
        // (SequenceStepId == SEQUENCESTEP_PRE_RANGE)

        new_pre_range_timeout_mclks = timeoutMsToMclks(timeouts.pre_range_us, period_pclks);

        writeReg16Bit(PRE_RANGE_TIMEOUT_MACROP_HI, encodeTimeout(new_pre_range_timeout_mclks));

        // set_sequence_step_timeout() end

        // set_sequence_step_timeout() begin
        // (SequenceStepId == SEQUENCESTEP_MSRC)
        new_msrc_timeout_mclks = timeoutMsToMclks(timeouts.msrc_dss_tcc_us, period_pclks);

        writeReg(MSRC_CONFIG_TIMEOUT_MACROP, (new_msrc_timeout_mclks > 256) ? 255 : (new_msrc_timeout_mclks - 1));

        // set_sequence_step_timeout() end
    }
    else if (type == VcselPeriodFinalRange)
    {
        switch (period_pclks)
        {
        case 8:
            writeReg(FINAL_RANGE_VALID_PHASE_HIGH, 0x10);
            writeReg(FINAL_RANGE_VALID_PHASE_LOW,  0x08);
            writeReg(GLOBAL_VCSEL_WIDTH, 0x02);
            writeReg(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x0C);
            writeReg(0xFF, 0x01);
            writeReg(ALGO_PHASECAL_LIM, 0x30);
            writeReg(0xFF, 0x00);
            break;

        case 10:
            writeReg(FINAL_RANGE_VALID_PHASE_HIGH, 0x28);
            writeReg(FINAL_RANGE_VALID_PHASE_LOW,  0x08);
            writeReg(GLOBAL_VCSEL_WIDTH, 0x03);
            writeReg(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x09);
            writeReg(0xFF, 0x01);
            writeReg(ALGO_PHASECAL_LIM, 0x20);
            writeReg(0xFF, 0x00);
            break;

        case 12:
            writeReg(FINAL_RANGE_VALID_PHASE_HIGH, 0x38);
            writeReg(FINAL_RANGE_VALID_PHASE_LOW,  0x08);
            writeReg(GLOBAL_VCSEL_WIDTH, 0x03);
            writeReg(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x08);
            writeReg(0xFF, 0x01);
            writeReg(ALGO_PHASECAL_LIM, 0x20);
            writeReg(0xFF, 0x00);
            break;

        case 14:
            writeReg(FINAL_RANGE_VALID_PHASE_HIGH, 0x48);
            writeReg(FINAL_RANGE_VALID_PHASE_LOW,  0x08);
            writeReg(GLOBAL_VCSEL_WIDTH, 0x03);
            writeReg(ALGO_PHASECAL_CONFIG_TIMEOUT, 0x07);
            writeReg(0xFF, 0x01);
            writeReg(ALGO_PHASECAL_LIM, 0x20);
            writeReg(0xFF, 0x00);
            break;

        default:
            // invalid period
            return 0;
        }

        // apply new VCSEL period
        writeReg(FINAL_RANGE_CONFIG_VCSEL_PERIOD, vcsel_period_reg);

        // update timeouts

        // set_sequence_step_timeout() begin
        // (SequenceStepId == SEQUENCESTEP_FINAL_RANGE)

        // "For the final range timeout, the pre-range timeout
        //  must be added. To do this both final and pre-range
        //  timeouts must be expressed in macro periods MClks
        //  because they have different vcsel periods."

        new_final_range_timeout_mclks = timeoutMsToMclks(timeouts.final_range_us, period_pclks);

        if (enables.pre_range)
        {
            new_final_range_timeout_mclks += timeouts.pre_range_mclks;
        }

        writeReg16Bit(FINAL_RANGE_TIMEOUT_MACROP_HI, encodeTimeout(new_final_range_timeout_mclks));

        // set_sequence_step_timeout end
    }
    else
    {
        // invalid type
        return 0;
    }

    // "Finally, the timing budget must be re-applied"

    setMeasureTimingBudget(measurement_timing_budget_us);

    // "Perform the phase calibration. This is needed after changing on vcsel period."
    // perform_phase_calibration() begin

    sequence_config = readReg(SYSTEM_SEQUENCE_CONFIG);
    writeReg(SYSTEM_SEQUENCE_CONFIG, 0x02);
    performSingleRefCalibration(0x0);
    writeReg(SYSTEM_SEQUENCE_CONFIG, sequence_config);

    // perform_phase_calibration() end

    return 1;
}

// Get the VCSEL pulse period in PCLKs for the given period type.
// based on get_vcsel_pulse_period()
uint8_t getVcselPulsePeriod(enum vcselPeriodType type)
{
    if (type == VcselPeriodPreRange)
    {
        return decodeVcselPeriod(readReg(PRE_RANGE_CONFIG_VCSEL_PERIOD));
    }
    else if (type == VcselPeriodFinalRange)
    {
        return decodeVcselPeriod(readReg(FINAL_RANGE_CONFIG_VCSEL_PERIOD));
    }
    else
    {
        return 255;
    }
}

// Start continuous ranging measurements. If period_ms (optional) is 0 or not
// given, continuous back-to-back mode is used (the sensor takes measurements as
// often as possible); otherwise, continuous timed mode is used, with the given
// inter-measurement period in milliseconds determining how often the sensor
// takes a measurement.
// based on StartMeasurement()
void startContinuous(uint32_t period_ms)
{
    uint16_t osc_calibrate_val;

    writeReg(0x80, 0x01);
    writeReg(0xFF, 0x01);
    writeReg(0x00, 0x00);
    writeReg(0x91, stop_variable);
    writeReg(0x00, 0x01);
    writeReg(0xFF, 0x00);
    writeReg(0x80, 0x00);

    if (period_ms != 0)
    {
        // continuous timed mode
        // SetInterMeasurementPeriodMilliSeconds() begin

        osc_calibrate_val = readReg16Bit(OSC_CALIBRATE_VAL);

        if (osc_calibrate_val != 0)
        {
            period_ms *= osc_calibrate_val;
        }

        writeReg32Bit(SYSTEM_INTERMEASUREMENT_PERIOD, period_ms);

        // SetInterMeasurementPeriodMilliSeconds() end

        writeReg(SYSRANGE_START, 0x04); // REG_SYSRANGE_MODE_TIMED
    }
    else
    {
        // continuous back-to-back mode
        writeReg(SYSRANGE_START, 0x02); // REG_SYSRANGE_MODE_BACKTOBACK
    }
}

// Stop continuous measurements
// based on StopMeasurement()
void stopContinuous(void)
{
    writeReg(SYSRANGE_START, 0x01); // REG_SYSRANGE_MODE_SINGLESHOT

    writeReg(0xFF, 0x01);
    writeReg(0x00, 0x00);
    writeReg(0x91, 0x00);
    writeReg(0x00, 0x01);
    writeReg(0xFF, 0x00);
}

// Returns a range reading in millimeters when continuous mode is active
// (readRangeSingleMillimeters() also calls this function after starting a
// single-shot range measurement)
uint16_t readRangeContinuous(void)
{
    uint16_t range;

    startTimeout();
    while ((readReg(RESULT_INTERRUPT_STATUS) & 0x07) == 0)
    {
        if (checkTimeoutExpired())
        {
            return 0;
        }
    }

    // assumptions: Linearity Corrective Gain is 1000 (default);
    // fractional ranging is not enabled
    range = readReg16Bit(RESULT_RANGE_STATUS + 10);

    writeReg(SYSTEM_INTERRUPT_CLEAR, 0x01);

    return range;
}

// Performs a single-shot range measurement and returns the reading in
// millimeters
// based on PerformSingleRangingMeasurement()
uint16_t readRangeSingle(void)
{
    writeReg(0x80, 0x01);
    writeReg(0xFF, 0x01);
    writeReg(0x00, 0x00);
    writeReg(0x91, stop_variable);
    writeReg(0x00, 0x01);
    writeReg(0xFF, 0x00);
    writeReg(0x80, 0x00);

    writeReg(SYSRANGE_START, 0x01);

    // "Wait until start bit has been cleared"
    startTimeout();
    while (readReg(SYSRANGE_START) & 0x01)
    {
        if (checkTimeoutExpired())
        {
            return 0;
        }
    }

    return readRangeContinuous();
}

// Did a timeout occur in one of the read functions since the last call to
// timeoutOccurred()?
uint8_t timeoutOccurred()
{
    uint8_t tmp = did_timeout;
    did_timeout = 0;
    return tmp;
}

// Private Methods /////////////////////////////////////////////////////////////

// Get reference SPAD (single photon avalanche diode) count and type
// based on get_info_from_device(),
// but only gets reference SPAD count and type
uint8_t getSpadInfo(uint8_t * count, uint8_t* type_is_aperture)
{
    uint8_t tmp;

    writeReg(0x80, 0x01);
    writeReg(0xFF, 0x01);
    writeReg(0x00, 0x00);

    writeReg(0xFF, 0x06);
    writeReg(0x83, readReg(0x83) | 0x04);
    writeReg(0xFF, 0x07);
    writeReg(0x81, 0x01);

    writeReg(0x80, 0x01);

    writeReg(0x94, 0x6b);
    writeReg(0x83, 0x00);
    startTimeout();
    while (readReg(0x83) == 0x00)
    {
        if (checkTimeoutExpired())
        {
            return 0;
        }
    }
    writeReg(0x83, 0x01);
    tmp = readReg(0x92);

    *count = tmp & 0x7f;
    *type_is_aperture = (tmp >> 7) & 0x01;

    writeReg(0x81, 0x00);
    writeReg(0xFF, 0x06);
    writeReg(0x83, readReg(0x83)  & ~0x04);
    writeReg(0xFF, 0x01);
    writeReg(0x00, 0x01);

    writeReg(0xFF, 0x00);
    writeReg(0x80, 0x00);

    return 1;
}



// Get sequence step timeouts
// based on get_sequence_step_timeout(),
// but gets all timeouts instead of just the requested one, and also stores
// intermediate values
void getSequenceStepTimeouts(struct SequenceStepEnables const * enables, struct SequenceStepTimeouts * timeouts)
{
    timeouts->pre_range_vcsel_period_pclks = getVcselPulsePeriod(VcselPeriodPreRange);

    timeouts->msrc_dss_tcc_mclks = readReg(MSRC_CONFIG_TIMEOUT_MACROP) + 1;
    timeouts->msrc_dss_tcc_us =
        timeoutMclksToMs(timeouts->msrc_dss_tcc_mclks,
                         timeouts->pre_range_vcsel_period_pclks);

    timeouts->pre_range_mclks =
        decodeTimeout(readReg16Bit(PRE_RANGE_TIMEOUT_MACROP_HI));
    timeouts->pre_range_us =
        timeoutMclksToMs(timeouts->pre_range_mclks,
                         timeouts->pre_range_vcsel_period_pclks);

    timeouts->final_range_vcsel_period_pclks = getVcselPulsePeriod(VcselPeriodFinalRange);

    timeouts->final_range_mclks =
        decodeTimeout(readReg16Bit(FINAL_RANGE_TIMEOUT_MACROP_HI));

    if (enables->pre_range)
    {
        timeouts->final_range_mclks -= timeouts->pre_range_mclks;
    }

    timeouts->final_range_us =
        timeoutMclksToMs(timeouts->final_range_mclks,
                         timeouts->final_range_vcsel_period_pclks);
}

// Decode sequence step timeout in MCLKs from register value
// based on decode_timeout()
// Note: the original function returned a uint32_t, but the return value is
// always stored in a uint16_t.
uint16_t decodeTimeout(uint16_t reg_val)
{
    // format: "(LSByte * 2^MSByte) + 1"
    return (uint16_t)((reg_val & 0x00FF) <<
                      (uint16_t)((reg_val & 0xFF00) >> 8)) + 1;
}

// Encode sequence step timeout register value from timeout in MCLKs
// based on encode_timeout()
// Note: the original function took a uint16_t, but the argument passed to it
// is always a uint16_t.
uint16_t encodeTimeout(uint16_t timeout_mclks)
{
    // format: "(LSByte * 2^MSByte) + 1"

    uint32_t ls_byte = 0;
    uint16_t ms_byte = 0;

    if (timeout_mclks > 0)
    {
        ls_byte = timeout_mclks - 1;

        while ((ls_byte & 0xFFFFFF00) > 0)
        {
            ls_byte >>= 1;
            ms_byte++;
        }

        return (ms_byte << 8) | (ls_byte & 0xFF);
    }
    else
    {
        return 0;
    }
}

// Convert sequence step timeout from MCLKs to microseconds with given VCSEL period in PCLKs
// based on calc_timeout_us()
uint32_t timeoutMclksToMs(uint16_t timeout_period_mclks, uint8_t vcsel_period_pclks)
{
    uint32_t macro_period_ns = calcMacroPeriod(vcsel_period_pclks);

    return ((timeout_period_mclks * macro_period_ns) + (macro_period_ns / 2)) / 1000;
}

// Convert sequence step timeout from microseconds to MCLKs with given VCSEL period in PCLKs
// based on calc_timeout_mclks()
uint32_t timeoutMsToMclks(uint32_t timeout_period_us, uint8_t vcsel_period_pclks)
{
    uint32_t macro_period_ns = calcMacroPeriod(vcsel_period_pclks);

    return (((timeout_period_us * 1000) + (macro_period_ns / 2)) / macro_period_ns);
}


// based on perform_single_ref_calibration()
uint8_t performSingleRefCalibration(uint8_t vhv_init_byte)
{
    writeReg(SYSRANGE_START, 0x01 | vhv_init_byte); // REG_SYSRANGE_MODE_START_STOP

    startTimeout();
    while ((readReg(RESULT_INTERRUPT_STATUS) & 0x07) == 0)
    {
        if (checkTimeoutExpired())
        {
            return 0;
        }
    }

    writeReg(SYSTEM_INTERRUPT_CLEAR, 0x01);

    writeReg(SYSRANGE_START, 0x00);

    return 1;
}



// Initialize sensor using sequence based on DataInit(),
// StaticInit(), and PerformRefCalibration().
// This function does not perform reference SPAD calibration
// (PerformRefSpadManagement()), since the API user manual says that it
// is performed by ST on the bare modules; it seems like that should work well
// enough unless a cover glass is added.
// If io_2v8 (optional) is true or not given, the sensor is configured for 2V8
// mode.
uint8_t VL53L0X_Init(void)
{
    uint8_t ref_spad_map[6];
    uint8_t spad_count;
    uint8_t first_spad_to_enable;
    uint8_t spad_type_is_aperture;
    uint8_t spads_enabled = 0;
    uint8_t i;

    // sensor uses 1V8 mode for I/O by default; switch to 2V8 mode if necessary
#ifdef IO_2V8
    writeReg(VHV_CONFIG_PAD_I2C_EXTSUP_HV,
             readReg(VHV_CONFIG_PAD_I2C_EXTSUP_HV) | 0x01); // set bit 0
#endif


    // get ID
    ref_spad_map[0] = readReg(0xC0); // if read OK, the result is 0xEE
    ref_spad_map[1] = readReg(0xC1); // if read OK, the result is 0xAA
    ref_spad_map[2] = readReg(0xC2); // if read OK, the result is 0x10

    if((ref_spad_map[0] != 0xEE) || (ref_spad_map[1] != 0xAA) || (ref_spad_map[2] != 0x10))
    {
        DebugPrint("VL53L0X Not Find\r\n");
        return 0;
    }

    // DataInit
    // "Set I2C standard mode"
    writeReg(0x88, 0x00);

    writeReg(0x80, 0x01);
    writeReg(0xFF, 0x01);
    writeReg(0x00, 0x00);
    stop_variable = readReg(0x91);
    writeReg(0x00, 0x01);
    writeReg(0xFF, 0x00);
    writeReg(0x80, 0x00);

    // disable SIGNAL_RATE_MSRC (bit 1) and SIGNAL_RATE_PRE_RANGE (bit 4) limit checks
    writeReg(MSRC_CONFIG_CONTROL, readReg(MSRC_CONFIG_CONTROL) | 0x12);

    // set final range signal rate limit to 0.25 MCPS (million counts per second)
    setSignalRateLimit((float)0.25);
    writeReg(SYSTEM_SEQUENCE_CONFIG, 0xFF);

    // StaticInit
    if (!getSpadInfo(&spad_count, &spad_type_is_aperture))
    {
        return 0;
    }

    // The SPAD map (RefGoodSpadMap) is read by get_info_from_device() in
    // the API, but the same data seems to be more easily readable from
    // GLOBAL_SPAD_ENABLES_REF_0 through _6, so read it from there
    readMulti(GLOBAL_SPAD_ENABLES_REF_0, ref_spad_map, 6);

    // -- set_reference_spads() begin (assume NVM values are valid)
    writeReg(0xFF, 0x01);
    writeReg(DYNAMIC_SPAD_REF_START_OFFSET, 0x00);
    writeReg(DYNAMIC_SPAD_NUM_REF_SPAD, 0x2C);
    writeReg(0xFF, 0x00);
    writeReg(GLOBAL_REF_EN_START_SELECT, 0xB4);

    first_spad_to_enable = spad_type_is_aperture ? 12 : 0; // 12 is the first aperture spad

    for (i = 0; i < 48; i++)
    {
        if (i < first_spad_to_enable || spads_enabled == spad_count)
        {
            // This bit is lower than the first one that should be enabled, or
            // (reference_spad_count) bits have already been enabled, so zero this bit
            ref_spad_map[i / 8] &= ~(1 << (i % 8));
        }
        else if ((ref_spad_map[i / 8] >> (i % 8)) & 0x1)
        {
            spads_enabled++;
        }
    }
    writeMulti(GLOBAL_SPAD_ENABLES_REF_0, ref_spad_map, 6);

    // -- load_tuning_settings
    // DefaultTuningSettings from vl53l0x_tuning.h
    writeReg(0xFF, 0x01);
    writeReg(0x00, 0x00);

    writeReg(0xFF, 0x00);
    writeReg(0x09, 0x00);
    writeReg(0x10, 0x00);
    writeReg(0x11, 0x00);

    writeReg(0x24, 0x01);
    writeReg(0x25, 0xFF);
    writeReg(0x75, 0x00);

    writeReg(0xFF, 0x01);
    writeReg(0x4E, 0x2C);
    writeReg(0x48, 0x00);
    writeReg(0x30, 0x20);

    writeReg(0xFF, 0x00);
    writeReg(0x30, 0x09);
    writeReg(0x54, 0x00);
    writeReg(0x31, 0x04);
    writeReg(0x32, 0x03);
    writeReg(0x40, 0x83);
    writeReg(0x46, 0x25);
    writeReg(0x60, 0x00);
    writeReg(0x27, 0x00);
    writeReg(0x50, 0x06);
    writeReg(0x51, 0x00);
    writeReg(0x52, 0x96);
    writeReg(0x56, 0x08);
    writeReg(0x57, 0x30);
    writeReg(0x61, 0x00);
    writeReg(0x62, 0x00);
    writeReg(0x64, 0x00);
    writeReg(0x65, 0x00);
    writeReg(0x66, 0xA0);

    writeReg(0xFF, 0x01);
    writeReg(0x22, 0x32);
    writeReg(0x47, 0x14);
    writeReg(0x49, 0xFF);
    writeReg(0x4A, 0x00);

    writeReg(0xFF, 0x00);
    writeReg(0x7A, 0x0A);
    writeReg(0x7B, 0x00);
    writeReg(0x78, 0x21);

    writeReg(0xFF, 0x01);
    writeReg(0x23, 0x34);
    writeReg(0x42, 0x00);
    writeReg(0x44, 0xFF);
    writeReg(0x45, 0x26);
    writeReg(0x46, 0x05);
    writeReg(0x40, 0x40);
    writeReg(0x0E, 0x06);
    writeReg(0x20, 0x1A);
    writeReg(0x43, 0x40);

    writeReg(0xFF, 0x00);
    writeReg(0x34, 0x03);
    writeReg(0x35, 0x44);

    writeReg(0xFF, 0x01);
    writeReg(0x31, 0x04);
    writeReg(0x4B, 0x09);
    writeReg(0x4C, 0x05);
    writeReg(0x4D, 0x04);

    writeReg(0xFF, 0x00);
    writeReg(0x44, 0x00);
    writeReg(0x45, 0x20);
    writeReg(0x47, 0x08);
    writeReg(0x48, 0x28);
    writeReg(0x67, 0x00);
    writeReg(0x70, 0x04);
    writeReg(0x71, 0x01);
    writeReg(0x72, 0xFE);
    writeReg(0x76, 0x00);
    writeReg(0x77, 0x00);

    writeReg(0xFF, 0x01);
    writeReg(0x0D, 0x01);

    writeReg(0xFF, 0x00);
    writeReg(0x80, 0x01);
    writeReg(0x01, 0xF8);

    writeReg(0xFF, 0x01);
    writeReg(0x8E, 0x01);
    writeReg(0x00, 0x01);
    writeReg(0xFF, 0x00);
    writeReg(0x80, 0x00);

    // "Set interrupt config to new sample ready"
    // -- SetGpioConfig
    writeReg(SYSTEM_INTERRUPT_CONFIG_GPIO, 0x04);
    writeReg(GPIO_HV_MUX_ACTIVE_HIGH, readReg(GPIO_HV_MUX_ACTIVE_HIGH) & ~0x10); // active low
    writeReg(SYSTEM_INTERRUPT_CLEAR, 0x01);

    measurement_timing_budget_us = getMeasureTimingBudget();

    // -- SetSequenceStepEnable
    // "Disable MSRC and TCC by default"
    // MSRC = Minimum Signal Rate Check
    // TCC = Target CentreCheck
    writeReg(SYSTEM_SEQUENCE_CONFIG, 0xE8);

    // "Recalculate timing budget"
    setMeasureTimingBudget(measurement_timing_budget_us);

    writeReg(SYSTEM_SEQUENCE_CONFIG, 0x01);
    if (!performSingleRefCalibration(0x40))
    {
        return 0;
    }

    writeReg(SYSTEM_SEQUENCE_CONFIG, 0x02);
    if (!performSingleRefCalibration(0x00))
    {
        return 0;
    }

    // "restore the previous Sequence Config"
    writeReg(SYSTEM_SEQUENCE_CONFIG, 0xE8);

    return 1;
}






