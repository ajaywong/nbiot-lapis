

/*  */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/* MCU */
#include "mcu.h"
#include "wdt.h"
#include "irq.h"
#include "lp_manage.h"
#include "uart0.h"
#include "gpio_common.h"
#include "remap.h"
#include "clock.h"


/* User Include */
#include "User_System.h"
#include "User_Generic.h"
#include "User_Console.h"
#include "user_board.h"


/* Driver Include */
#include "Driver_Sigfox.h"
#include "Driver_Adc.h"
#include "Driver_DataFlash.h"

#include "uart0.h"

#define FLASH_SIZE					( 0x10000 )
#define ISP_AREA_START_ADDR			( 0x0F000 )
#define TEST_AREA_END_ADDR			( 0x0FFFF )
#define FLASH_MIN_SECTOR_ERASE_ADRS	( 0x0E000 )

extern unsigned char s_txBuffer[];    // UART Buffer for Sigfox Communication

#define DebugPrintTest  printf


#define REPLY_NONE          0
#define REPLY_OK            1
#define REPLY_FAIL          2
#define REPLY_DATA          3
#define REPLY_TMD_ERROR     4
#define REPLY_TEST_END      5

enum
{
  /* Test Mode Related Command */
    LedRedON = 1,
    LedRedOFF,
    LedGreenON,
    LedGreenOFF,
    Key,
    Battery,
    Temperature,
    Distance,
    Motion,
    SetRTC,
    SetRTCAlarm,
    SigfoxTest,
    SigfoxSendFrame,
    Leakage,    
    
  /* System Setting Related Command */
    SigfoxReset = 50,
    Get_Device_Info = 51,    // Dump the Device Info    
    Enable_public_key,        // Enable the Public key for Sigfox
    Disable_public_key,       // Disable the Public key for Sigfox
    Dump_Public_Key_Reg,   // Dump out the public key status
    Enable_FCC,               // Enable FCC, real hop among 9 macrochannel
    Disable_FCC,              // Disable FCC Rule, always fix one macrochannel
    Dump_FCC_reg,             // Dump FCC register 
    Enable_OOB_w_Period,       // Enable OOB, and set with valid Period (1-24)
    Disable_OOB,              // Disable OOB
    Dump_OOB,                 // Dump OOB
    DownlinkToogle,           // Toogle Downlink Mode
    DownlinkToogleMain,           // Toogle Downlink Mode  
    
  /* Device Specific Related Command */
    Set_Leakage_interval = 81,     // set the schedule time for the Water leakage
    Get_Leakage_interval,           // Dump Leakage interval
    
    Sleep = 88,
    Water_Model = 89,
    
  /* Firmware Upgrade */
    Firmware_upgarde = 999,
    
  /* TEST_CMD_MAX*/    
    TEST_CMD_MAX   
    
} testCmd;

/*******************************************************************************
    Routine Name:   ResetPeri 
    Form:           void ResetPeri(void)
    Parameters:     void
    Return value:   void
    Description:    Reset Peripheral
******************************************************************************/
void ResetPeri(void)
{
	/* Disable UART Interrupt */
	irq_ua0_dis();
	irq_ua1_dis();

	/* Clear UART Interrupt Request */
	irq_ua0_clearIRQ();
	irq_ua1_clearIRQ();

	/* Set initial value PORT Register */
#if defined(_ML620Q15X)
	write_reg8(P0CON0, 0x00);
	write_reg8(P0CON1, 0x00);
#elif defined(_ML620Q40X_50X)
	clear_bit(P51DIR);
//	clear_bit(P51C0);
	clear_bit(P51C1);
#endif

	//uart_clear_port();
	write_reg16(P4MOD,0x0000);
	write_reg16(P4CON,0x0000);
	write_reg8(P4DIR,0x00);

	//led_reset();
	clear_bit(P52C0);
	clear_bit(P52C1);
	clear_bit(P52D);

	/* Reset Peripheral with BLKCON */
	ResetPeriBlock();

	/* Set initial value CLOCK */
	//sys_reset_clock();
	clk_setSysclk(CLK_SYSCLK_LSCLK);
	clk_disHsclk();
//	clk_setHsclk(FCON0_INIT_DATA);

	/* Set initial to HSCLK */
	clk_enaHsclk();
	__asm("nop");
	__asm("nop");
	clk_setSysclk(CLK_SYSCLK_HSCLK);	/* Sets to HSCLK */  

	write_reg16(LTBINT, 0x0630);
}



unsigned char Test_Mode(void)
{
    uint8_t reply = REPLY_NONE;
    unsigned char ret;
    uint8_t exit_test = 0;

    /*  */
    GPIO_UART0_RXD(UART0_RXD_PIN);
    GPIO_UART0_TXD(UART0_TXD_PIN);

    /* enable interrupt */
    irq_ua0_clearIRQ();
    irq_ua1_clearIRQ();
    irq_ua0_ena();
    irq_ua1_ena();
    
    strcpy(uart0_wd_Buffer, "Test Mode In\r\n");
    testMode_Write(strlen(uart0_wd_Buffer));
    
    testMode_Read();

    //Test Mode
    while((get_bit(P32D) == 0) && (exit_test != 1))
    {
        wdt_clear();
        //Get Test Command
        if(testMode_getData())
        {
            if(strncmp(uart0_Buffer, ":TMD", 4) == 0)
            {

                unsigned short oob_period;
                unsigned long s_time;            
                // Parser Test Command
                testCmd = atoi(&uart0_Buffer[4]);

                if(testCmd)
                {
                    switch(testCmd)
                    {
                    case LedRedON:   //Turn On LED Red
                        onLED(RED_LED);
                        reply = REPLY_OK;
                        break;
                    case LedRedOFF:
                        offLED(RED_LED);
                        reply = REPLY_OK;
                        break;
                    case LedGreenON: //Turn On LED Red
                        onLED(GREEN_LED);
                        reply = REPLY_OK;
                        break;
                    case LedGreenOFF:
                        offLED(GREEN_LED);
                        reply = REPLY_OK;
                        break;
#ifdef P31_SW_ENABLE                        
                    case Key:   //Check Key
                        if(get_bit(P31D) == 0)
                        {
                            reply = REPLY_OK;
                        }
                        else
                        {
                            reply = REPLY_FAIL;
                        }
                        break;
#endif                        

                    case Battery: //
                        get_Adc_Value();
                        sprintf(uart0_wd_Buffer, "%03d\r\n", batteryCapacity);
                        reply = REPLY_DATA;
                        break;

                    case SigfoxTest:
                        Sigfox_HW_Reset();
                        strcpy(uart0_wd_Buffer, "OK\r\n");
                        testMode_Write(strlen(uart0_wd_Buffer));
                        TestMode_SigfoxTest();

                        reply = REPLY_OK;
                        break;
                        
                    case DownlinkToogle:
                        replyOption = SIGFOX_REPLY;
                    case SigfoxSendFrame:
                          Sigfox_HW_Reset();
                          if(Sigfox_SendFrameInit() == SIGFOX_OK)
                            {
                                strcpy(uart0_wd_Buffer,s_txBuffer);
                                testMode_Write(strlen(uart0_wd_Buffer));

                                while(1)
                                {
                                    wdt_clear();
                                    reply = Sigfox_SendFrameCheck();
                                    if(reply == SIGFOX_OK)
                                    {
                                        reply = REPLY_OK;
                                        break;
                                    }
                                    else if(reply == SIGFOX_ERROR)
                                    {
                                        reply = REPLY_FAIL;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                reply = REPLY_FAIL;
                            }
                        break;
                    case Leakage:
                        if(GET_BIT(TRIGGER_COUNTER_PIN) == 1)
                        {
                            DebugPrintTest("Below Threshold\r\n");
                        }
                        else
                        {
                            DebugPrintTest("Above Threshold\r\n");
                        }
                        reply = REPLY_OK;
                        break;

                    case Set_Leakage_interval:                    
                        reply = REPLY_FAIL;
                        s_time = atol(&uart0_Buffer[8]);
                        DebugPrintTest("Set Leakage Time interval %s\r\n", s_time);
                        if((s_time != schedule_Time) && (s_time >= MIN_SCHEDUCLE_TIME))
                        {
                            schedule_Time = s_time;
                            DebugPrintTest("Schedule Time updated:%d\r\n", schedule_Time);
                            
                            userData[2] = ((schedule_Time >> 16) & 0xffff);        
                            userData[3] = (schedule_Time & 0x0000ffff);        
                            UserDataFlash_Write(userData, USER_DATA_SIZE);                            

                            /* save up the downlink set schedule time*/
                            UserDataFlash_Write(userData, USER_DATA_SIZE);
                        }
                        reply = REPLY_OK;
                        break;
                    case Get_Leakage_interval:
                      DebugPrintTest("Current Schedule Time:%d\r\n", schedule_Time);
                      reply = REPLY_OK;
                      break;
                    case Firmware_upgarde:
                      DebugPrintTest("Firmware Upgarde\r\n");
                      Remap((unsigned long)ISP_AREA_START_ADDR);
                      reply = REPLY_OK;
                      break;
                    case Sleep:
                      exit_test = 1;
                      break;
                    case Water_Model:
                      #ifdef AM53     // Water Leakage
                      DebugPrintTest("It is AM53 Project\r\n");
                      #else           // Water Water Rope
                      DebugPrintTest("It is AM52 Project\r\n");
                      #endif
                    default:
                        reply = REPLY_TMD_ERROR;
                        break;
                    } // End of Switch
                }// End of if
            }
            else
            {
                reply = REPLY_TMD_ERROR;
                            
            }
        }
        ///
        if(reply != REPLY_NONE)
        {
            if(reply == REPLY_OK)
            {
                strcpy(uart0_wd_Buffer, "OK\r\n");
            }
            else if(reply == REPLY_FAIL)
            {
                strcpy(uart0_wd_Buffer, "FAIL\r\n");
            }
            else if(reply == REPLY_TMD_ERROR)
            {
                strcpy(uart0_wd_Buffer, "CMD ERROR\r\n");
            }

            testMode_Write(strlen(uart0_wd_Buffer));
            testMode_Read();
            reply = REPLY_NONE;
        }

    }//End of While
    
    strcpy(uart0_wd_Buffer, "Test Mode Out\r\n");
    testMode_Write(strlen(uart0_wd_Buffer));
    
    ///
    offLED(RED_LED);
    offLED(GREEN_LED);

    irq_ua0_dis(); /* disable interrupt */
    irq_ua1_dis(); /* disable interrupt */
    irq_ua0_clearIRQ();
    irq_ua1_clearIRQ();

    /* UART 0 as GPIO*/
    GPIO_INPUT_PUTUP( UART0_RXD_PIN);
    GPIO_INPUT_OUTPUT_MODE(UART0_RXD_PIN);
    GPIO_INPUT_PUTUP( UART0_TXD_PIN);
    GPIO_INPUT_OUTPUT_MODE(UART0_TXD_PIN);
    //
    return 0;
}




















