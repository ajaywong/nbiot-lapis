/*****************************************************************************


 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
/*  */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* MCU */
#include "rdwr_reg.h"
#include "mcu.h"
#include "clock.h"
#include "wdt.h"
#include "irq.h"

#include "i2c0.h"



/*   */
#include "User_System.h"
#include "User_Generic.h"

/*  */
#include "Driver_VL53L0X.h"
#include "Driver_DistanceSensor.h"


// Power to distance sensor (VL53L0X): Active-HIGH = POWER ON
#define LASER_PWR_ON		P35D = 1
#define LASER_PWR_OFF		P35D = 0

// Reset to distance sensor (VL53L0X): Active-LOW = SHUT DOWN (STANDBY)
#define D_SHT_ON			P36D = 0
#define D_SHT_OFF			P36D = 1

// Output enable to translator signals (TXB0304RUT): Active-HIGH = Output enable
#define D_OE_ON				P37D = 1
#define D_OE_OFF			P37D = 0

#define READ_DIST_SENSOR_COUNT		4
#define MAX_BIN_DIST   2000  //Millimeters


#define  LONG_RANGE
//#define  HIGH_SPEED
//#define  HIGH_ACCURACY

uint16_t bin_distance;
unsigned char bin_capacity;

void Power_On_DistSensorCtrl(void)
{
    // Distance sensor control
    D_SHT_OFF;  // not shut down distance sensor
    LASER_PWR_ON; // enable power to distance sensor
    D_OE_ON;   // enable translator signals to distance sensor
}

void Power_Off_DistSensorCtrl(void)
{
    // Distance sensor control
    D_SHT_ON;  // shut down distance sensor

    clear_bit(P40D);
    clear_bit( P40DIR );			/* Output */
    set_bit( P40C0 );				/* CMOS */
    set_bit( P40C1 );
    clear_bit( P40MD0 );			/* GPIO mode */
    clear_bit( P40MD1 );

    clear_bit(P41D);
    clear_bit( P41DIR );			/* Output */
    set_bit( P41C0 );				/* CMOS */
    set_bit( P41C1 );
    clear_bit( P41MD0 );			/* GPIO mode */
    clear_bit( P41MD1 );

    D_OE_OFF;   // disable translator signals to distance sensor
    LASER_PWR_OFF; // disable power to distance sensor
}

void Standby_On_DistSensorCtrl(void)
{
    // Distance sensor control
    D_SHT_OFF;  // not shut down distance sensor
}

void Standby_Off_DistSensorCtrl(void)
{
    // Distance sensor control
    D_SHT_ON;  // shut down distance sensor
}





uint8_t Run_DistSensorCtrl(void)
{
    uint8_t i;
    uint8_t stateCode;
    uint16_t range=0;
    uint16_t result=0;

    stateCode = VL53L0X_Init();
    if(stateCode == 0)
    {
        return RET_ERROR;
    }

#if defined LONG_RANGE
    // lower the return signal rate limit (default is 0.25 MCPS)
    setSignalRateLimit((float)0.1);
    // increase laser pulse periods (defaults are 14 and 10 PCLKs)
    setVcselPulsePeriod(VcselPeriodPreRange, 18);
    setVcselPulsePeriod(VcselPeriodFinalRange, 14);
#endif // defined


#if defined HIGH_SPEED
    // reduce timing budget to 20 ms (default is about 33 ms)
    setMeasureTimingBudget(20000);
#elif defined HIGH_ACCURACY
    // increase timing budget to 200 ms
    setMeasureTimingBudget(200000);
#endif

    for(i=0; i<READ_DIST_SENSOR_COUNT; i++)
    {
        result = readRangeSingle();

        if(result == 0)
        {
            DebugPrint("Time Out\r\n");
        }

        if(result > MAX_BIN_DIST)
        {
            result = MAX_BIN_DIST;
        }

        range += result;
    }

    range = range/READ_DIST_SENSOR_COUNT;
    bin_distance = range;

    bin_capacity = (uint8_t)(((uint16_t)(range)/15)); //2000mm

    if(bin_capacity >= 100)
    {
        bin_capacity = 100;
    }

    bin_capacity = 100 - bin_capacity;

    //DebugPrint("Dist:%dmm, Full:%%d\r\n", bin_distance, bin_capacity);

    return RET_OK;
}



