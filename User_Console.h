/*****************************************************************************
 User_Console.h

 Copyright (C).
 All rights reserved.


 History
    2014.06.17 ver.1.00

******************************************************************************/
/**
 *
 */
#ifndef _USER_CONSOLE_H_
#define _USER_CONSOLE_H_

#include "uart0.h"



/*############################################################################*/
/*#                                Variable                                  #*/
/*############################################################################*/
#define UART_PARAM_MODE0    ( UART_CS_HSCLK | UART_RSS_BR_DIV2 )                                          /**< Parameters for UART control */
#define UART_PARAM_MODE1    ( UART_LG_8BIT  | UART_PT_NON | UART_STP_1BIT | UART_NEG_POS | UART_DIR_LSB ) /**< Parameters for communication protocol */
#define UART0_115200BPS     ( 0x008A ) /**< The parameter of baud-rate */
#define UART0_19200BPS      ( 0x0340 )
#define UART0_9600BPS       ( 0x0682 )


#define UART0_BUFFSIZE  50


extern unsigned char uart0_Buffer[];
extern unsigned char uart0_wd_Buffer[];



/*############################################################################*/
/*#                                  API                                     #*/
/*############################################################################*/


void testMode_Read(void);
unsigned char testMode_getData(void);

void testMode_Write(unsigned char len);
unsigned char TestMode_SigfoxTest(void);
#endif /*_USER_CONSOLE_H_*/



