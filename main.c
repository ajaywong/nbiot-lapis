/*****************************************************************************
 main.c

 Copyright (C)
 All rights reserved.

    2018.Feb.6 ver.1.00

******************************************************************************/

/*  */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/* MCU */
#include "mcu.h"
#include "wdt.h"
#include "lp_manage.h"
#include "gpio_common.h"


/* User Include */
#include "User_System.h"
#include "User_Generic.h"
#include "Test_Mode.h"
#include "Driver_DataFlash.h"

/* Driver Include */
#include "Driver_Sigfox.h"
#include "Driver_Adc.h"

#include "Driver_Battery.h"
#include "Driver_TempSensor.h"
#include "Driver_DistanceSensor.h"
#include "Driver_MotionSensor.h"
#include "Driver_MAG3110.h"
#include "User_Console.h"
#include "User_Board.h"


#define SYS_SIGFOX_INIT         0
#define SYS_SIGFOX_FRAME        1
#define SYS_SIGFOX_FRAME_WAIT   2
#define SYS_CARRIER_INIT        3
#define SYS_CARRIER_RUN         4
#define SYS_DOWNLINK            5
#define SYS_DOWNLINK_WAIT       6
#define SYS_SIGFOX_ERROR        7
#define SYS_IDLE                8
#define SYS_BATTERY_LOW         9
#define SYS_SIGFOX_RETRY        10


#define ERROR_TIME  5

/*
*
*
*
*/

unsigned char currentSensor = 1; // smoker
unsigned char sysState = SYS_SIGFOX_INIT;
unsigned char continueFlag = FLAG_CLR;
unsigned char errCount = 0;
unsigned char sigfoxRetryFlag = SIGFOX_RETRY_OFF;
unsigned char retryCount = 0;
#ifdef LOW_BAT_LED_INDICATION
unsigned char low_bat = 0;
#endif
uint16_t userData[USER_DATA_SIZE];     // byte 0-3, Not Used 
                                      // byte 4-7, schedule time
                                              

/**
 * Main function
 *
 * @param           -
 * @return          not return(infinite loop)
 */
int main( void )
{
    unsigned char ret = 0;

    /*  */
    s_initPeri();

    DebugPrint("Start.....v1.5 NBIOT low_bat\r\n");

    /* Delay for Battery Stable */
    onLED(GREEN_LED);
    onLED(RED_LED);

    delay_Ms(1000);

    offLED(GREEN_LED);
    offLED(RED_LED);


#if defined (WATER_LEAKAGE_SENSOR) || defined (GAS_COUNTER)

    UserDataFlash_Read(&userData,USER_DATA_SIZE);    
    DebugPrint("Flash user data:0x%04x 0x%04x 0x%04x 0x%04x\r\n", userData[0], userData[1], userData[2], userData[3]);    

    schedule_Time = (((uint32_t)(userData[2])) << 16);
    schedule_Time |= ((uint32_t)(userData[3]));

    DebugPrint("schedule_Time from flash:%lu\r\n", schedule_Time);
    if (schedule_Time == 0xFFFFFFFF)
        schedule_Time = SCHEDUCLE_TIME;    

    if (schedule_Time < MIN_SCHEDUCLE_TIME) 
        schedule_Time = MIN_SCHEDUCLE_TIME;
        
    DebugPrint("schedule_Time:%lu\r\n", schedule_Time);
#endif

#if defined (WATER_LEAKAGE_SENSOR) || defined (GAS_COUNTER)
    
    /* Check the ADC value is valid */    
    get_Adc_Value();

    if (batteryCapacity  == 1)    // abnormal state, go to the error state    
        sysState = SYS_SIGFOX_ERROR;

#ifdef LOW_BAT_LED_INDICATION    
    if (batteryCapacity < LOW_BATTER_LEVEL)
      low_bat = 0;
#endif      

    /* check for the Sigfox Module */
    /* Sigfox module will be init in the main loop, if error, it will go to the error case also */
    /* Did not do the checking here, to avoid the double init for the Sigfox Module */
    
#endif




#ifdef WATER_LEAKAGE_SENSOR
    if(GET_BIT(TRIGGER_COUNTER_PIN) == 1)
    {
        waterState = WATER_LEAK;
    }
    else
    {
        waterState = WATER_NO_LEAK;
    }

    waterOldState = waterState;
#endif // WATER_LEAKAGE_SENSOR

    delay_Ms(3000);

    /*      */
    while(1)
    {
        wdt_clear();

        switch(sysState)
        {
        case SYS_SIGFOX_INIT:
            /* Sigfox */
            
            DebugPrint("NB-IOT Module Config Init \r\n");
            if(Sigfox_config() == SIGFOX_ERROR)
            {
                offLED(GREEN_LED);
                onLED(RED_LED);
                sysState = SYS_SIGFOX_ERROR;
            }
            else
            {
                sysState = SYS_SIGFOX_FRAME;
            }
        case SYS_DOWNLINK:
        case SYS_SIGFOX_FRAME:
            /*  */
            schedule_Stamp = SysTickets;
        case SYS_SIGFOX_RETRY:
            /*  */
            if(sysState == SYS_DOWNLINK )
            {
                replyOption = SIGFOX_REPLY;
            }
            else
            {
                replyOption = SIGFOX_DEFAULT_LINK;
            }

            /*  */
            ret = Sigfox_SendFrameInit();
#ifdef LOW_BAT_LED_INDICATION            
            if (batteryCapacity < LOW_BATTER_LEVEL)
              low_bat = 1;
            else {
              low_bat = 0;
              offLED(RED_LED);
            }               
#endif                          
            if(ret == SIGFOX_ERROR)
            {
                offLED(GREEN_LED);
                onLED(RED_LED);
                sysState = SYS_SIGFOX_ERROR;
            }
            else
            {

                offLED(RED_LED);
                onLED(GREEN_LED);
                if(sysState == SYS_DOWNLINK)
                {
                    DebugPrint("Wait Downlink\r\n");
                    sysState = SYS_DOWNLINK_WAIT;
                }
                else
                {
                    DebugPrint("Wait Frame\r\n");
                    sysState = SYS_SIGFOX_FRAME_WAIT;
                }
            }
            break;

        case SYS_DOWNLINK_WAIT:
        case SYS_SIGFOX_FRAME_WAIT:

             schedule_Stamp = SysTickets;

            ret = Sigfox_SendFrameCheck();
            if(ret == SIGFOX_ERROR)
            {
                offLED(GREEN_LED);
                onLED(RED_LED);
                if(sigfoxRetryFlag == SIGFOX_RETRY_ON)
                {
                    retryCount++;
                    if(retryCount>=SIGFOX_RETRY_COUNT)
                    {
                        retryCount = 0;
                        errCount = 0;
                        sysState = SYS_SIGFOX_ERROR;
                        Sigfox_config();
                    }
                    else
                    {
                        DebugPrint("Frame Send 2nd time\r\n");
                        Sigfox_ATCloseSock();                

                        sysState = SYS_SIGFOX_FRAME;
                    }
                }
                else
                {
                    Sigfox_ATCloseSock();                
                    errCount = 0;
                    sysState = SYS_SIGFOX_ERROR;
                }
            }
            else if(ret == SIGFOX_OK)
            {
                offLED(GREEN_LED);
                offLED(RED_LED);
                
                Sigfox_ATCloseSock();
                /*  */
                DebugPrint("Frame OK\r\n");
                sysState = SYS_IDLE;
                continueFlag = 1;
                retryCount = 0;
            }
            else if(SysTicketFlag)
            {
                SysTicketFlag = FLAG_CLR;
                if(sysState == SYS_DOWNLINK_WAIT)
                {
                    blinkLED(GREEN_LED);
                    blinkLED(RED_LED);
                }
                else
                {
                    blinkLED(GREEN_LED);
                }
            }
/*
            if(retryCount >0)
            {
                 schedule_Stamp = SysTickets;
            }
*/
            break;

        case SYS_CARRIER_INIT:
            DebugPrint("Carrier Mode\r\n");
            if(Sigfox_CarrierMode() == SIGFOX_OK)
            {
                onLED(GREEN_LED);
                onLED(RED_LED);
                sysState = SYS_CARRIER_RUN;
            }
            else
            {
                sysState = SYS_SIGFOX_ERROR;
            }
            break;

        case SYS_CARRIER_RUN:
            /* Reset Schedule Time */
            schedule_Stamp = SysTickets;
            /* If key press, exit carrier mode */
            if(keyEvent != KEY_EVENT_NONE)
            {
                if(keyEvent == KEY_EVENT_CARRIER)
                {
                    DebugPrint("Exit Carrier\r\n");
                    if(Sigfox_SetCarrier()==SIGFOX_OK)
                    {
                        Sigfox_SetPowerMode(SIGFOX_SLEEP);
                        offLED(GREEN_LED);
                        offLED(RED_LED);
                        sysState = SYS_IDLE;
                    }
                }
                else if(keyEvent == KEY_EVENT_DOWNLINK)
                {
                    DebugPrint("To Downlink\r\n");
                    if(Sigfox_SetCarrier() == SIGFOX_OK)
                    {
                        sysState = SYS_DOWNLINK;
                        continueFlag = FLAG_SET;
                    }
                    else
                    {
                        sysState = SYS_SIGFOX_ERROR;
                    }
                }

                keyEvent = KEY_EVENT_NONE;
            }
            else if(SysTicketFlag)
            {
                SysTicketFlag = FLAG_CLR;

                blinkLED(RED_LED);
                blinkLED(GREEN_LED);
            }
            break;

        case SYS_SIGFOX_ERROR:
            if(SysTicketFlag)
            {
                SysTicketFlag = FLAG_CLR;

                blinkLED(RED_LED);

                errCount++;
                if(errCount > 10)
                {
                    errCount = 0;
                    offLED(GREEN_LED);
                    offLED(RED_LED);
                    sysState = SYS_IDLE;
                }
            }
            break;

        case SYS_IDLE:
            if((SysTickets - schedule_Stamp) >= schedule_Time)
            {
                DebugPrint("Schedule...\r\n");
                /* Reset Schedule Time */
                schedule_Stamp = SysTickets;

#ifdef  SMART_BIN_SENSOR
                Run_MotionSensorCtrl();
                if(Bin_CoverClosed==1)
                {
                    /* I2C0 SDA */
                    clear_bit( P40DIR );
                    clear_bit( P40C0 );
                    set_bit( P40C1 );
                    set_bit( P40MD0 );
                    clear_bit( P40MD1 );

                    /* I2C0 SCL */
                    clear_bit( P41DIR );
                    clear_bit( P41C0 );
                    set_bit( P41C1 );
                    set_bit( P41MD0 );
                    clear_bit( P41MD1 );

                    Power_On_TempSensorCtrl();
                    delay_Ms(3);
                    Power_On_DistSensorCtrl();
                    delay_Ms(3);
                    Run_TempSensorCtrl();
                    Run_DistSensorCtrl();
                    Power_Off_DistSensorCtrl();
                    Power_Off_TempSensorCtrl();

                    bq27541_get_status();
                    bq27541_get_BatterySOC(&FG_Bat_SOC);

                    /* Send Frame */
                    sysState = SYS_SIGFOX_FRAME;
                    continueFlag = 1;
                }
#endif // SMART_BIN_SENSOR

#ifdef LOW_BAT_LED_INDICATION
                if (low_bat == 1) {
                  get_Adc_Value();
                  if (batteryCapacity >= LOW_BATTER_LEVEL) {
                    // Reset the Low Battery status
                    low_bat = 0;
                    offLED(RED_LED);
                  }                  
                }
#endif                


#ifdef WATER_LEAKAGE_SENSOR
                /*  */
                if(waterState == WATER_LEAK)
                {
                    waterOldState = waterState;
                    /* Send Frame */
                    sysState = SYS_SIGFOX_FRAME;
                    continueFlag = 1;
                    DebugPrint("Schedule Leak\r\n");
                }
#endif // WATER_LEAKAGE_SENSOR
#ifdef GAS_COUNTER
                /* Send Frame */
                if(gasOldCounter != gasCounter)
                {
                    userData[0] = ((gasCounter >> 16) & 0xffff);
                    userData[1] = (gasCounter & 0x0000ffff);
                    UserDataFlash_Write(userData, 2);
                    gasOldCounter = gasCounter;
                    sysState = SYS_SIGFOX_FRAME;
                    continueFlag = 1;
                }
#endif // GAS_COUNTER

#ifdef GAS_COUNTER_TEST
                gasOldCounter = gasCounter;
                sysState = SYS_SIGFOX_FRAME;
                continueFlag = 1;
#endif // GAS_COUNTER_TEST
            } //End Schedule

#ifdef LOW_BAT_LED_INDICATION            
            if (low_bat == 1) {
              blinkLED(RED_LED);
            }
#endif            
            

#ifdef WATER_LEAKAGE_SENSOR
            if (waterOldState != waterState)
            {
                DebugPrint(">>>>>Sensor State Change\r\n");
                /*  */
                if(waterOldState == WATER_NO_LEAK && waterState == WATER_LEAK)
                {
                    waterOldState = waterState;
                    sysState = SYS_SIGFOX_FRAME;
                    continueFlag = 1;
                    DebugPrint("Below Threshold\r\n");
                }

                if(waterOldState == WATER_LEAK && waterState == WATER_NO_LEAK)
                {
                    waterOldState = waterState;
                    sysState = SYS_SIGFOX_FRAME;
                    continueFlag = 1;
                    DebugPrint("Above Threshold\r\n");
                }
            }
#endif // WATER_LEAKAGE_SENSOR

            break; //End Idle

        case SYS_BATTERY_LOW:
            break;
        } //End Switch

#if defined (SMART_BIN_SENSOR) || defined (PACKING_SENSOR)
/*
         if(get_bit(BATTERY_GOOD_PIN)== 0)
         {
            if(get_bit(BATTERY_CE_PIN) == 1)
            {
                 clear_bit(BATTERY_CE_PIN);
            }
         }
         else
         {
             if(get_bit(BATTERY_CE_PIN) == 0)
             {
                 set_bit(BATTERY_CE_PIN);
             }
         }
*/
#endif // defined

        /*  */
        if((sysState != SYS_SIGFOX_ERROR) || (sysState != SYS_BATTERY_LOW)||(sysState != SYS_DOWNLINK_WAIT))
        {
            /* Check Key */
            if(keyEvent != KEY_EVENT_NONE)
            {
                if(keyEvent == KEY_EVENT_CARRIER)
                {
                
                    if (currentSensor == 0) {
                        currentSensor = 1;
                        DebugPrint("Switch Sensor to Smoker Sensor\r\n");
                        NH3_LED_OFF;
                    }
                    else { 
                        currentSensor = 0;
                        DebugPrint("Switch Sensor to NH3 Sensor\r\n");
                        NH3_LED_ON;
                    }
                    sysState = SYS_IDLE;
                }
                else if(keyEvent == KEY_EVENT_DOWNLINK)
                {
                    sysState = SYS_DOWNLINK;
                }

                continueFlag = 1;
                keyEvent = KEY_EVENT_NONE;
            }

            /*      */
            if(continueFlag)
            {
                continueFlag = FLAG_CLR;
                continue;
            }
        }


#ifdef PACKING_SENSOR
        if(sysState == SYS_IDLE)
        {
            write_bit( P37D, 1);
            if(MAG3110_Int || get_bit(P36D))
            {
                if(Run_MagneticSensorCtrl())
                {
                    if(sysState == SYS_IDLE)
                    {
                        DebugPrint("Send Statue\r\n");
                        sysState = SYS_SIGFOX_FRAME;
                        continueFlag = 1;
                    }
                }

                MAG3110_Int = 0;
            }
            write_bit( P37D, 0);
        }

#endif // PACKING_SENSOR

        /*      */
        if((sysState == SYS_SIGFOX_FRAME_WAIT) || (sysState == SYS_DOWNLINK_WAIT))
        {
            lp_setHaltMode();
        }
        else
        {
            //DebugPrint("Halt H\r\n");
            lp_setHaltHMode();
        }

    }

} /* End */




