

#include <string.h>
#include <stdio.h>

#include "mcu.h"
#include "clock.h"
#include "flash.h"
#include "wdt.h"

#include "User_System.h"
#include "User_Generic.h"


#ifdef _ML620Q504
/* Data flash area definition */
#define FLASH_SECTOR_SIZE        ( 1024 )            /**< The size of sector */
#define FLASH_SECTOR_NUM_SEG7    ( 1 )               /**< The number of sector */
#define FLASH_SIZE               ( FLASH_SECTOR_SIZE * FLASH_SECTOR_NUM_SEG7 ) /**< The size of flash memory (data flash area) */

/* Flash memory (data flash area) top address */
#define FLASH_ADRS               ( 0x70000)

#else
#error "Not supported MCU, in this sample program."
#endif

unsigned short verifyResult;
unsigned long  writeAdrs;
unsigned long  blankAddress = 0;

/*=== Data write ===*/
void UserDataFlash_Write(uint16_t *UserWritedata,uint16_t size)
{
    /* init */
  //  unsigned short DataH = (unsigned short)((data>>16) & 0xFFFF);
 //   unsigned short DataL = (unsigned short)(data & 0xFFFF);
    uint16_t i;
    writeAdrs  = FLASH_ADRS;;



    /* enable data flash control */
    flash_controlSelfProg( 1 );
    wdt_clear();
    flash_eraseBlock( (unsigned short __far *)writeAdrs );            /* erase flash block */
    wdt_clear();
    for(i=0;i<size;i++)
    {
        flash_writeWord( (unsigned short __far *)writeAdrs, UserWritedata[i] ); /* write 2Byte data */
        writeAdrs+=2;

    }
    /* disable data flash control */
    flash_controlSelfProg( 0 );
    wdt_clear();
}

void UserDataFlash_Read(uint16_t *UserReaddata,uint16_t size)
{
    /* init */
  //  unsigned short DataH = (unsigned short)((data>>16) & 0xFFFF);
 //   unsigned short DataL = (unsigned short)(data & 0xFFFF);
    uint8_t i;

    writeAdrs  = FLASH_ADRS;

    /* enable data flash control */
    for(i=0;i<size;i++)
    {
        UserReaddata[i]= *((unsigned short __far *)writeAdrs); /* read 2Byte data */
        writeAdrs+=2;

    }
    /* disable data flash control */
}


void DataFlash_BlockErase(void)
{
    // block erase
    writeAdrs = FLASH_ADRS;

    flash_controlSelfProg( 1 );                                       /* enable data flash control */
    wdt_clear();
    flash_eraseBlock( (unsigned short __far *)writeAdrs );            /* erase flash block */
    flash_controlSelfProg( 0 );                                       /* disable data flash control */
    verifyResult = 0;
}

/*=== Data write ===*/
/*
void DataFlash_Write(unsigned int data)
{

    unsigned short DataH = (unsigned short)((data>>16) & 0xFFFF);
    unsigned short DataL = (unsigned short)(data & 0xFFFF);

    writeAdrs = blankAddress;

    if(blankAddress >= FLASH_ADRS + (FLASH_SIZE *2))
    {
        DataFlash_BlockErase();
        blankAddress = FLASH_ADRS;
    }

  //   enable data flash control
    flash_controlSelfProg( 1 );
    wdt_clear();
    flash_writeWord( (unsigned short __far *)writeAdrs, DataH ); // write 2Byte data
    writeAdrs+=2;
    flash_writeWord( (unsigned short __far *)writeAdrs, DataL ); //write 2Byte data
    writeAdrs+=2;
    blankAddress = writeAdrs;
    ///disable data flash control
    flash_controlSelfProg( 0 );
}
*/
/*
void DataFlash_GetBlankAddress(unsigned int* data)
{

    unsigned short i;
    unsigned short byteH, byteL;
    unsigned char flag = 2;
    unsigned int temp = 0;

    writeAdrs = FLASH_ADRS;
//     verify
    for(i=0; i < (FLASH_SIZE / 2)+1 ; i++)
    {
        wdt_clear();
        byteH = *((unsigned short __far *)writeAdrs);
        blankAddress = writeAdrs;
        writeAdrs += 2;
        byteL = *((unsigned short __far *)writeAdrs);
        writeAdrs += 2;

        if(byteH == 0xFFFF && byteL==0xFFFF)
        {
            if(blankAddress == FLASH_ADRS)
            {
               flag = 0;
            }
            else
            {
               flag = 1;
            }
            break;
        }
    }


    if(flag == 1)
    {
        writeAdrs = blankAddress - 2;
        byteL = *((unsigned short __far *)writeAdrs);
        writeAdrs -= 2;
        byteH = *((unsigned short __far *)writeAdrs);
        temp = byteH;
        temp = temp << 16;
        temp = temp | byteL;
        *data = temp;
    }


    if(flag == 2)
    {
        DataFlash_BlockErase();
        blankAddress = FLASH_ADRS;
//        DebugPrint("Erase\r\n");
    }
}
*/

#if 0
unsigned char DataFlash_BankCheck(void)
{
    /* init */
    unsigned short i;

    writeData = 0xFFFF;
    writeAdrs = FLASH_ADRS;

    /* verify */
    for(i=0; i < FLASH_SIZE; i++)
    {
        wdt_clear();
        if( *((unsigned short __far *)writeAdrs) != writeData )
        {
//            DebugPrint("BankCheck Fail\r\n");
            return 1;
        }
        writeAdrs += 2;
    }

 //   DebugPrint("BankCheck OK\r\n");
    return 0;
}

void DataFlash_SectorErase(void)
{
    /*=== Sector erase ===*/
    writeAdrs = FLASH_ADRS;

    flash_controlSelfProg( 1 );                               /* enable data flash control */
    wdt_clear();
    flash_eraseSector( (unsigned short __far *)writeAdrs );   /* erase flash sector */
    flash_controlSelfProg( 0 );
}



#endif // 0


